import {HTMLObserverObservable} from "./html-observer-observable";

export {Button}

import * as loglevel from '../../libraries/loglevel.js';
var log = loglevel.getLogger("gui.html");

class Button extends HTMLObserverObservable {
    constructor() {
        super();
        //this._buttonEl = null;

        this._command = null;
        log.debug("Button initialized", this);
    }

    set command(command) {
        this._command = command;
        this._buildHTML();
    }


    _buildHTML() {
        /*let a = document.createElement("a");
        a.classList.add("mt-2", "btn", "btn-block", "btn-outline-primary", "btn-lg");
        a.setAttribute("role", "button");
        a.setAttribute("type", "button");

        this.innerHTML = '';
        this.appendChild(a);
*/
        // Create and append icon
        if (this.dataset.src) {
            let icon = document.createElement("img");
            icon.setAttribute("src", this.dataset.src);
            icon.setAttribute("width", this.dataset.width);
            icon.setAttribute("height", this.dataset.height);
            icon.classList.add("button__icon");
            //a.appendChild(icon);
            this.appendChild(icon);
        }


        // Create and append label
        let label = document.createElement("span");
        label.innerHTML = this.dataset.label || this._command.text;
        label.classList.add("button__label");

        //a.appendChild(label);
        this.appendChild(label);


        let _this = this;
        //a.addEventListener("click", function(evt) {
        this.addEventListener("click", function(evt) {
            log.trace("Button clicked", evt);
            _this.notifyObservers(_this._command.value);
        });
    }

    attributeChangedCallback(name, oldValue, newValue) {
        // When the drawer is disabled, update keyboard/screen reader behavior.
        // if (this.disabled) {
        //     this.setAttribute('tabindex', '-1');
        //     this.setAttribute('aria-disabled', 'true');
        // } else {
        //     this.setAttribute('tabindex', '0');
        //     this.setAttribute('aria-disabled', 'false');
        // }
        // TODO: also react to the open attribute changing.
    }
}

customElements.define('mosaic-button', Button);

