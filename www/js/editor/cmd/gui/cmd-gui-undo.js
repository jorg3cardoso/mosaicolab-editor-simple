export {CmdGUIUndo}


import {Cmd} from "../imports";
import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd");

/**
 * GUI Command Undo
 */
class CmdGUIUndo extends Cmd {

    constructor() {
        super();
    }

    get name() {
        return CmdGUIUndo.name;
    }


    get editor() {
        return this._editor;
    }

    set editor(value) {
        this._editor = value;
    }


    _dodo() {
        super._dodo();
        this._hadEffect = this._editor._commandHistory.undo();
        if (!this._hadEffect) {
            log.info("Nothing to undo");
        } else {
            log.info("Undid previous command.");
        }

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        return this;
    }

    toString() {
        return "CmdGUIUndo[]";
    }


    /* Private methods */
    _check() {
        if (!this._editor) {
            log.error(this.toString());
            throw("No MosaicEditor has been set, cannot execute command.");
        }

    }
}