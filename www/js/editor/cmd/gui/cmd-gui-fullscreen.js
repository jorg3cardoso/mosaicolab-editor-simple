import {FetchLocal} from "../../utility/FetchLocal";
import * as loglevel from '../../libraries/loglevel.js';

import {Cmd} from "../imports";

export {CmdGUIFullscreen}

var log = loglevel.getLogger("cmd.gui");

class CmdGUIFullscreen extends Cmd {

    constructor() {
        super();
        this._persistence = null;
        this._fileURI = null;
        this._data = null;
    }

    get name() {
        return CmdGUIFullscreen.name;
    }


    _dodo() {
        super._dodo();
        log.debug("Fullscreening");
        let fs = this.editor.sketch.fullscreen();
        if (fs) {
            log.info("Leaving fullscreen");
        } else {
            log.info("Entering fullscreen");
        }
        this.editor.sketch.fullscreen(!fs);
        log.debug("Fullscreend");
    }

    toJS() {
        let json = {cmdName: this.name};
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        super.fromJS(obj)
        return this;
    }


    toString() {
        return "CmdGUIFullscreen []";
    }


    /* Private methods */
    _check() {
        if (!this.editor) {
            log.error(this.toString());
            throw("No editor has been set, cannot execute command.");
        }

    }
}