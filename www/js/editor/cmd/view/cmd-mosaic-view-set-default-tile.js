export {CmdMosaicViewSetDefaultTile}

import {CmdUndoable} from "../cmd-undoable";
import {TesselShapeFactory} from "../../mosaic/tessel-shape-factory";
import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.view");

class CmdMosaicViewSetDefaultTile extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._w = null;
        this._h = null;
        this._shapeName = null;
        this._colorIndex = null;


        this._previousW = null;
        this._previousH = null;
        this._previousShapeName = null;
        this._previousColorIndex = null;
    }

    get name() {
        return CmdMosaicViewSetDefaultTile.name;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }


    get w() {
        return this._w;
    }

    set w(value) {
        this._w = value;
    }

    get h() {
        return this._h;
    }

    set h(value) {
        this._h = value;
    }

    get shapeName() {
        return this._shapeName;
    }

    set shapeName(value) {
        this._shapeName = value;
    }

    get colorIndex() {
        return this._colorIndex;
    }

    set colorIndex(value) {
        this._colorIndex = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Changing default tile: ", this);

        let tessel = this._mosaicView.defaultTile;

        this._previousColorIndex = tessel.colorIndex;
        this._previousW = tessel.w;
        this._previousH = tessel.h;
        this._previousShapeName = tessel.shape.name;

        tessel.colorIndex = this._colorIndex;
        tessel.w = this._w;
        tessel.h = this._h;
        tessel.shape = TesselShapeFactory.create(this._shapeName);

        this._mosaicView.defaultTile = tessel;
        log.debug("Changed default tile to: ", tessel );

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {colorIndex: this._colorIndex, w: this._w, h: this._h, shapeName:this.shapeName});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.w = obj.w;
        this.h = obj.h;
        this.shapeName = obj.shapeName;
        this.colorIndex = obj.colorIndex;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdMosaicViewSetDefaultTile();
        undo.mosaicView = this._mosaicView;
        undo.colorIndex = this._previousColorIndex;
        undo.w = this._previousH;
        undo.h = this._previousH;
        undo.shapeName = this._previousShapeName;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousColorIndex !== this._colorIndex ||
        this._previousW !== this._w ||
        this._previousH !== this._h ||
        this._previousShapeName !== this._shapeName) {
            return true;
        } else {
            return false;
        }
    }



    toString() {
        return "CmdMosaicViewSetDefaultTile [mosaicView:" + this._mosaicView + ", colorIndex: " + this._colorIndex + ", w: " + this._w + ", h: " + this._h + ", shapeName: " + this._shapeName + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }

        if (this._w === null || this._w === undefined) {
            log.error(this.toString());
            throw("No w provided, cannot execute command.");
        }
        if (this._h === null || this._h === undefined) {
            log.error(this.toString());
            throw("No h provided, cannot execute command.");
        }
        if (!this._shapeName) {
            log.error(this.toString());
            throw("No shape name provided, cannot execute command.");
        }

    }
}