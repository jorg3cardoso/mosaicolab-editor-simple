export {TesselShapeOval}
import {TesselShape} from "./tessel-shape";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("mosaic");

class TesselShapeOval extends TesselShape {
    constructor() {
        super();
    }

    get name() {
        return TesselShapeOval.name;
    }

    doPath(ctx, w, h) {
        let x = 0;
        let y = 0;
        ctx.beginPath();
        ctx.ellipse(w/2, h/2, w/2, h/2, 0, 0, 2 * Math.PI);

        ctx.closePath();
    }

    toJSON() {
        let json = super.toJSON();

        log.trace("Converting ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJSON(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJSON(obj);
    }
}