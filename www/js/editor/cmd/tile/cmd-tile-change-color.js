export {CmdTileChangeColor}

import {CmdUndoable} from "../cmd-undoable";

import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.tile");


class CmdTileChangeColor extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._tesselId = null;
        this._tessel = null;
        this._colorIndex = null;
        this._previousColorIndex = null;
    }

    get name() {
        return CmdTileChangeColor.name;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get tesselId() {
        return this._tesselId;
    }

    set tesselId(value) {
        this._tesselId = value;
    }

    get colorIndex() {
        return this._colorIndex;
    }

    /**
     * colorId is optional. If not set will clear the tile.
     * @param value
     */
    set colorIndex(value) {
        this._colorIndex = value;
    }


    _dodo() {
        super._dodo();

        this._tessel = this._mosaicView.getTesselById(this._tesselId);
        if (this._tessel === null) {
            log.error("No tile with id ", this._tesselId, " found in mosaic.");
            return;
        }
        this._previousColorIndex = this._tessel.colorIndex;// === null ? null : this._tessel.color.id;
        this._tessel.colorIndex = this._colorIndex;//this._mosaicView.getColorById(this._colorId);
        log.debug("Changed tile color index to: ", this._tessel.colorIndex );

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {tesselId: this._tessel.id, colorIndex: this._colorIndex});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.tesselId = obj.tesselId;
        this.colorIndex = obj.colorIndex;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdTileChangeColor();
        undo.mosaicView = this._mosaicView;
        undo.tesselId = this._tesselId;
        undo.colorIndex = this._previousColorIndex;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousColorIndex === this._colorIndex) {
            return false;
        } else {
            return true;
        }
    }



    toString() {
        return "CmdTileChangeColor [mosaicView:" + this._mosaicView + ", tesselId: " + this._tesselId + ", colorId: " + this._colorId + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (this._tesselId === null || this._tesselId === undefined) {
            log.error(this.toString());
            throw("No tile id provided, cannot execute command.");
        }

    }
}