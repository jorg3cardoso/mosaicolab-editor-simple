import {FetchLocal} from "../../utility/FetchLocal";
import * as loglevel from '../../libraries/loglevel.js';

import {Cmd} from "../imports";

export {CmdGUIImportProject}

var log = loglevel.getLogger("cmd.gui");

class CmdGUIImportProject extends Cmd {

    constructor() {
        super();
        this._persistence = null;
        this._fileURI = null;
        this._data = null;
    }

    get name() {
        return CmdGUIImportProject.name;
    }


    get persistence() {
        return this._persistence;
    }

    set persistence(value) {
        this._persistence = value;
    }

    get fileURI() {
        return this._fileURI;
    }

    set fileURI(value) {
        this._fileURI = value;
    }

    get data() {
        return this._data;
    }

    set data(data) {
        this._data = data;
    }

    _dodo() {
        super._dodo();
        log.debug("Importing project");

        let jsonState = "";

        if (this._fileURI) {
            FetchLocal.getInstance().fetch(this._fileURI).then(function (response) {
                return response.text();
            }, function(fail) {
                log.error(fail);
            }).then(function (json) {
                jsonState = JSON.parse(json);
            }, function(fail) {
                log.error(fail);
            }).finally(function () {
                // TODO: need fromObject
                log.debug("Read: ", jsonState);
                this._persistence.loadFromJS(jsonState);
            }.bind(this));
        } else if (this._data) {
            jsonState = JSON.parse(this._data);
        }

        log.debug("Read: ", jsonState);
        this._persistence.loadFromJS(jsonState);
        log.debug("Imported project: ", this._fileURI);
    }

    toJS() {
        let json = {cmdName: this.name, fileURI: this._fileURI, data: this._data};
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        super.fromJS(obj)

        this.fileURI = obj.fileURI;
        this.data = obj.data;
        return this;
    }


    toString() {
        return "CmdGUIImportProject [persistence:" + this._persistence + ", fileURI:" + this._fileURI + "; data:", + this._data + "]";
    }


    /* Private methods */
    _check() {
        if (!this._persistence) {
            log.error(this.toString());
            throw("No PersistentState has been set, cannot execute command.");
        }
        if (!this._fileURI && !this._data) {
            log.error(this.toString());
            throw("No file URI nor data provided, cannot execute command");
        }

    }
}