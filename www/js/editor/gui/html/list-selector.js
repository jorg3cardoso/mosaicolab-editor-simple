import {HTMLObserverObservable} from "./html-observer-observable";

export {ListSelector}

import * as loglevel from '../../libraries/loglevel.js';
var log = loglevel.getLogger("gui.html");

class ListSelector extends HTMLObserverObservable {
    constructor() {
        super();
        this._selectEl = null;
        this._listItems = [];
        log.debug("ListSelector initialized", this);
    }

    set items(items) {
        log.debug("Adding items: ", items);
        Array.prototype.push.apply(this._listItems, items);
        this._buildHTML();
    }

    /**
     * Sets the selected item in the list
     * @param text
     */
    select(text) {
        this._listItems.forEach(function (option) {
           if (option.text === text) {
               this._selectEl.value = JSON.stringify(option.value);
           }
        }.bind(this));
    }

    _buildHTML() {
        let select = document.createElement("select");
        select.classList.add("mt-3", "form-control", "form-control-lg");

        this._listItems.forEach(function (option) {
             let op = document.createElement("option");
             op.value = JSON.stringify(option.value);
             op.innerHTML = option.text;
             select.appendChild(op);
         });
        this.innerHTML = '';
        this.appendChild(select);

        let _this = this;
        select.addEventListener("change", function valueChanged(evt) {
            log.trace("Value changed", evt);
            //let s = this.value;
            _this.notifyObservers(JSON.parse(this.value));
        });
        this._selectEl = select;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        // When the drawer is disabled, update keyboard/screen reader behavior.
        // if (this.disabled) {
        //     this.setAttribute('tabindex', '-1');
        //     this.setAttribute('aria-disabled', 'true');
        // } else {
        //     this.setAttribute('tabindex', '0');
        //     this.setAttribute('aria-disabled', 'false');
        // }
        // TODO: also react to the open attribute changing.
    }
}

customElements.define('list-selector', ListSelector);

