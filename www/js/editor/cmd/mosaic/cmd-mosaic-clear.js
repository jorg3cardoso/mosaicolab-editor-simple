import {CmdUndoable} from "../cmd-undoable";

import * as loglevel from '../../libraries/loglevel.js';
import {CmdMosaicBulkCommand} from "./cmd-mosaic-bulk-command";
import {CmdTileChangeColor} from "../tile/cmd-tile-change-color";

export {CmdMosaicClear}

var log = loglevel.getLogger("cmd.mosaic");

class CmdMosaicClear extends CmdUndoable {
    constructor() {
        super();
        this._mosaicView = null;

        this._previousColorIndexes = null;

        this._hadEffect = false;
    }

    get name() {
        return CmdMosaicClear.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Clearing mosaic..." );
        this._previousColorIndexes = [];
        this._hadEffect = false;
        this._mosaicView.mosaic.startBulkUpdate();
        this._mosaicView.mosaic.tessels.forEach(function(tessel, index) {
            this._previousColorIndexes.push(tessel.colorIndex);
            tessel.colorIndex = null;

            if (tessel.colorIndex !== this._previousColorIndexes[index]) {
                this._hadEffect = true;
            }
        }.bind(this));

        this._mosaicView.mosaic.endBulkUpdate();

        log.debug("Cleared mosaic: ", this._mosaicView.mosaic );

    }

    getUndoCommand() {
        let undo = new CmdMosaicBulkCommand();
        undo.mosaicView = this._mosaicView;

        for (let i = 0; i < this._mosaicView.mosaic.tessels.length; i++) {
            let tessel = this._mosaicView.mosaic.tessels[i];
            let cmdChangeColor = new CmdTileChangeColor();
            cmdChangeColor.mosaicView = this._mosaicView;
            cmdChangeColor.tesselId = tessel.id;
            cmdChangeColor.colorIndex = this._previousColorIndexes[i];
            undo.addCommand(cmdChangeColor);
        }
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        return this._hadEffect;
    }
    toJS() {
        let json = super.toJS();
        //Object.assign(json, {numberOfTessels: this._numberOfTessels});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        //this.numberOfTessels = obj.numberOfTessels;
        return this;
    }


    toString() {
        return "CmdMosaicClear [mosaicView:" + this._mosaicView + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }

    }
}