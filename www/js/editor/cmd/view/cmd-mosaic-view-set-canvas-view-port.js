import {Vec2} from "../../utility/vec2";

export {CmdMosaicViewSetCanvasViewPort}
import {CmdUndoable} from "../cmd-undoable";
import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.view");

class CmdMosaicViewSetCanvasViewPort extends CmdUndoable {
    constructor() {
        super();
        this._mosaicView = null;
        this._canvasViewPort = null;

        this._previousCanvasViewPort = null;

    }
    get name() {
        return CmdMosaicViewSetCanvasViewPort.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }


    get canvasViewPort() {
        return this._canvasViewPort;
    }

    set canvasViewPort(value) {
        this._canvasViewPort = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Changing canvas view port to: ", this._canvasViewPort);
        this._previousCanvasViewPort = this._mosaicView.canvasViewPort;

        this._mosaicView.canvasViewPort = this._canvasViewPort;

        log.debug("Changed canvas view port to: ", this._canvasViewPort);
    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {canvasViewPort: this._canvasViewPort.toJSON()});

        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);
        let viewPort = new Vec2();
        this.canvasViewPort = viewPort.fromJSON(obj.canvasViewPort);
        return this;
    }

    getUndoCommand() {
        let undo = new CmdMosaicViewSetCanvasViewPort();
        undo.mosaicView = this._mosaicView;
        undo.canvasViewPort = this._previousCanvasViewPort;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousCanvasViewPort.equals(this._canvasViewPort)) {
            return false;
        } else {
            return true;
        }
    }



    toString() {
        return "CmdMosaicViewSetCanvasViewPort [mosaicView:" + this._mosaicView + ", canvasViewPort: " + this._canvasViewPort + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }

        if (!this._canvasViewPort) {
            log.error(this.toString());
            throw("No canvas view port has been set, cannot execute command.");
        }

    }
}