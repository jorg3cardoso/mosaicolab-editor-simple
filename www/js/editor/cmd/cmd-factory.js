import {CmdMosaicViewSetCanvasViewPort} from "./view/cmd-mosaic-view-set-canvas-view-port";

export {CmdFactory}
import {CmdGUIChangeShape, CmdMosaicViewGridResize, CmdUndoable, CmdList} from "./imports";

import * as loglevel from '../libraries/loglevel.js';
import {CmdTileChangeColor} from "./tile/cmd-tile-change-color";
import {CmdMosaicCreate} from "./mosaic/cmd-mosaic-create";
import {CmdMosaicViewSetLayout} from "./view/cmd-mosaic-view-set-layout";
import {CmdMosaicAddDefaultTile} from "./mosaic/cmd-mosaic-add-default-tile";
import {CmdMosaicInsertDefaultTile} from "./mosaic/cmd-mosaic-insert-default-tile";
import {CmdMosaicRemoveTile} from "./mosaic/cmd-mosaic-remove-tile";
import {CmdMosaicViewTileSelect} from "./view/cmd-mosaic-view-tile-select";
import {CmdTileChangeShape} from "./tile/cmd-tile-change-shape";
import {CmdGUIExportProject} from "./gui/cmd-gui-export-project";
import {CmdGUIImportProject} from "./gui/cmd-gui-import-project";
import {CmdGUIExportImage} from "./gui/cmd-gui-export-image";
import {CmdMosaicViewSetViewport} from "./view/cmd-mosaic-view-set-viewport";
import {CmdMosaicViewSetCanvasScale} from "./view/cmd-mosaic-view-set-canvas-scale";
import {CmdGUIUndo} from './gui/cmd-gui-undo';
import {CmdGUIRedo} from './gui/cmd-gui-redo';
import {CmdGUIHome} from './gui/cmd-gui-home';
import {CmdGUIImportSVG} from "./gui/cmd-gui-import-svg";
import {CmdGUIPublishProject} from "./gui/cmd-gui-publish-project";
import {CmdGUIPublishExportImage} from "./gui/cmd-gui-publish-export-image";
import {CmdGUIFullscreen} from "./gui/cmd-gui-fullscreen";

var log = loglevel.getLogger("cmd");


class CmdFactory {
    static create(cmdName) {
        switch (cmdName) {
            case CmdGUIChangeShape.name:
                return new CmdGUIChangeShape();
            case CmdGUIExportImage.name:
                return new CmdGUIExportImage();
            case CmdGUIExportProject.name:
                return new CmdGUIExportProject();
            case CmdGUIFullscreen.name:
                return new CmdGUIFullscreen();
            case CmdGUIImportProject.name:
                return new CmdGUIImportProject();
            case CmdGUIImportSVG.name:
                return new CmdGUIImportSVG();
            case CmdGUIHome.name:
                return new CmdGUIHome();
            case CmdGUIPublishProject.name:
                return new CmdGUIPublishProject();
            case CmdGUIPublishExportImage.name:
                return new CmdGUIPublishExportImage();
            case CmdTileChangeColor.name:
                return new CmdTileChangeColor();
            case CmdTileChangeShape.name:
                return new CmdTileChangeShape();
            case CmdMosaicCreate.name:
                return new CmdMosaicCreate();
            case CmdMosaicAddDefaultTile.name:
                return new CmdMosaicAddDefaultTile();
            case CmdMosaicInsertDefaultTile.name:
                return new CmdMosaicInsertDefaultTile();
            case CmdMosaicRemoveTile.name:
                return new CmdMosaicRemoveTile();
            case CmdMosaicViewGridResize.name:
                return new CmdMosaicViewGridResize();
            case CmdMosaicViewSetViewport.name:
                return new CmdMosaicViewSetViewport();
            case CmdMosaicViewTileSelect.name:
                return new CmdMosaicViewTileSelect();
            case CmdMosaicViewSetCanvasScale.name:
                return new CmdMosaicViewSetCanvasScale();
            case CmdMosaicViewSetCanvasViewPort.name:
                return new CmdMosaicViewSetCanvasViewPort();
            case CmdMosaicViewSetLayout.name:
                return new CmdMosaicViewSetLayout();
            case CmdList.name:
                return new CmdList();
            case CmdGUIRedo.name:
                return new CmdGUIRedo();
            case CmdGUIUndo.name:
                return new CmdGUIUndo();
            default:
                log.warn("Tried creating a command not available in CmdFactory: ", cmdName);
        }

        return null;
    }

    static createFromJS(obj){
        log.debug("Creating Cmd from JS: ", obj);
        return CmdFactory.create(obj.cmdName).fromJS(obj);
    }
}



