export {CmdList}
import {CmdUndoable} from "./imports";
import {CmdFactory} from "./imports";
import * as loglevel from '../libraries/loglevel.js';

var log = loglevel.getLogger("cmd");
class CmdList extends CmdUndoable {

    constructor() {
        super();
        this.commandList = new Array();

        this._mosaicView = null;
    }


    get name() {
        return CmdList.name;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
        this.commandList.forEach(function(cmd){
            cmd.mosaicView = value;
        });
    }

    addCommand(cmd) {
        this.commandList.push(cmd);
    }

    _predo() {
        super._predo();
    }

    _dodo() {
        super._dodo();
        log.debug("Executing list of commands");
        this.commandList.forEach(function(cmd){
            cmd._predo();
            cmd._dodo();
            cmd._postdo();
        });
        log.debug("Finished executing list of commands");
    }
    _postdo() {
        super._postdo();
    }

    getUndoCommand() {
        let undoList = new CmdList();
        for (let i = this.commandList.length-1; i >= 0; i--) {
            //this.commandList[i].getUndoCommand();
            undoList.addCommand(this.commandList[i].getUndoCommand());
        }
        return undoList;
    }

    hadEffect() {
        if (!this._executed) {
            log.warn("CmdUndoable.hadEffect() should not be called before command executed");
        }

        for (let i = this.commandList.length-1; i >= 0; i--) {
            if (this.commandList[i].hadEffect()) {
                return true;
            }
        }

        return false;
    }

    toJS() {
        let json = super.toJS();

        let cmds = [];
        this.commandList.forEach(function(cmd){
            cmds.push(cmd.toJS())
        });

        Object.assign(json, {cmds: cmds});

        log.trace("Converting ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);
        this.commandList = new Array();
        let _this = this;
        obj.cmds.forEach(function(cmdJSON){
            let cmd = CmdFactory.createFromJS(cmdJSON);
            cmd.mosaicView = _this._mosaicView;
            _this.addCommand(cmd);
        });
        return this;
    }


    toString() {
        let s ="[";
        for (let i = this.commandList.length-1; i >= 0; i--) {
            s+=this.commandList[i].toString() + ", ";
        }
        s+="]";
        return s;
    }
}
