export {CmdMosaicViewTileSelect}

import {CmdUndoable} from "../cmd-undoable";

import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.tile");


class CmdMosaicViewTileSelect extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._tesselId = null;

    }

    get name() {
        return CmdMosaicViewTileSelect.name;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get tesselId() {
        return this._tesselId;
    }

    set tesselId(value) {
        this._tesselId = value;
    }


    _dodo() {
        super._dodo();
        this._tessel = this._mosaicView.getTesselById(this._tesselId);
        if (this._tessel === null) {
            log.warn("No tile with id ", this._tesselId, " found in mosaic.");
        }
        this._mosaicView.addSelectedTessel(this._tessel);
        log.debug("Selected tile: ", this._tessel );

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {tesselId: this._tessel.id});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.tesselId = obj.tesselId;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdUnselectTessel();
        undo.mosaicView = this._mosaicView;
        undo.tesselId = this._tesselId;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        return true;

    }



    toString() {
        return "CmdMosaicViewTileSelect [mosaicView:" + this._mosaicView + ", tesselId: " + this._tesselId + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (this._tesselId === null || this._tesselId === undefined) {
            log.error(this.toString());
            throw("No tile id provided, cannot execute command.");
        }

    }
}