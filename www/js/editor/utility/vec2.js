export {Vec2}

class Vec2 {
    constructor(x, y) {
        this._x = x;
        this._y = y;
    }

    get x() {
        return this._x;
    }

    set x(value) {
        this._x = value;
    }

    get y() {
        return this._y;
    }

    set y(value) {
        this._y = value;
    }

    add(vec2) {
        return new Vec2(this._x+vec2._x, this._y+vec2._y);
    }



    toJSON() {
        return {x: this._x, y: this._y};
    }

    fromJSON(obj) {
        this.x = obj.x;
        this.y = obj.y;
        return this;
    }

    clone() {
        return new Vec2(this._x, this._y);
    }

    equals(vec2) {
        return vec2.x === this._x && vec2.y === this._y;
    }

    toString() {
        return "Vec2 [x: "+this.x + ", y: " + this._y + "]";
    }
}