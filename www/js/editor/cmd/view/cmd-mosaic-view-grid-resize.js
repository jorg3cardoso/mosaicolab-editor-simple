import {CmdList, CmdMosaicBulkCommand, CmdUndoable} from "../imports";

import * as loglevel from '../../libraries/loglevel.js';

import {CmdMosaicCreate} from "../mosaic/cmd-mosaic-create";
import {CmdTileChangeColor} from "../tile/cmd-tile-change-color";
import {CmdTileChangeShape} from "../tile/cmd-tile-change-shape";
import {CmdTileChangeId} from "../tile/cmd-tile-change-id";
import {CmdMosaicViewSetLayout} from "./cmd-mosaic-view-set-layout";
import {MosaicLayoutGrid} from "../../layout/mosaic-layout-grid";

export {CmdMosaicViewGridResize}

var log = loglevel.getLogger("cmd.view");

/**
 * The CmdMosaicViewGridResize assumes the mosaic is using a MosaicLayoutGrid
 */
class CmdMosaicViewGridResize extends CmdUndoable {
    constructor() {
        super();
        this._mosaicView = null;
        this._cols = null;
        this._rows = null;

        this._previousCols = null;
        this._previousRows = null;
        this._previousLayout = null;
        this._previousTessels = null;

    }
    get name() {
        return CmdMosaicViewGridResize.name;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }


    get cols() {
        return this._cols;
    }

    set cols(value) {
        this._cols = value;
    }

    get rows() {
        return this._rows;
    }

    set rows(value) {
        this._rows = value;
    }


    _dodo() {
        super._dodo();
        if (!(this._mosaicView.mosaicLayout instanceof MosaicLayoutGrid)) {
            log.warn("Cannot change grid size because mosaic does not have a grid layout applied.");
            return;
        }
        this._previousLayout = this._mosaicView.mosaicLayout;
        let mosaic = this._mosaicView.mosaic;
        let oldTessels = this._mosaicView.mosaic.tessels;
        this._previousTessels = oldTessels;
        if (!(this._previousLayout instanceof MosaicLayoutGrid)) {
            log.warn("Attempted to resize a mosaic not using MosaicLayoutGrid");
        }

        let newTessels = [];
        for (let i = 0; i < this._cols*this._rows; i++ ) {
            newTessels.push(this._mosaicView.defaultTile);
        }


        this._previousCols = this._previousLayout.params[0];
        this._previousRows = this._previousLayout.params[1];


        //  centering it
        let centerC = Math.floor(this._cols / 2);
        let centerR = Math.floor(this._rows / 2);
        let oldCenterC = Math.floor(this._previousCols / 2);
        let oldCenterR = Math.floor(this._previousRows / 2);


        for (let i = 0; i < oldTessels.length; i++) {
            let oldC = i % this._previousCols;
            let oldR = Math.floor(i / this._previousCols);
            let newC = centerC-oldCenterC+oldC;
            let newR = centerR-oldCenterR+oldR;

            if (newC >= 0 && newR >= 0 && newC < this._cols && newR < this._rows) {
                let newI = newR*this._cols+newC;
                newTessels[newI].colorIndex = oldTessels[i].colorIndex;
            }
        }

        // Update mosaic
        mosaic.startBulkUpdate();
        for (let i = oldTessels.length-1; i >= 0; i--) {
            mosaic.removeTessel(oldTessels[i]);
        }

        mosaic.addTessels(newTessels);
        this._previousLayout.params = [this._cols, this._rows, this._previousLayout.params[2]];
        mosaic.endBulkUpdate();

        log.debug("Resized mosaic grid to ", this._cols, " x ", this._rows);

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {cols: this._cols, rows: this._rows});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.cols = obj.cols;
        this.rows = obj.rows;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdList();
        let cmdCreate = new CmdMosaicCreate();
        cmdCreate.mosaicView = this._mosaicView;
        cmdCreate.numberOfTessels = this._previousTessels.length;
        undo.addCommand(cmdCreate)

        let bulk = new CmdMosaicBulkCommand();
        bulk.mosaicView = this._mosaicView;

        for (let i = 0; i < this._previousTessels.length; i++) {
            let cmdChangeId = new CmdTileChangeId();
            cmdChangeId.mosaicView = this._mosaicView;
            cmdChangeId.tesselIndex = i;
            cmdChangeId.newTesselId = this._previousTessels[i].id;
            bulk.addCommand(cmdChangeId);

            let cmdChangeColor = new CmdTileChangeColor();
            cmdChangeColor.mosaicView = this._mosaicView;
            cmdChangeColor.tesselId = this._previousTessels[i].id;
            cmdChangeColor.colorIndex = this._previousTessels[i].colorIndex;
            bulk.addCommand(cmdChangeColor);

            let cmdChangeTesselShape = new CmdTileChangeShape();
            cmdChangeTesselShape.mosaicView = this._mosaicView;
            cmdChangeTesselShape.tesselId = this._previousTessels[i].id;
            cmdChangeTesselShape.shapeName = this._previousTessels[i].shape.name;
            bulk.addCommand(cmdChangeTesselShape);
        }

        let cmdSetLayout = new CmdMosaicViewSetLayout();
        cmdSetLayout.mosaicView = this._mosaicView;
        cmdSetLayout.mosaicLayoutName = this._mosaicView.mosaicLayout.name;
        cmdSetLayout.params = [this._previousCols, this._previousRows, this._previousLayout.params[2]];
        bulk.addCommand(cmdSetLayout);

        undo.addCommand(bulk);
        return undo;
    }

    hadEffect() {
        super.hadEffect();

        if (this._cols != this._previousCols || this._rows != this._previousRows) {
            return true;
        } else {
            return false;
        }
    }



    toString() {
        return "CmdMosaicViewGridResize [mosaicView:" + this._mosaicView + ", cols: " + this._cols + ", rows: " + this._rows+ "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (this._cols === null || this._cols === undefined) {
            log.error(this.toString());
            throw("No columns have been set, cannot execute command.");
        }
        if (this._rows === null || this._rows === undefined) {
            log.error(this.toString());
            throw("No rows have been set, cannot execute command.");
        }
    }
}