export {MosaicLayout}


import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("layout");

class MosaicLayout {
    constructor(){}

    set params(params) {

    }

    get name() {
        return MosaicLayout.name;
    }

    doLayout(mosaic) {
        log.debug("Doing layout");
    }

    toJSON() {
        return {name: MosaicLayout.name};
    }

    fromJSON(obj) {
        log.trace("Creating from JSON: ", MosaicLayout.name);
        if (obj.name !== MosaicLayout.name) {
            throw "Wrong layout name. Was expecting '" + MosaicLayout.name + "'. Got '" + obj.name + "' instead.";
        }
        return this;
    }
}
