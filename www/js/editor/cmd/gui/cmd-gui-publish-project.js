

export {CmdGUIPublishProject}

import * as loglevel from '../../libraries/loglevel.js';

import {Cmd} from "../imports";

var log = loglevel.getLogger("cmd.gui");

class CmdGUIPublishProject extends Cmd {

    constructor() {
        super();
        this._editor = null;
        this._persistence = null;
    }

    get name() {
        return CmdGUIPublishProject.name;
    }

    get editor() {
        return this._editor;
    }

    set editor(value) {
        this._editor = value;
    }

    get persistence() {
        return this._persistence;
    }

    set persistence(value) {
        this._persistence = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Publishing project");
        if (this._editor) {
            this._editor.notify("Publishing project...", "info");
        }

        let js = this._persistence.getJS();

        let date = new Date();
        let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(js));

        async function postData(url = '', data = {}) {
            // Default options are marked with *
            const response = await fetch(url, {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.

                headers: {
                    'authorization': 'token d64fbd27-6778-41c6-905a-daada2cf74ef'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });
            return response.json(); // parses JSON response into native JavaScript objects
        }

        let name = date.getFullYear()+ "/" + date.getMonth() + "/" + date.getDate() + "/" + date.getHours()+ "/" + date.getMinutes() + "/"+ date.getSeconds() + "/" + parseInt(Math.random()*1000);

        postData('https://jsonbin.org/jorgecardoso/mosaics/'+name, js)
            .then(data => {
                console.log(data); // JSON data parsed by `data.json()` call
            });

        if (this._editor) {
            this._editor.notify("Published project!", "success");
        }
        log.debug("Published project: ", name);
    }

    toJS() {
        let json = {cmdName: this.name};
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        super.fromJS(obj)

        //this.linkSelector = obj.linkSelector;
        return this;
    }


    toString() {
        return "CmdGUIPublishProject [persistence:" + this._persistence + "]";
    }


    /* Private methods */
    _check() {
        if (!this._persistence) {
            log.error(this.toString());
            throw("No PersistentState has been set, cannot execute command.");
        }
    }
}