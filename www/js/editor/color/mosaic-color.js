export {MosaicColor, MosaicSolidColor, MosaicTextureColor}
import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("color");

class MosaicColor {

    constructor() {
        this._id = MosaicColor._nextId;
        MosaicColor._nextId++;
    }
    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
        if (value >= MosaicColor._nextId) {
            MosaicColor._nextId = value + 1;
        }
    }
    equals() {
    }
}
MosaicColor._nextId = 0;

class MosaicSolidColor extends MosaicColor {

    constructor(r, g, b) {
        super();
        this._r = r;
        this._g = g;
        this._b = b;
    }

    get red() {
        return this._r;
    }

    get green() {
        return this._g;
    }

    get blue() {
        return this._b;
    }
    equals(mosaicColor) {
        let ret =  mosaicColor instanceof MosaicSolidColor && this._r === mosaicColor.red && this._g === mosaicColor.green && this._b === mosaicColor.blue;
        return ret;
    }

    toString() {
        return "SolidColor [" + this._r + ", " + this._g + ", " + this._b + "]";
    }
}

class MosaicTextureColor extends MosaicColor {
    constructor(image, url) {
        super();

        this._texture = image;
        this._imgUrl = url;
    }
    get texture() {
        return this._texture;

    }
    get imgUrl() {
        return this._imgUrl;
    }

    equals(mosaicColor) {

        let ret =  mosaicColor instanceof MosaicTextureColor && this._texture === mosaicColor.texture;
        return ret;
    }

    toString() {
        return "MosaicTextureColor [" +this._imgUrl + "]";
    }
}