import {CmdUndoable} from "../cmd-undoable";

import * as loglevel from '../../libraries/loglevel.js';
import {CmdMosaicRemoveTile} from "./cmd-mosaic-remove-tile";

export {CmdMosaicInsertDefaultTile}


var log = loglevel.getLogger("cmd.mosaic");

class CmdMosaicInsertDefaultTile extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._positionToInsert = null;
        this._idToInsert = null;
        this._createdTesselId = null;
    }

    get name() {
        return CmdMosaicInsertDefaultTile.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }
    get positionToInsert() {
        return this._positionToInsert;
    }

    set positionToInsert(value) {
        this._positionToInsert = value;
    }

    get idToInsert() {
        return this._idToInsert;
    }

    set idToInsert(value) {
        this._idToInsert = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Adding tile at ", this._positionToInsert ? "position " + this._positionToInsert : " end" );

        let tessel = this._mosaicView.defaultTile;

        let mosaic = this._mosaicView.mosaic;
        if(this._positionToInsert === null) {
            mosaic.addTessel(tessel);
        } else {
            mosaic.insertTesselAt(tessel, this._positionToInsert);
        }

        if (this._idToInsert !== null) {
            tessel.id = this._idToInsert;
        }


        // save id for undo
        this._createdTesselId = tessel.id;

        log.debug("Created tile: ", tessel.toString() );

    }

    getUndoCommand() {
        let undo = new CmdMosaicRemoveTile();
        undo.mosaicView = this._mosaicView;
        undo.tesselId = this._createdTesselId;

        return undo;

    }

    hadEffect() {
        super.hadEffect();

        return true;
    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {positionToInsert: this._positionToInsert});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.positionToInsert = obj.positionToInsert;
        return this;
    }


    toString() {
        return "CmdMosaicInsertDefaultTile [mosaicView:" + this._mosaicView + ", positionToInsert: " + this._positionToInsert + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }

    }
}