export {CmdUndoable}

import {Cmd} from "./cmd.js";
import * as loglevel from '../libraries/loglevel.js';

var log = loglevel.getLogger("cmd");

class CmdUndoable extends Cmd {
    constructor() {
        super();
    }

    
    getUndoCommand() {

    }

    hadEffect() {
        if (!this._executed) {
            log.warn("CmdUndoable.hadEffect() should not be called before command executed");
        }
        return true;
    }

}