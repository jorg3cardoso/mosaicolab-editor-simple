import {MosaicLayoutGrid} from "./mosaic-layout-grid";

export {MosaicLayoutFactory}


import * as loglevel from '../libraries/loglevel.js';
import {MosaicLayoutTriangles} from "./mosaic-layout-triangles";
import {MosaicLayout} from "./mosaic-layout";
var log = loglevel.getLogger("layout");

class MosaicLayoutFactory {
    static create(layoutName) {
        switch (layoutName) {
            case MosaicLayout.name:
                return new MosaicLayout();
            case MosaicLayoutGrid.name:
                return new MosaicLayoutGrid();
            case MosaicLayoutTriangles.name:
                return new MosaicLayoutTriangles();

        }
        return null;
    }

    static createFromJSON(json) {
        log.trace("Factory creating from JSON: ", json.name);
        let name = json.name;
        let layout = MosaicLayoutFactory.create(name);
        layout.fromJSON(json);
        return layout;
    }
}
