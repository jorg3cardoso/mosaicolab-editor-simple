export {HTMLObserverObservable}

import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("gui.html");

class HTMLObserverObservable extends HTMLElement {

    constructor() {
        super();
        this._observerList = new Array();
        this._asyncObserverList = new Array();
    }

    addObserver(observer, async) {
        if (async) {
            log.debug("Adding async observer: ", observer);
            if (!this._asyncObserverList.includes(observer)) {
                this._asyncObserverList.push(observer);
            }
        } else {
            log.debug("Adding sync observer: ", observer);
            if (!this._observerList.includes(observer)) {
                this._observerList.push(observer);
            }
        }

    }

    removeObserver(observer, async) {
        if (async === undefined || !async) {
            if (this._observerList.includes(observer)) {
                this._observerList.splice(this._observerList.indexOf(observer), 1);
            }
        }
        if (async === undefined || async) {
            if (this._asyncObserverList.includes(observer)) {
                this._asyncObserverList.splice(this._asyncObserverList.indexOf(observer), 1);
            }
        }
    }

    notifyObservers(params) {
        let t0 = performance.now();
        log.trace("Notifying sync observers: ", this._observerList);
        this._observerList.forEach(function(observer) {
            log.trace("Notifying sync observer: ", observer.toString());
            observer.update(this, params);
        }, this);

        log.trace("Notifying async observers: ", this._asyncObserverList);
        let _this = this;
        this._asyncObserverList.forEach(function(observer) {
            log.trace("Notifying async observer: ", observer.toString());
            setTimeout(function() {observer.update(_this, params)}, 0);
            //observer.update(this, params);
        });
        //var t1 = performance.now();
        //log.debug("Notifying observers took: " + (t1 - t0) + " milliseconds.");
    }

    update (observable, params) {
        //log.trace(observable, params);
    }
}