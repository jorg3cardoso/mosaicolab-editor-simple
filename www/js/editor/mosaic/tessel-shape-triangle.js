export {TesselShapeTriangle}
import {TesselShape} from "./tessel-shape";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("mosaic");

class TesselShapeTriangle extends TesselShape {

    constructor() {
        super();
        this._inverted = false;
    }

    get name() {
        return TesselShapeTriangle.name;
    }

    get inverted() {
        return this._inverted;
    }

    set inverted(value) {
        this._inverted = value;
    }

    doPath(ctx, w, h) {
        let x = 0;
        let y = 0;
        ctx.beginPath();
        if (this._inverted) {
            ctx.moveTo(w/2, h);
            ctx.lineTo(0, 0);
            ctx.lineTo(w, 0);
        } else {
            ctx.moveTo(w/2, 0);
            ctx.lineTo(w, h);
            ctx.lineTo(0, h);
        }

        ctx.closePath();
    }

    toJSON() {
        let json = super.toJSON();
        Object.assign(json, {inverted: this._inverted});
        log.trace("Converting ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJSON(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJSON(obj);
        this.inverted = obj.inverted;
        return this;
    }
}