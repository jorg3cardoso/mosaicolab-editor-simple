import {CmdUndoable} from "../cmd-undoable";

import * as loglevel from '../../libraries/loglevel.js';
import {CmdMosaicRemoveTile} from "./cmd-mosaic-remove-tile";

export {CmdMosaicAddDefaultTile}


var log = loglevel.getLogger("cmd.mosaic");

class CmdMosaicAddDefaultTile extends CmdUndoable {
    constructor() {
        super();
        this._mosaicView = null;
        this._createdTesselId = null;
    }

    get name() {
        return CmdMosaicAddDefaultTile.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Adding tile" );

        let tessel = this._mosaicView.defaultTile;

        let mosaic = this._mosaicView.mosaic;
        mosaic.addTessel(tessel);

        // save id for undo
        this._createdTesselId = tessel.id;

        log.debug("Created tile: ", tessel.toString() );

    }
    getUndoCommand() {
        let undo = new CmdMosaicRemoveTile();
        undo.mosaicView = this._mosaicView;
        undo.tesselId = this._createdTesselId;

        return undo;

    }

    hadEffect() {
        super.hadEffect();

        return true;
    }

    toJS() {
        let json = super.toJS();
        //Object.assign(json, {numberOfTessels: this._numberOfTessels});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        //this.numberOfTessels = obj.numberOfTessels;
        return this;
    }


    toString() {
        return "CmdMosaicAddDefaultTile [mosaicView:" + this._mosaicView + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }

    }
}