export {Observer, Observable, ObservableObserver}

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("observer");

class Observer {
    constructor() {

    }

    update(observable, params) {
        //log.trace(observable, params);
    }

}

class Observable {
    constructor() {
        this._observerList = new Array();
        this._asyncObserverList = new Array();
    }

    addObserver(observer, async) {
        if (async === true) {
            log.debug("Adding async observer: ", observer, " to: ", this);
            if (!this._asyncObserverList.includes(observer)) {
                this._asyncObserverList.push(observer);
            }
        } else {
            log.debug("Adding sync observer: ", observer, " to: ", this);
            if (!this._observerList.includes(observer)) {
                this._observerList.push(observer);
            }
        }

    }

    removeObserver(observer, async) {
        if (async === undefined || !async) {
            if (this._observerList.includes(observer)) {
                this._observerList.splice(this._observerList.indexOf(observer), 1);
            }
        }
        if (async === undefined || async) {
            if (this._asyncObserverList.includes(observer)) {
                this._asyncObserverList.splice(this._asyncObserverList.indexOf(observer), 1);
            }
        }
    }

    notifyObservers(params) {
        let t0 = performance.now();
        log.debug("Notifying sync observers: ", this._observerList);
        this._observerList.forEach(function(observer) {
            log.trace("Notifying sync observer: ", observer.toString(), params);
            observer.update(this, params);
        }, this);

        log.debug("Notifying async observers: ", this._asyncObserverList);
        let _this = this;
        this._asyncObserverList.forEach(function(observer) {
            log.trace("Notifying async observer: ", observer.toString());
            setTimeout(function() {observer.update(_this, params)}, 0);
            //observer.update(this, params);
        });
        //var t1 = performance.now();
        //log.debug("Notifying observers took: " + (t1 - t0) + " milliseconds.");
    }
}


class ObservableObserver extends Observable {
    constructor() {
        super();
    }

    update (observable, params) {
        //log.trace(observable, params);
    }
}