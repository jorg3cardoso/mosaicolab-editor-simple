export {PointerEvent, MainClick, ShiftMainClick, ControlMainClick, SecundaryClick, ShiftSecundaryClick, ControlSecundaryClick, MainDrag, ShiftMainDrag, ControlMainDrag, SecundaryDrag, ShiftSecundaryDrag, ControlSecundaryDrag, Pinch}

class PointerEvent {
    constructor(originalEvt) {
        this._originalEvt = originalEvt;

        if (originalEvt instanceof MouseEvent) {
            this.clientX = originalEvt.clientX;
            this.clientY = originalEvt.clientY;
        } else if (originalEvt instanceof TouchEvent) {
            this.clientX = originalEvt.touches[0].clientX;
            this.clientY = originalEvt.touches[0].clientY;
        }
    }

    get name() {
        return PointerEvent.name;
    }

    get originalEvt() {
        return this._originalEvt;
    }
}

class MainClick extends PointerEvent {
    constructor(originalEvt) {
        super(originalEvt);


    }
    get name() {
        return MainClick.name;
    }
}

class ShiftMainClick extends PointerEvent {
    constructor(originalEvt) {
        super(originalEvt);
    }
    get name() {
        return ShiftMainClick.name;
    }
}

class ControlMainClick extends PointerEvent {
    constructor(originalEvt) {
        super(originalEvt);
    }
    get name() {
        return ControlMainClick.name;
    }
}

class SecundaryClick extends PointerEvent {
    constructor(originalEvt) {
        super(originalEvt);
    }
    get name() {
        return SecundaryClick.name;
    }
}
class ShiftSecundaryClick extends PointerEvent {
    constructor(originalEvt) {
        super(originalEvt);
    }
    get name() {
        return ShiftSecundaryClick.name;
    }
}
class ControlSecundaryClick extends PointerEvent {
    constructor(originalEvt) {
        super(originalEvt);
    }
    get name() {
        return ControlSecundaryClick.name;
    }
}
class MainDrag extends PointerEvent {
    constructor(originalEvt, isFinished) {
        super(originalEvt);
        this._isFinished = isFinished;
    }

    get name() {
        return MainDrag.name;
    }

    get finished() {
        return this._isFinished;
    }

    set movementX(x) {
        this._movementX = x;
    }
    set movementY(y) {
        this._movementY = y;
    }
    get movementX() {
        return this._movementX;
    }
    get movementY() {
        return this._movementY;
    }
}
class ShiftMainDrag extends MainDrag {
    constructor(originalEvt, isFinished) {
        super(originalEvt, isFinished);

    }

    get name() {
        return ShiftMainDrag.name;
    }
}
class ControlMainDrag extends MainDrag {
    constructor(originalEvt, isFinished) {
        super(originalEvt, isFinished);

    }

    get name() {
        return ControlMainDrag.name;
    }
}
class SecundaryDrag extends MainDrag {
    constructor(originalEvt, isFinished) {
        super(originalEvt, isFinished);

    }

    get name() {
        return SecundaryDrag.name;
    }
}
class ShiftSecundaryDrag extends MainDrag {
    constructor(originalEvt, isFinished) {
        super(originalEvt, isFinished);

    }

    get name() {
        return ShiftSecundaryDrag.name;
    }
}
class ControlSecundaryDrag extends MainDrag {
    constructor(originalEvt, isFinished) {
        super(originalEvt, isFinished);

    }

    get name() {
        return ControlSecundaryDrag.name;
    }
}
class Pinch extends MainDrag {
    constructor(originalEvt, isFinished, pinchStart) {
        super(originalEvt, isFinished);
        //console.log("pinch start", pinchStart)
        // average position of two pointers
        this.clientX = (originalEvt.touches[0].clientX+originalEvt.touches[1].clientX)/2;
        this.clientY = (originalEvt.touches[0].clientY+originalEvt.touches[1].clientY)/2;

        //average position of starting pointers
        this.startClientX = (pinchStart[0].x+pinchStart[1].x)/2;
        this.startClientY = (pinchStart[0].y+pinchStart[1].y)/2;

        // movement
        this.movementX = this.clientX-this.startClientX;
        this.movementY = this.clientY-this.startClientY;

        // current distance of pointers
        this.pointerDist = Math.sqrt((originalEvt.touches[0].clientX-originalEvt.touches[1].clientX)*(originalEvt.touches[0].clientX-originalEvt.touches[1].clientX)+(originalEvt.touches[0].clientY-originalEvt.touches[1].clientY)*(originalEvt.touches[0].clientY-originalEvt.touches[1].clientY));

        // distance of starting pointers
        this.startPointerDist = Math.sqrt( (pinchStart[0].x-pinchStart[1].x)*(pinchStart[0].x-pinchStart[1].x) + (pinchStart[0].y-pinchStart[1].y)*(pinchStart[0].y-pinchStart[1].y) );

        // percentage
        this.pointerDistPercent =  this.pointerDist/this.startPointerDist;
    }

    get name() {
        return Pinch.name;
    }
}
