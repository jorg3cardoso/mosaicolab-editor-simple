export {CmdTileChangeId}

import {CmdUndoable} from "../cmd-undoable";

import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.tile");


class CmdTileChangeId extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._tesselIndex = null;
        this._newTesselId = null;

        this._previousTesselId = null;
    }

    get name() {
        return CmdTileChangeId.name;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }


    get tesselIndex() {
        return this._tesselIndex;
    }

    set tesselIndex(value) {
        this._tesselIndex = value;
    }

    get newTesselId() {
        return this._newTesselId;
    }

    set newTesselId(value) {
        this._newTesselId = value;
    }

    _dodo() {
        super._dodo();
        this._tessel = this._mosaicView.mosaic.tessels[this._tesselIndex];
        if (this._tessel === null) {
            log.error("No tile with index ", this._tesselIndex, " found in mosaic.");
            return;
        }
        this._previousTesselId = this._tessel.id;// === null ? null : this._tessel.color.id;
        this._tessel.id = this._newTesselId;//this._mosaicView.getColorById(this._colorId);
        log.debug("Changed tile id to: ", this._tessel.id);

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {tesseldIndex: this._tesselIndex, newTesseldId: this._newTesselId});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.tesselIndex = obj.tesselIndex;
        this.newTesselId = obj.newTesselId;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdTileChangeId();
        undo.mosaicView = this._mosaicView;
        undo.tesselIndex = this._tesselIndex;
        undo.newTesselId = this._previousTesselId;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousColorIndex === this._colorIndex) {
            return false;
        } else {
            return true;
        }
    }



    toString() {
        return "CmdTileChangeId [mosaicView:" + this._mosaicView + ", tesselIndex: " + this._tesselIndex + ", newTesselId: " + this._newTesselId + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (this._tesselIndex === null || this._tesselIndex === undefined) {
            log.error(this.toString());
            throw("No tile index provided, cannot execute command.");
        }

        if (this._newTesselId === null || this._newTesselId === undefined) {
            log.error(this.toString());
            throw("No new tile id provided, cannot execute command.");
        }
    }
}