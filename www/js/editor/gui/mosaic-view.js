import {TesselPainterFactory} from "../painter/tessel-painter-factory";

export {MosaicView}

import {Observable, ObservableObserver} from "../observer/observer-observable";
import {Vec2} from "../utility/vec2";
import {Dimension} from "../utility/dimension";

//let {scale, rotate, translate, compose, applyToPoint, inverse} = window.TransformationMatrix;
import {scale, rotate, translate, compose, applyToPoint} from '../libraries/transformation-matrix.min.js';

import {Tessel} from "../mosaic/tessel";

import {TesselPainterComposite} from "../painter/tessel-painter-composite";
import {TesselShapeOval} from "../mosaic/tessel-shape-oval";
import {TesselShapeTriangle} from "../mosaic/tessel-shape-triangle";
import {TesselShapeFactory} from "../mosaic/tessel-shape-factory";
import * as loglevel from '../libraries/loglevel.js';

var log = loglevel.getLogger("gui");

class MosaicView extends ObservableObserver {

    constructor() {
        super();

        this._mosaic = null;

        this._mosaicLayout = null;

        this._colorPalette = null;

        this._defaultTile = null;

        this._canvasViewPort = new Vec2(0, 0);
        this._canvasScale = 1;
        // normalized viewport
        this._mosaicViewPort = new Dimension(0, 0, 1, 1);

        this._mosaicSelectedTessels = [];
        this._isScaling = false;
        this._isScalingPrevious = false;
    }

    /* Getters and Setters */

    get mosaic() {
        return this._mosaic;
    }

    set mosaic(value) {
        this._mosaic = value;
        this._mosaic.addObserver(this);

        //

        if (this._mosaicLayout) {
            this._mosaicLayout.doLayout(this._mosaic);
        }

        this.notifyObservers();
    }

    get colorPalette() {
        return this._colorPalette;
    }

    set colorPalette(value) {
        this._colorPalette = value;
    }

    get mosaicLayout() {
        return this._mosaicLayout;
    }

    set mosaicLayout(value) {
        this._mosaicLayout = value;
        if (this._mosaic && this._mosaicLayout) this._mosaicLayout.doLayout(this._mosaic);
        this.notifyObservers();
    }

    get canvasViewPort() {
        return this._canvasViewPort.clone();
    }

    get canvasScale() {
        return this._canvasScale;
    }

    set canvasViewPort(s) {
        this._canvasViewPort.x = s.x;
        this._canvasViewPort.y = s.y;
        this.notifyObservers();
    }

    set canvasScale(s) {
        this._canvasScale = s;
        this.notifyObservers();
    }

    set mosaicViewPort(dimension) {
        log.debug("Setting viewport: ", dimension);
        this._mosaicViewPort.x = dimension.x;
        this._mosaicViewPort.y = dimension.y;
        this._mosaicViewPort.w = dimension.w;
        this._mosaicViewPort.h = dimension.h;
        this.notifyObservers();
    }

    get mosaicViewPort() {
        return this._mosaicViewPort.clone();
    }

    get isScaling() {
        return this._isScaling;
    }

    set isScaling(value) {

        this._isScalingPrevious = this.isScaling;
        this._isScaling = value;

        if (!this._isScaling) {
            this._canvasImage = null;
            this._isScalingPrevious = false;
        }
        this.notifyObservers();
    }

    _saveCanvasImage(ctx) {
        let _this = this;
        let p = this._calculateTransformParams(ctx);

        let x = p.centerTranslateX - p.mosaicWidth / 2 * p.scaleFactor
        let y = p.centerTranslateY - p.mosaicHeight / 2 * p.scaleFactor
        let w = p.mosaicWidth * p.scaleFactor
        let h = p.mosaicHeight * p.scaleFactor
        console.log(p);
        console.log(x, y, w, h);

        let offscreenCanvas = document.createElement('canvas');
        offscreenCanvas.width = w;
        offscreenCanvas.height = h;
        let ctx1 = offscreenCanvas.getContext('2d');


        createImageBitmap(ctx.canvas, x, y, w, h).then(function (image) {
            ctx1.drawImage(image, Math.min(0, x), Math.min(0, y), w, h);
            _this._canvasImage = offscreenCanvas;
            console.log("Saved canvas image")
            console.log(_this._canvasImage)
        });


    }

    addSelectedTessel(tessel) {
        this._mosaicSelectedTessels.push(tessel);
        tessel.painter.add(TesselPainterFactory.getInstance().createTesselPainter("SelectedTesselPainter"));
        this.notifyObservers();
    }

    addSelectedTessels(tessels) {
        tessels.forEach(function (tessel) {
            tessel.painter.add(TesselPainterFactory.getInstance().createTesselPainter("SelectedTesselPainter"));
        });
        Array.prototype.push.apply(this._mosaicSelectedTessels, tessels);
        this.notifyObservers();
    }

    removeSelectedTessel(tessel) {
        tessel.painter.remove(TesselPainterFactory.getInstance().createTesselPainter("SelectedTesselPainter"));
        for (var i = this._mosaicSelectedTessels.length - 1; i >= 0; i--) {
            if (this._mosaicSelectedTessels[i] === tessel) {
                this._mosaicSelectedTessels.splice(i, 1);
            }
        }
        this.notifyObservers();
    }


    get defaultTile() {
        if (this._defaultTile) {
            let tile = new Tessel();
            tile.w = this._defaultTile.w;
            tile.h = this._defaultTile.h;
            tile.colorIndex = this._defaultTile.colorIndex;
            tile.shape = TesselShapeFactory.create(this._defaultTile.shape.name);
            let painter = new TesselPainterComposite();
            painter.add(TesselPainterFactory.getInstance().createTesselPainter("ClearTesselPainter"));
            painter.add(TesselPainterFactory.getInstance().createTesselPainter("SolidTesselPainter"));
            painter.add(TesselPainterFactory.getInstance().createTesselPainter("OutlineTesselPainter"));
            tile.painter = painter;
            return tile;
        } else {
            let tile = new Tessel();
            tile.w = 30;
            tile.h = 30;
            tile.colorIndex = null;
            tile.shape = TesselShapeFactory.create(TesselShapeOval.name);
            let painter = new TesselPainterComposite();
            painter.add(TesselPainterFactory.getInstance().createTesselPainter("ClearTesselPainter"));
            painter.add(TesselPainterFactory.getInstance().createTesselPainter("SolidTesselPainter"));
            painter.add(TesselPainterFactory.getInstance().createTesselPainter("OutlineTesselPainter"));
            tile.painter = painter;//new SolidTesselPainter(); //new ClearTesselPainter();
            return tile;
        }
    }

    set defaultTile(value) {
        this._defaultTile = value;
    }

    /* Overrides update in ObserverObservable. Invoked when Observable notifies of change. */
    update(observable, params) {
        super.update(observable, params);
        if (observable === this._mosaic) {
            // Only trigger a re-layout if the tessels were added or removed from
            // the mosaic or if a tile's position or size changed.
            if (this._mosaicLayout && params &&
                (params.addTessel || params.removeTessel || params.x || params.y || params.w || params.h)) {
                log.debug("Mosaic changed, doing layout. ", params);
                this._mosaicLayout.doLayout(this._mosaic);
            }

            // In any case, something changes, so notify listeners
            this.notifyObservers(params);
        }
    }


    /* Public methods */
    paint(ctx, params) {
        if (this._mosaic === null) return;

        let t0 = performance.now();


        let p = this._calculateTransformParams(ctx);
        log.debug("Calculated transform params: ", p);

        if (!this._mosaicCanvas || this._mosaicCanvas.width !== ctx.canvas.width || this._mosaicCanvas.height !== ctx.canvas.height){
            log.debug("(re)Creating offscreen mosaic canvas (canvas does not exist or mosaic size changed)", this._mosaicCanvas, p);

            this._mosaicCanvas = document.createElement('canvas');
            //this._mosaicCanvas.width = Math.floor(p.mosaicWidth);
            //this._mosaicCanvas.height = Math.floor(p.mosaicHeight);
            this._mosaicCanvas.width = Math.floor(ctx.canvas.width);
            this._mosaicCanvas.height = Math.floor(ctx.canvas.height);
            log.debug("Created canvas with ", this._mosaicCanvas.width, " x ", this._mosaicCanvas.height);
        }

        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.fillStyle = "#fff";
        //ctx.fillStyle=`rgb(${Math.random()*255},${Math.random()*255},${Math.random()*255})`;
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);



        if (!this._isScaling) {
            let mctx = this._mosaicCanvas.getContext('2d');
            if (!params || (params && !params.tesselId) || params.shape) {
                log.debug("Clearing offscreen mosaic canvas.");
                // clear canvas
                mctx.setTransform(1, 0, 0, 1, 0, 0);
                mctx.fillStyle = "#fff";
                //ctx.fillStyle=`rgb(${Math.random()*255},${Math.random()*255},${Math.random()*255})`;
                mctx.fillRect(0, 0, mctx.canvas.width, mctx.canvas.height);
            }
            mctx.setTransform(1, 0, 0, 1, 0, 0);
            //mctx.fillStyle = "#fff";
            //ctx.fillStyle=`rgb(${Math.random()*255},${Math.random()*255},${Math.random()*255})`;
            //mctx.fillRect(0, 0, mctx.canvas.width, mctx.canvas.height);
            mctx.translate(p.centerTranslateX, p.centerTranslateY);

            mctx.scale(p.scaleFactor, p.scaleFactor);
            mctx.translate(-p.mosaicCenterX, -p.mosaicCenterY);

            mctx.save();
            // Clip region outside mosaic viewport
            mctx.beginPath();
            mctx.moveTo(p.mosaicLeft, p.mosaicTop);
            mctx.lineTo(p.mosaicLeft + p.mosaicWidth, p.mosaicTop);
            mctx.lineTo(p.mosaicLeft + p.mosaicWidth, p.mosaicTop + p.mosaicHeight);
            mctx.lineTo(p.mosaicLeft, p.mosaicTop + p.mosaicHeight);
            mctx.closePath();
            mctx.clip();
            // If no params are passed, or
            // params no not include a tesselid, or
            // a tile shape changed, clear the canvas
            // tile shape change causes complete redraw because the old shape needs to be erased

            this._mosaic.paint(mctx, this._colorPalette, params);
            mctx.restore();
        }
        ctx.drawImage(this._mosaicCanvas, 0, 0);
        if (this._isScaling) {
            ctx.save();
            ctx.translate(p.centerTranslateX, p.centerTranslateY);
            ctx.scale(p.scaleFactor, p.scaleFactor);
            ctx.translate(-p.mosaicCenterX, -p.mosaicCenterY);
            ctx.beginPath();
            ctx.moveTo(p.mosaicLeft, p.mosaicTop);
            ctx.lineTo(p.mosaicLeft + p.mosaicWidth, p.mosaicTop);
            ctx.lineTo(p.mosaicLeft + p.mosaicWidth, p.mosaicTop + p.mosaicHeight);
            ctx.lineTo(p.mosaicLeft, p.mosaicTop + p.mosaicHeight);
            ctx.closePath();
            ctx.stroke();

            ctx.restore();
        }
        //ctx.rect(0, 0, p.canvasWidth, p.canvasHeight);
        var t1 = performance.now();
        log.debug("Call to paint took " + (t1 - t0) + " milliseconds.");
    }

    fullCanvas() {
        let mosaicDimension = this._mosaic.mosaicDimension;

        let canvas = document.createElement('canvas');
        canvas.width = mosaicDimension.w;
        canvas.height = mosaicDimension.h;
        log.debug("Created canvas with ", canvas.width, " x ", canvas.height);
        this._mosaic.paint(canvas.getContext('2d'), this._colorPalette);
        return canvas;
    }

    getTesselAt(ctx, x, y) {
        //console.log(x, y)
        let t0 = performance.now();
        let p = this._calculateTransformParams(ctx);
        //console.log(p);

        let domRect = ctx.canvas.getBoundingClientRect();
        //console.log(domRect)
        x = p.canvasWidth * (x - domRect.x) / domRect.width;
        y = p.canvasHeight * (y - domRect.y) / domRect.height;
        //console.log(x, y);


        let matrix = compose(
            translate(p.mosaicCenterX, p.mosaicCenterY),
            scale(1.0 / p.scaleFactor, 1.0 / p.scaleFactor),
            translate(-p.centerTranslateX, -p.centerTranslateY)
        );

        let point = applyToPoint(matrix, {x: x, y: y});
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        let tessels = this._mosaic.getTesselsAt(ctx, point.x, point.y);
        let t1 = performance.now();
        log.debug("Answering click took:" + (t1 - t0) + " milliseconds.");
        return tessels.length === 0 ? null : tessels[0];
    }


    /**
     * Methods delegated to components
     */


    getTesselById(id) {
        if (!this._mosaic) {
            log.error("MosaicView does not have a Mosaic yet!");
        }
        return this._mosaic.getTesselById(id);
    }

    getColorById(id) {
        if (!this._colorPalette) {
            log.error("MosaicView does not have a ColorPalette yet!");
        }
        return this._colorPalette.getColorById(id);
    }

    /* Private methods */

    _calculateTransformParams(ctx) {

        let dim = this._mosaic.mosaicDimension;

        let mosaicLeft = dim.x + this._mosaicViewPort.x * dim.w;
        let mosaicTop = dim.y + this._mosaicViewPort.y * dim.h;

        let mosaicWidth = dim.w * this._mosaicViewPort.w;
        let mosaicHeight = dim.h * this._mosaicViewPort.h;

        let mosaicCenterX = mosaicLeft + mosaicWidth / 2;
        let mosaicCenterY = mosaicTop + mosaicHeight / 2;

        let centeringTranslateX = ctx.canvas.width / 2 + this._canvasViewPort.x * ctx.canvas.width;
        let centeringTranslateY = ctx.canvas.height / 2 + this._canvasViewPort.y * ctx.canvas.height;

        let scaleFactor = Math.min(ctx.canvas.width / mosaicWidth,
            ctx.canvas.height / mosaicHeight);

        return {
            centerTranslateX: centeringTranslateX,
            centerTranslateY: centeringTranslateY,
            scaleFactor: scaleFactor * this._canvasScale,
            mosaicCenterX: mosaicCenterX,
            mosaicCenterY: mosaicCenterY,
            mosaicLeft: mosaicLeft,
            mosaicTop: mosaicTop,
            mosaicWidth: mosaicWidth,
            mosaicHeight: mosaicHeight,
            canvasWidth: ctx.canvas.width,
            canvasHeight: ctx.canvas.height
        };
    }
}