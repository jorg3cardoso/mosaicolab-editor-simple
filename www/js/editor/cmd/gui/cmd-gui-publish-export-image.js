import {CmdGUIPublishProject} from "./cmd-gui-publish-project";


export {CmdGUIPublishExportImage}

import * as loglevel from '../../libraries/loglevel.js';

import {Cmd} from "../imports";
import {CmdGUIExportImage} from "./cmd-gui-export-image";

var log = loglevel.getLogger("cmd.gui");

/**
 * Publishes the project and exports the image
 */
class CmdGUIPublishExportImage extends Cmd {

    constructor() {
        super();
        this._editor = null;
        this._mosaicView = null;
        this._persistence = null;
    }

    get name() {
        return CmdGUIPublishExportImage.name;
    }


    get editor() {
        return this._editor;
    }

    set editor(value) {
        this._editor = value;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }


    get persistence() {
        return this._persistence;
    }

    set persistence(value) {
        this._persistence = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Publishing project and Exporting image");
        let cmdPublish = new CmdGUIPublishProject();
        cmdPublish.editor = this.editor;
        cmdPublish.persistence = this.persistence;

        cmdPublish.do();

        let cmdGUIExportImage = new CmdGUIExportImage();
        cmdGUIExportImage.editor = this.editor;
        cmdGUIExportImage.mosaicView = this.mosaicView;
        cmdGUIExportImage.do();

    }

    toJS() {
        let json = super.toJS();
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        super.fromJS(obj)
        return this;
    }


    toString() {
        return "CmdGUIPublishExportImage [editor: " + this._editor + ", mosaicView:" + this._mosaicView  +"]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
    }
}