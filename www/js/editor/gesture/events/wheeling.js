import {MainDrag} from "./pointer-event";
import * as loglevel from "../../libraries/loglevel";

export {Wheeling, ShiftWheeling, ControlWheeling, SecundaryWheeling}

var log = loglevel.getLogger("gesture");

class Wheeling extends MainDrag {
    constructor(originalEvt, isFinished) {
        super(originalEvt, isFinished);
    }

    get name() {
        return Wheeling.name;
    }
}

class ShiftWheeling extends Wheeling {
    constructor(originalEvt, isFinished) {
        super(originalEvt, isFinished);
    }

    get name() {
        return ShiftWheeling.name;
    }
}


class ControlWheeling extends Wheeling {
    constructor(originalEvt, isFinished) {
        super(originalEvt, isFinished);
    }

    get name() {
        return ControlWheeling.name;
    }
}

class SecundaryWheeling extends Wheeling {
    constructor(originalEvt, isFinished) {
        super(originalEvt, isFinished);
    }

    get name() {
        return SecundaryWheeling.name;
    }
}