import {CmdUndoable} from "../cmd-undoable";
import {Mosaic} from "../../mosaic/mosaic";
import {MosaicPainter} from "../../painter/mosaic-painter";

import * as loglevel from '../../libraries/loglevel.js';

export {CmdMosaicCreate}

var log = loglevel.getLogger("cmd.mosaic");

class CmdMosaicCreate extends CmdUndoable {
    constructor() {
        super();
        this._mosaicView = null;
        this._numberOfTessels = null;
    }

    get name() {
        return CmdMosaicCreate.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get numberOfTessels() {
        return this._numberOfTessels;
    }

    set numberOfTessels(value) {
        this._numberOfTessels = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Creating mosaic with ", this._numberOfTessels, "tessels" );
        let mosaic = new Mosaic();
        mosaic.painter = new MosaicPainter();
        mosaic.startBulkUpdate();
        for (let i = 0; i < this._numberOfTessels; i++) {
                let tessel = this._mosaicView.defaultTile;
                mosaic.addTessel(tessel);
        }

        mosaic.endBulkUpdate();
        this._mosaicView.mosaic = mosaic;
        log.debug("Created mosaic: ", mosaic );

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {numberOfTessels: this._numberOfTessels});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.numberOfTessels = obj.numberOfTessels;
        return this;
    }


    toString() {
        return "CmdMosaicCreate [mosaicView:" + this._mosaicView + ", numberOfTessels: " + this._numberOfTessels + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (!this._numberOfTessels) {
            log.error(this.toString());
            throw("No number of tessels provided, cannot execute command.");
        }

    }
}