export {CmdGUIHome}
import {Cmd} from "../imports";

import * as loglevel from '../../libraries/loglevel';

var log = loglevel.getLogger("cmd");


class CmdGUIHome extends Cmd{

    constructor() {
        super();
    }

    get name() {
        return CmdGUIHome.name;
    }


    get target() {
        return this._target;
    }

    set target(value) {
        this._target = value;
    }


    _dodo() {
        super._dodo();
        location.href = this._target;

        log.info("Redirecting to " + this._target);
    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {target: this._target});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);
        this.target = obj.target;
        return this;
    }



    toString() {
        return "CmdGUIHome[]";
    }


    /* Private methods */
    _check() {

    }
}