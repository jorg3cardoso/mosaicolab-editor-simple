export {TesselShapePath}
import {TesselShape} from "./tessel-shape";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("mosaic");

class TesselShapePath extends TesselShape {
    constructor() {
        super();
        this._pathCommands = null;
    }

    get name() {
        return TesselShapePath.name;
    }

    set pathCommands(svgPathCommands) {
        //this._path = svgpath;
        this._pathCommands = svgPathCommands;
    }

    /*
    scale(percent) {
        log.debug("Scaling shape")
        this._pathCommands.forEach(function(cmd){
            cmd.x = cmd.x*percent;
            cmd.y = cmd.y*percent;
            cmd.x1 = cmd.x1*percent;
            cmd.y1 = cmd.y1*percent;
            cmd.x2 = cmd.x2*percent;
            cmd.y2 = cmd.y2*percent;
        }.bind(this));
        log.debug(this._pathCommands);
    }
*/
    doPath(ctx, w, h) {
       // console.log("scale ", this._percent);

       // let scaleX = this._percent; //w/this._maxX;
       // let scaleY = this._percent; //h/this._maxY;
       ctx.beginPath();
        //ctx.ellipse(w/2, h/2, w/2, h/2, 0, 0, 2 * Math.PI);


       this._pathCommands.forEach(function(cmd){
           switch(cmd.code) {
               case 'M': ctx.moveTo(cmd.x, cmd.y);
               break;
               case 'L': ctx.lineTo(cmd.x, cmd.y);
                break;
               case 'C': ctx.bezierCurveTo(cmd.x1, cmd.y1, cmd.x2, cmd.y2, cmd.x, cmd.y);
                   break;
               case 'Z': ctx.closePath();
               break;
           }
       });


        ctx.closePath();

    }

    toJSON() {
        let json = super.toJSON();
        json.pathCommands = this._pathCommands;
        /*this._pathCommands.forEach(function(cmd){
            json.pathCommands.push(cmd)
        });*/
        log.debug("Converting ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJSON(obj) {
        log.debug("Converting from JSON: ", obj);
        super.fromJSON(obj);
        this._pathCommands = obj.pathCommands;
    }
}