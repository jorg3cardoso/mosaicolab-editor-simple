import {FetchLocal} from "../../utility/FetchLocal";
import * as loglevel from '../../libraries/loglevel.js';

import {Cmd} from "../imports";
import {Mosaic} from "../../mosaic/mosaic";
import {MosaicPainter} from "../../painter/mosaic-painter";
import {TesselShapeFactory} from "../../mosaic/tessel-shape-factory";
import {TesselShapePath} from "../../mosaic/tessel-shape-path";
import {MosaicLayoutFactory} from "../../layout/mosaic-layout-factory";
import {MosaicLayout} from "../../layout/mosaic-layout";
import {CmdMosaicCreate} from "../mosaic/cmd-mosaic-create";

export {CmdGUIImportSVG}

var log = loglevel.getLogger("cmd.gui");

class CmdGUIImportSVG extends CmdMosaicCreate {

    constructor() {
        super();
        this._fileURI = null;
        this._data = null;
        this._numberOfTessels = 1;
    }

    get name() {
        return CmdGUIImportSVG.name;
    }


    get fileURI() {
        return this._fileURI;
    }

    set fileURI(value) {
        this._fileURI = value;
    }

    get data() {
        return this._data;
    }

    set data(data) {
        this._data = data;
    }

    _dodo() {
        //super._dodo();
        log.debug("Importing SVG");
        let _this = this;

        let data = null;
        if (this._fileURI) {
            let a = _this.fileURI.substring(_this.fileURI.indexOf(","))

            let b = atob(_this.fileURI.substring(_this.fileURI.indexOf(",")+1));
            console.log(b);
            data = b;
        } else if (this._data) {
            data = this._data;
        }



        let doc = new DOMParser();
        let svgDoc = doc.parseFromString(_this._data, "image/svg+xml");

        _this._createFromSVG(svgDoc);

        log.debug("Imported SVG: ", this._fileURI);
    }

    _createFromSVG(svgDoc) {
        log.debug("Creating mosaic from svg" );
        var {parseSVG, makeAbsolute} = require('svg-path-parser');

        let _this = this;
        let mosaic = new Mosaic();
        mosaic.painter = new MosaicPainter();
        mosaic.startBulkUpdate();
        let paths = svgDoc.querySelectorAll('svg path');
        this.numberOfTessels = paths.length;
        for (let i = 0; i < this.numberOfTessels; i++) {
            let path = paths[i];

            let pathCommands = makeAbsolute(parseSVG(path.getAttribute('d')));
            console.log(JSON.stringify(pathCommands))
            let pos = _this._translateToZero(pathCommands);

            let tessel = _this._mosaicView.defaultTile;
            tessel.x = pos.x;
            tessel.y = pos.y;
            tessel.w = pos.w;
            tessel.h = pos.h;
            tessel.shape = TesselShapeFactory.create(TesselShapePath.name);
            tessel.shape.pathCommands =   pathCommands;
            console.log(pathCommands)
            mosaic.addTessel(tessel);
        };

        //}

        mosaic.endBulkUpdate();

        this._mosaicView.mosaicLayout = MosaicLayoutFactory.create(MosaicLayout.name);
        this._mosaicView.mosaic = mosaic;

        log.debug("Created mosaic: ", mosaic );
    }

    _translateToZero(commands) {

        // find BB
        let minX = Infinity;
        let maxX = -Infinity;
        let minY = Infinity;
        let maxY = -Infinity;
        commands.forEach(function(cmd) {
            if (cmd.x < minX) {
                minX = cmd.x;
            }
            if (cmd.x > maxX) {
                maxX = cmd.x;
            }
            if (cmd.x1 < minX) {
                minX = cmd.x1;
            }
            if (cmd.x1 > maxX) {
                maxX = cmd.x1;
            }
            if (cmd.x2 < minX) {
                minX = cmd.x2;
            }
            if (cmd.x2 > maxX) {
                maxX = cmd.x2;
            }

            if (cmd.y < minY) {
                minY = cmd.y;
            }
            if (cmd.y > maxY) {
                maxY = cmd.y;
            }
            if (cmd.y1 < minY) {
                minY = cmd.y1;
            }
            if (cmd.y1 > maxY) {
                maxY = cmd.y1;
            }
            if (cmd.y2 < minY) {
                minY = cmd.y2;
            }
            if (cmd.y2 > maxY) {
                maxY = cmd.y2;
            }
        });


        let dx = minX; //commands[0].x;
        let dy = minY;// commands[0].y;

        console.log("Translating by ", dx, " ", dy)

        commands.forEach(function(cmd) {
            cmd.x -= dx;
            cmd.y -= dy;
            cmd.x0 -= dx;
            cmd.y0 -= dy;
            cmd.x1 -= dx;
            cmd.y1 -= dy;
            cmd.x2 -= dx;
            cmd.y2 -= dy;
        });

        /*let sx = 600/(maxX-minX);
        let sy = 400/(maxY-minY);

        commands.forEach(function(cmd) {
            cmd.x *= sx;
            cmd.y *= sy;
            cmd.x1 *= sx;
            cmd.y1 *= sy;
            cmd.x2 *= sx;
            cmd.y2 *= sy;
        });*/

        return {x: dx, y: dy, w: maxX-minX, h: maxY-minY};
    }

    toJS() {
        let json = {cmdName: this.name, fileURI: this._fileURI, data: this._data};
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        super.fromJS(obj)

        this.fileURI = obj.fileURI;
        this.data = obj.data;
        return this;
    }


    toString() {
        return "CmdGUIImportSVG [persistence:" + this._persistence + ", fileURI:" + this._fileURI + ", data:" + this._data + "]";
    }


    /* Private methods */
    _check() {
        if (!this._fileURI && !this._data) {
            log.error(this.toString());
            throw("No file URI or data provided, cannot execute command");
        }
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
    }
}