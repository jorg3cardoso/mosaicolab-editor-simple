export {CmdMosaicViewSetCanvasScale}
import {CmdUndoable} from "../cmd-undoable";
import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.view");

class CmdMosaicViewSetCanvasScale extends CmdUndoable {
    constructor() {
        super();
        this._mosaicView = null;
        this._canvasScale = null;

        this._previousCanvasScale = null;

    }
    get name() {
        return CmdMosaicViewSetCanvasScale.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }


    get canvasScale() {
        return this._canvasScale;
    }

    set canvasScale(value) {
        this._canvasScale = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Changing canvas scale to: ", this._canvasScale);
        this._previousCanvasScale = this._mosaicView.canvasScale;

        this._mosaicView.canvasScale = this._canvasScale;

        log.debug("Changed canvas scale to: ", this._canvasScale);
    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {canvasScale: this._canvasScale});

        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);
        this.canvasScale = obj.canvasScale;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdMosaicViewSetCanvasScale();
        undo.mosaicView = this._mosaicView;
        undo.canvasScale = this._previousCanvasScale;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousCanvasScale === this._canvasScale) {
            return false;
        } else {
            return true;
        }
    }



    toString() {
        return "CmdMosaicViewSetCanvasScale [mosaicView:" + this._mosaicView + ", canvasScale: " + this._canvasScale + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }

        if (this._canvasScale === null || this._canvasScale === undefined) {
            log.error(this.toString());
            throw("No canvas scale has been set, cannot execute command.");
        }

    }
}