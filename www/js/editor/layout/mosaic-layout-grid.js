export {MosaicLayoutGrid}
import {MosaicLayout} from "./mosaic-layout";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("layout");

/**
 * Lays tessels in a grid of cols x rows. The column width and row height are taken from the first
 * tile of the mosaic. If tessels are not all of the same size, this may cause overlaps.
 *
 */
class MosaicLayoutGrid extends MosaicLayout {
    constructor() {
        super();
        this._params = null;
        this._cols = null;
        this._rows = null;
        this._margin = null;
    }

    set params(params) {
        this._params = params;
        if (params[0] === undefined || params[0] === null) {
            log.error(params);
            throw "'cols' parameter not set!"
        }
        if (params[1] === undefined || params[1] === null) {
            log.error(params);
            throw "'rows' parameter not set!"
        }
        if (params[2] === undefined || params[2] === null) {
            log.error(params);
            throw "'margin' parameter not set!"
        }
        this._cols = params[0];
        this._rows = params[1];
        this._margin = params[2];
    }

    get params() {
        return this._params;
    }
    get name() {
        return MosaicLayoutGrid.name;
    }

    doLayout(mosaic) {
        mosaic.startBulkUpdate();
        super.doLayout(mosaic);

        log.debug("Doing layout ", this._cols, " x ", this._rows);
        let tessels = mosaic.tessels;
        if (tessels.length !== (this._cols*this._rows)) {
            log.warn("Number of tessels does not match number of cols * rows!");
        }
        if (tessels.length === 0) {
            log.warn("Zero tessels to lay out.");

        } else {
            let colWidth = tessels[0].w;
            let rowHeight = tessels[0].h;
            //log.debug("Tessel size: ", colWidth, " x ", rowHeight);
            let effectiveRows = Math.ceil(tessels.length / this._cols);

            let i = 0;
            for (let row = 0; row < effectiveRows && i < tessels.length; row++) {
                for (let col = 0; col < this._cols && i < tessels.length; col++) {

                    tessels[i].x = col*(colWidth+this._margin);
                    tessels[i].y = row*(rowHeight+this._margin);
                    //console.log(i, tessels[i])
                    i++;

                }
            }
        }


        mosaic.endBulkUpdate();
    }

    toJSON() {
        return {name: MosaicLayoutGrid.name, cols: this._cols, rows: this._rows, margin: this._margin};
    }

    fromJSON(obj) {
        log.trace("Creating from JSON: ", MosaicLayoutGrid.name);
        if (obj.name !== MosaicLayoutGrid.name) {
            throw "Wrong layout name. Was expecting '" + MosaicLayout.name + "'. Got '" + obj.name + "' instead.";
        }
        this.params = [obj.cols, obj.rows, obj.margin];
    }
}