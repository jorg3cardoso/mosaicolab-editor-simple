import {HTMLObserverObservable} from "./html-observer-observable";

export {ViewPortSelector}

import * as loglevel from '../../libraries/loglevel.js';
import {Dimension} from "../../utility/dimension";
import {CmdMosaicViewSetViewport} from "../../cmd/view/cmd-mosaic-view-set-viewport";
import {CmdMosaicViewSetCanvasScale} from "../../cmd/view/cmd-mosaic-view-set-canvas-scale";
import {CmdList} from "../../cmd/imports";
import {CmdMosaicViewSetCanvasViewPort} from "../../cmd/view/cmd-mosaic-view-set-canvas-view-port";
import {Vec2} from "../../utility/vec2";
var log = loglevel.getLogger("gui.html");

class ViewPortSelector extends HTMLObserverObservable {
    constructor() {
        super();
        this._viewPort = null;
    }

    set viewPort(viewPort) {
        this._viewPort = viewPort;

        this._buildHTML();
    }

    _buildHTML() {
        this.innerHTML = "";
        for (let r = 0; r < this._viewPort.rows; r++) {
            let rowEl = document.createElement("div");
            rowEl.classList.add("row", "no-gutters");
            this.appendChild(rowEl);
            for (let c = 0; c < this._viewPort.cols; c++) {
                let colEl = document.createElement("div");
                colEl.classList.add("col");
                let button = document.createElement("button");
                button.dataset.x = c / this._viewPort.cols;
                button.dataset.w = 1 / this._viewPort.cols;
                button.dataset.y = r / this._viewPort.rows;
                button.dataset.h = 1 / this._viewPort.rows;
                button.classList.add("btn");
                if (r === 0) {
                    if (c === 0) {
                        button.innerHTML = "<i style=\"transform: rotate(45deg);\" class=\"fas fa-arrow-left\"></i>";
                    } else if (c === this._viewPort.cols - 1) {
                        button.innerHTML = "<i style=\"transform: rotate(45deg);\" class=\"fas fa-arrow-up\"></i>";
                    } else {
                        button.innerHTML = "<i  class=\"fas fa-arrow-up\"></i>";
                    }
                } else if (r === this._viewPort.rows - 1) {
                    if (c === 0) {
                        button.innerHTML = "<i style=\"transform: rotate(45deg);\" class=\"fas fa-arrow-down\"></i>";
                    } else if (c === this._viewPort.cols - 1) {
                        button.innerHTML = "<i style=\"transform: rotate(45deg);\" class=\"fas fa-arrow-right\"></i>";
                    } else {
                        button.innerHTML = "<i  class=\"fas fa-arrow-down\"></i>";
                    }
                } else {
                    if (c === 0) {
                        button.innerHTML = "<i  class=\"fas fa-arrow-left\"></i>";
                    } else if (c == this._viewPort.cols - 1) {
                        button.innerHTML = "<i  class=\"fas fa-arrow-right\"></i>";
                    } else {

                    }
                }

                button.setAttribute("type", "button");
                colEl.appendChild(button);
                rowEl.appendChild(colEl);
            }
        }
        let _this = this;

        this.addEventListener("click", function (evt) {
            if (evt.target.nodeName === "BUTTON" || evt.target.nodeName === "I") {
                let btn = evt.target;
                if (evt.target.nodeName === "I") {
                    btn = evt.target.parentElement;
                }
                if (btn.classList.contains("btn-primary")) {
                    // Toggle view port control and get back to full view
                    btn.classList.remove("btn-primary");
                    let cmd = new CmdMosaicViewSetViewport();
                    cmd.mosaicViewPort = new Dimension(0, 0, 1, 1);

                    _this.notifyObservers(cmd.toJS());
                } else {
                    this.querySelectorAll("button").forEach(function (btn) {
                        btn.classList.remove("btn-primary");
                    });
                    btn.classList.add("btn-primary");
                    let cmdList = new CmdList();
                    let cmd = new CmdMosaicViewSetViewport();
                    cmd.mosaicViewPort = new Dimension(
                        btn.dataset.x,
                        btn.dataset.y,
                        btn.dataset.w,
                        btn.dataset.h);
                    cmdList.addCommand(cmd);

                    let cmdScale = new CmdMosaicViewSetCanvasScale();
                    cmdScale.canvasScale = 1;
                    cmdList.addCommand(cmdScale);

                    let cmdCanvasViewPort = new CmdMosaicViewSetCanvasViewPort();
                    cmdCanvasViewPort.canvasViewPort = new Vec2(0, 0);
                    cmdList.addCommand(cmdCanvasViewPort);

                    _this.notifyObservers(cmdList.toJS());
                    // Reset scale and shift that user may have introduced
                    //_this._mosaicView.canvasScale = 1;
                    //_this._mosaicView.canvasViewPort = new Vec2(0, 0);
                }
            }


        });
    }

}

customElements.define('mosaic-viewport-selector', ViewPortSelector);

