export {TesselShapeRoundRect}
import {TesselShape} from "./tessel-shape";


import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("mosaic");
class TesselShapeRoundRect extends TesselShape {
    constructor() {
        super();
    }

    get name() {
        return TesselShapeRoundRect.name;
    }

    doPath(ctx, w, h) {
        let x = 0;
        let y = 0;
        let radius = 0.1 * w;
        ctx.beginPath();
        ctx.moveTo(x + radius, y);
        ctx.lineTo(x + w - radius, y);
        ctx.quadraticCurveTo(x + w, y, x + w, y + radius);
        ctx.lineTo(x + w, y + h - radius);
        ctx.quadraticCurveTo(x + w, y + h, x + w - radius, y + h);
        ctx.lineTo(x + radius, y + h);
        ctx.quadraticCurveTo(x, y + h, x, y + h - radius);
        ctx.lineTo(x, y + radius);
        ctx.quadraticCurveTo(x, y, x + radius, y);
        ctx.closePath();
    }
    /*
        isPointInPath(x, y, w, h) {
            let path = new Path2D();
            let radius = 0.1 * w;
            path.beginPath();
            path.moveTo(0 + radius, 0);
            path.lineTo(0 + w - radius, 0);
            path.quadraticCurveTo(0 + w, 0, 0 + w, 0 + radius);
            path.lineTo(0 + w, 0 + h - radius);
            path.quadraticCurveTo(0 + w, 0 + h, 0 + w - radius, 0 + h);
            path.lineTo(0 + radius, 0 + h);
            path.quadraticCurveTo(0, 0 + h, 0, 0 + h - radius);
            path.lineTo(0, 0 + radius);
            path.quadraticCurveTo(0, 0, 0 + radius, 0);
            path.closePath();
            return false;
        }*/

    toJSON() {
        let json = super.toJSON();

        log.trace("Converting ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJSON(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJSON(obj);
    }
}