export {CmdMosaicViewSetViewport}

import {Dimension} from "../../utility/dimension";


import {CmdUndoable} from "../cmd-undoable";
import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.view");

class CmdMosaicViewSetViewport extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._mosaicViewPort = null;

        this._previousViewPort = null;

    }
    get name() {
        return CmdMosaicViewSetViewport.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get mosaicViewPort() {
        return this._mosaicViewPort;
    }

    set mosaicViewPort(value) {
        this._mosaicViewPort = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Changing mosaic view port to: ", this._mosaicViewPort);


        this._previousViewPort = this._mosaicView.mosaicViewPort;
        //console.log(this._previousViewPort);
        this._mosaicView.mosaicViewPort = this._mosaicViewPort;


        log.debug("Changed mosaic view port to: ", this._mosaicViewPort);
    }

    toJS() {
        let json = super.toJS();
        //Object.assign(json, {mosaicLayoutName: this._mosaicLayoutName, params: this._params});

        Object.assign(json, {mosaicViewPort: this._mosaicViewPort.toJSON()});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);


        this.mosaicViewPort = new Dimension().fromJSON(obj.mosaicViewPort);
        return this;
    }

    getUndoCommand() {
        let undo = new CmdMosaicViewSetViewport();
        undo.mosaicView = this._mosaicView;
        undo.mosaicViewPort = this._previousViewPort;

        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousViewPort === this._mosaicViewPort) {
            return false;
        } else {
            return true;
        }
    }



    toString() {
        return "CmdMosaicViewSetViewport [mosaicView:" + this._mosaicView + ", mosaicViewPort: " + this._mosaicViewPort.toString + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set; cannot execute command.");
        }
        if (!this._mosaicViewPort) {
            log.error(this.toString());
            throw("No Viewport has been set; cannot execute command.");
        }
        if (!(this._mosaicViewPort instanceof Dimension)) {
            log.error(this._mosaicViewPort);
            throw("Viewport is not a Dimension; cannot execute command.");
        }

    }
}