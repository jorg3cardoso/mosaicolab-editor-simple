import {CmdUndoable} from "../cmd-undoable";
import {Mosaic} from "../../mosaic/mosaic";
import {MosaicPainter} from "../../painter/mosaic-painter";

import * as loglevel from '../../libraries/loglevel.js';
import {TesselShapeFactory} from "../../mosaic/tessel-shape-factory";
import {TesselShapePath} from "../../mosaic/tessel-shape-path";
import {MosaicLayout} from "../../layout/mosaic-layout";
import {MosaicLayoutFactory} from "../../layout/mosaic-layout-factory";
import {CmdMosaicCreate} from "./cmd-mosaic-create";

export {CmdMosaicCreateFromSVG}

var log = loglevel.getLogger("cmd.mosaic");

class CmdMosaicCreateFromSVG extends CmdMosaicCreate {
    constructor() {
        super();
        this._mosaicView = null;
        this._numberOfTessels = null;
    }

    get name() {
        return CmdMosaicCreateFromSVG.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get numberOfTessels() {
        return this._numberOfTessels;
    }

    set numberOfTessels(value) {
        this._numberOfTessels = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Creating mosaic with from svg" );
        var {parseSVG, makeAbsolute} = require('svg-path-parser');

        let _this = this;
        let mosaic = new Mosaic();
        mosaic.painter = new MosaicPainter();
        mosaic.startBulkUpdate();
        let paths = document.querySelectorAll('svg path');
        this._numberOfTessels = paths.length;
        for (let i = 0; i < this._numberOfTessels; i++) {
            let path = paths[i];

            let pathCommands = makeAbsolute(parseSVG(path.getAttribute('d')));
            let pos = _this._translateToZero(pathCommands);

            let tessel = _this._mosaicView.defaultTile;
            tessel.x = pos.x;
            tessel.y = pos.y;
            tessel.w = pos.w;
            tessel.h = pos.h;
            tessel.shape = TesselShapeFactory.create(TesselShapePath.name);
            tessel.shape.pathCommands =   pathCommands;
            mosaic.addTessel(tessel);
        };

        //}

        mosaic.endBulkUpdate();

        this._mosaicView.mosaicLayout = MosaicLayoutFactory.create(MosaicLayout.name);
        this._mosaicView.mosaic = mosaic;

        log.debug("Created mosaic: ", mosaic );

    }

    _translateToZero(commands) {

        // find BB
        let minX = Infinity;
        let maxX = -Infinity;
        let minY = Infinity;
        let maxY = -Infinity;
        commands.forEach(function(cmd) {
            if (cmd.x < minX) {
                minX = cmd.x;
            }
            if (cmd.x > maxX) {
                maxX = cmd.x;
            }
            if (cmd.x1 < minX) {
                minX = cmd.x1;
            }
            if (cmd.x1 > maxX) {
                maxX = cmd.x1;
            }
            if (cmd.x2 < minX) {
                minX = cmd.x2;
            }
            if (cmd.x2 > maxX) {
                maxX = cmd.x2;
            }

            if (cmd.y < minY) {
                minY = cmd.y;
            }
            if (cmd.y > maxY) {
                maxY = cmd.y;
            }
            if (cmd.y1 < minY) {
                minY = cmd.y1;
            }
            if (cmd.y1 > maxY) {
                maxY = cmd.y1;
            }
            if (cmd.y2 < minY) {
                minY = cmd.y2;
            }
            if (cmd.y2 > maxY) {
                maxY = cmd.y2;
            }
        });


        let dx = minX; //commands[0].x;
        let dy = minY;// commands[0].y;


        commands.forEach(function(cmd) {
            cmd.x -= dx;
            cmd.y -= dy;
            cmd.x1 -= dx;
            cmd.y1 -= dy;
            cmd.x2 -= dx;
            cmd.y2 -= dy;
        });

        return {x: dx, y: dy, w: maxX-minX, h: maxY-minY};
    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {numberOfTessels: this._numberOfTessels});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.numberOfTessels = obj.numberOfTessels;
        return this;
    }


    toString() {
        return "CmdMosaicCreateFromSVG [mosaicView:" + this._mosaicView + ", numberOfTessels: " + this._numberOfTessels + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (!this._numberOfTessels) {
            log.error(this.toString());
            throw("No number of tessels provided, cannot execute command.");
        }

    }
}