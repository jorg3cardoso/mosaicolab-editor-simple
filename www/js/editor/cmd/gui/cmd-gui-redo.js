export {CmdGUIRedo}

import {Cmd} from "../imports";
import {CmdGUIUndo} from "./cmd-gui-undo";

import * as loglevel from '../../libraries/loglevel';

var log = loglevel.getLogger("cmd");

/**
 * GUI Command Redo
 */
class CmdGUIRedo extends Cmd{

    constructor() {
        super();
    }

    get name() {
        return CmdGUIRedo.name;
    }


    get editor() {
        return this._editor;
    }

    set editor(value) {
        this._editor = value;
    }


    _dodo() {
        super._dodo();
        this._hadEffect = this._editor._commandHistory.redo();
        if (!this._hadEffect) {
            log.info("Nothing to redo");
        } else {
            log.debug("Redid last command");
        }


    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        return this;
    }

    getUndoCommand() {
        let undo = new CmdGUIUndo();
        undo.editor = this._editor;

        return undo;

    }

    hadEffect() {
        super.hadEffect();

        return this._hadEffect;
    }



    toString() {
        return "CmdGUIRedo[]";
    }


    /* Private methods */
    _check() {
        if (!this._editor) {
            log.error(this.toString());
            throw("No MosaicEditor has been set, cannot execute command.");
        }
    }
}