import {FetchLocal} from "../utility/FetchLocal";

export {MosaicColorPalette}

import {MosaicSolidColor} from "./mosaic-color.js";
import {MosaicTextureColor} from "./mosaic-color.js";
import {ObservableObserver} from "../observer/observer-observable.js";
import {MosaicColorPaletteSelector} from "./mosaic-color-palette-selector.js";

import * as loglevel from '../libraries/loglevel.js';

var log = loglevel.getLogger("color");

class MosaicColorPalette extends ObservableObserver {
    constructor(htmlElementSelector, config) {
        super();
        log.debug("Constructing MosaicColorPalette");
        this._paletteName = "";
        this._colors = new Array();
        this._selectedColorIndex = -1;
        this._htmlElementSelector = htmlElementSelector;
        this._config = config;

        if (config) {
            this._configure(config);
        }
    }

    get paletteName() {
        return this._paletteName;
    }

    get selectedColorIndex() {
        // this._selectedColorIndex = Math.floor(Math.random()*this._colors.length);
        //console.log(this._selectedColor);
        if (this._selectedColorIndex == -1) return null;
        return this._selectedColorIndex;
    }

    get selectedColor() {
        // this._selectedColorIndex = Math.floor(Math.random()*this._colors.length);
        //console.log(this._selectedColor);
        if (this._selectedColorIndex == -1) return null;
        return this._colors[this._selectedColorIndex];
    }

    set colorPaletteSelector(colorPaletteSelector) {
        colorPaletteSelector.addObserver(this);
        this._loadColorPaletteConfig(colorPaletteSelector.value);
    }

    /**
     * ObservableObserver
     */
    update(observable, params) {
        //console.log(observable, params);
        log.trace(observable, params);
        if (observable instanceof MosaicColorPaletteSelector) {
            this._loadColorPaletteConfig(params);
        }

    }


    colorFromIndex(index) {
        if (index === null) return null;
        return this._colors[index];
    }

    getColorById(id) {
        for (let i = 0; i < this._colors.length; i++) {
            if (this._colors[i].id === id) return this._colors[i];
        }
        return null;
    }

    toJSON() {
        log.debug("Converting MosaicColorPalette to JSON (", this._colors.length, " colors).");
        let solidColors = [];
        let textureColors = [];
        let _this = this;

        this._colors.forEach(function (color) {
            if (color instanceof MosaicSolidColor) {
                solidColors.push(_this._rgbToHex(color.red, color.green, color.blue));
            } else if (color instanceof MosaicTextureColor) {
                textureColors.push(color.imgUrl);
            }
        });

        let json = JSON.stringify({name: this._paletteName, solidColors: solidColors, textures: textureColors});
        log.trace("JSON: ", json);
        return json;
    }

    fromJSON(jsonState) {
        log.debug("Loading color palette from JSON.");
        log.trace("JSON: ", jsonState);
        if (!jsonState) {
            log.error("Invalid JSON data.");
            return;
        }

        let config = JSON.parse(jsonState);
        this._configure(config);

    }

    _configure(config) {
        log.debug("Parsing color palette data.");
        log.trace("Parsing color palette data.")
        if (config.name) {
            this._paletteName = config.name;
        } else {
            this._paletteName = "";
        }
        log.info("Loading palette '", this._paletteName, "'");
        this._colors = new Array();
        if (config.solidColors) {

            config.solidColors.forEach(function (hex) {
                let rgb = this._hexToRgb(hex);
                let solidColor = new MosaicSolidColor(rgb.r, rgb.g, rgb.b);
                log.info("Parsed:", solidColor.toString());
                this._colors.push(solidColor);

            }.bind(this));
        } else {
            log.debug("No solid colors to parse.");
        }

        let promises = [];
        let promisesCreateImageBitmap = [];
        let numberColors = this._colors.length;
        if (config.textures) {

            config.textures.forEach(function (imgUrl, index) {
                // add element to array
                this._colors.push(null);
                let promise = FetchLocal.getInstance().checkImage(imgUrl);
                promises.push(promise);

                promise.then(function (img) {
                    log.info("Loaded image:", img.currentSrc);

                    promisesCreateImageBitmap.push(createImageBitmap(img).then(function (bmp) {
                        let textureColor = new MosaicTextureColor(bmp, img.currentSrc);
                        log.debug("Parsed: ", textureColor.toString(), index + numberColors);

                        // Keep palette in the same order as in the config file
                        this._colors[index + numberColors] = textureColor;//new MosaicTextureColor(bmp, imgUrl);

                    }.bind(this)))
                }.bind(this));
            }.bind(this));

        } else {
            log.debug("No textures to load");
        }

        if (promises.length > 0) {
            Promise.all(promises).then(function () {
                Promise.all(promisesCreateImageBitmap).then(function () {
                    this._initHTML();
                    this.notifyObservers();
                }.bind(this));

            }.bind(this));
        } else {
            this._initHTML();
            this.notifyObservers();
        }
        this._selectedColorIndex = 0;

    }

    _loadColorPaletteConfig(configURl) {
        let config = this._config;
        FetchLocal.getInstance().fetch(configURl).then(function (response) {
            return response.json();
        }, function (fail) {
            console.log(fail);
        }).then(function (json) {
            config = json;
        }, function (fail) {
            console.log(fail);
        }).finally(function () {
            this._configure(config);
        }.bind(this));
    }
/*

    _fetchLocal(url) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest
            xhr.onload = function () {
                resolve(new Response(xhr.responseText, {status: xhr.status}))
            }
            xhr.onerror = function () {
                reject(new TypeError('Local request failed'))
            }
            xhr.open('GET', url)
            xhr.send(null)
        })
    }
*/
/*
    _checkImage(imgSrc) {
        log.debug("Checking image", imgSrc);
        return new Promise(function (resolve, reject) {
                const img = new Image();
                img.onload = function () {
                    resolve(img);
                }
                img.onerror = function (error) {
                    log.error("Error loading image", error);
                    reject();
                }
                img.src = imgSrc;
            }
        );
    }*/

    _initHTML() {
        log.debug("Initing HTML [" + this._htmlElementSelector + "]");

        let elems = document.querySelectorAll(this._htmlElementSelector);
        elems.forEach(function (elem) {
            //console.log(elem)
            elem.innerHTML = "";
            let c = document.createElement("span");
            c.classList.add("colorPalette__button", "fas", "fa-eraser", "fa-2x");
            c.innerHTML = "";//<i class=\"fas fa-eraser fa-2x\"></i>";
            elem.appendChild(c);
            this._colors.forEach(function (item, index) {
                log.debug("Adding color ", item, " to HTML");
                let colorElem = document.createElement("span");
                colorElem.classList.add("colorPalette__button");
                if (index === 0) {
                    colorElem.classList.add("colorPalette__button--active");
                }
                if (item instanceof MosaicSolidColor) {
                    colorElem.style.backgroundColor = "rgb(" + item.red + ", " + item.green + ", " + item.blue + ")";
                } else if (item instanceof MosaicTextureColor) {

                    colorElem.style.backgroundImage = "url(" + item._imgUrl + ")";
                    colorElem.style.backgroundSize = "100%";
                }

                elem.appendChild(colorElem);
            });


            let _this = this;
            elem.addEventListener("click", function (evt) {
                log.debug("Color pallette clicked.");
                if (evt.target.classList.contains("colorPalette__button")) {
                    elem.querySelectorAll(".colorPalette__button").forEach(function (item) {
                        item.classList.remove("colorPalette__button--active");
                    });
                    evt.target.classList.add("colorPalette__button--active");
                    let i = -1;
                    let child = evt.target;
                    while ((child = child.previousSibling) != null) i++;
                    _this._selectedColorIndex = i;
                }
            })
        }.bind(this));


    }

    // from: https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
    _hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    _rgbToHex(r, g, b) {
        return "#" + ("0" + r.toString(16)).substr(-2) + ("0" + g.toString(16)).substr(-2) + ("0" + b.toString(16)).substr(-2);
    }

    toString() {
        return "MosaicColorPalette [name: " + this._paletteName + ", number of color: " + this._colors.length + "]";
    }
}