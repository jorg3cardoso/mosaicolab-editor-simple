import {Observable} from "../observer/observer-observable";

export {Tessel}

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("mosaic");

class Tessel extends Observable {

    constructor() {
        super();
        this._id = Tessel._nextId;
        Tessel._nextId++;
        this._x = 0;
        this._y = 0;
        this._w = 0;
        this._h = 0;
        this._colorIndex = null;
        this._shape = null;
        this._painter = null;
    }
    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
        if (Tessel._nextId <= value) {
            Tessel._nextId = value+1;
        }
    }

    get x() {
        return this._x;
    }

    set x(value) {
        if (value !== this._x) {
            this._x = value;
            this.notifyObservers({x:true});
        }
    }

    get y() {
        return this._y;
    }

    set y(value) {
        if (value !== this._y) {
            //console.log(value, this._y);
            this._y = value;
            this.notifyObservers({y:true});
        }
    }

    get w() {
        return this._w;
    }

    set w(value) {
        if (value !== this._w) {
            this._w = value;
            this.notifyObservers({w:true});
        }
    }

    get h() {
        return this._h;
    }

    set h(value) {
        if (value !== this._h) {
            this._h = value;
            this.notifyObservers({h:true});
        }
    }

    get colorIndex() {
        return this._colorIndex;
    }

    set colorIndex(value) {
        if (value !== this._colorIndex) {
            this._colorIndex = value;
            this.notifyObservers({color:true});
        }
    }

    get shape() {
        return this._shape;
    }

    set shape(value) {
        if (value !== this._shape) {
            this._shape = value;
            this.notifyObservers({shape:true});
        }
    }

    get painter() {
        return this._painter;
    }

    set painter(value) {
        if (value !== this._painter) {
            this._painter = value;
            this.notifyObservers({painter:true});
        }
    }


    paint(ctx, palette) {
        if (this._painter === null) {
            log.error("Tessel has no painter assigned");
        } else {
            this._painter.paint(ctx, palette, this);
        }
    }

    isPointInPath(ctx, x, y) {
        let path = new Path2D();
        this._shape.doPath(path, this._w, this._h);
        //console.log("ispointin path ", x-this._x, y-this._y);
        return ctx.isPointInPath(path, x-this._x, y-this._y);
    }

    toString() {
        return "Tessel[id: " +this._id+ ", x:" + this._x + ", y:" + this._y + ", w:" + this._w + ", h:" + this._h+"]";
    }
}

Tessel._nextId = 0;