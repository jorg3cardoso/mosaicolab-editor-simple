import * as loglevel from '../libraries/loglevel.js';

export {FetchLocal}

var log = loglevel.getLogger("utility");

class FetchLocal {
    constructor() {
        if (!FetchLocal._instance) {
            FetchLocal._instance = this;
            FetchLocal.baseUrl = "";
        }
        return FetchLocal._instance;
    }
    static getInstance() {
        return new FetchLocal();
    }

    fetch(url) {
        let _this = this;

        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest
            xhr.onload = function() {
                resolve(new Response(xhr.responseText, {status: xhr.status}))
            }
            xhr.onerror = function() {
                reject(new TypeError('Local request failed'))
            }

            let urlToFetch = url;
            if (!url.startsWith("http") && !url.startsWith("/")) {
                urlToFetch = _this.baseUrl + url;
            }

            log.debug("Fetching: ", urlToFetch);
            xhr.open('GET', urlToFetch);
            xhr.send(null)
        })
    }

    fetchJSON(url) {
        return this.fetch(url).then(function (response) {
            return response.json();
        }, function(fail) {
            log.error(fail);
            return reject(fail);
        });
    }


    checkImage(imgSrc) {
        let _this = this;

        log.debug("Checking image", imgSrc);
        return new Promise(function (resolve, reject) {
                const img = new Image();
                img.onload = function () {
                    resolve(img);
                }
                img.onerror = function (error) {
                    log.error("Error loading image", error);
                    reject();
                }

            let urlToFetch = imgSrc;
            if (!urlToFetch.startsWith("http") && !urlToFetch.startsWith("/")) {
                urlToFetch = _this.baseUrl + imgSrc;
            }

                img.src = urlToFetch;
            }
        );
    }
}
