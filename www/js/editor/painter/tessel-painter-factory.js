import {
    ClearTesselPainter,
    OutlineTesselPainter,
    SelectedTesselPainter,
    SolidTesselPainter
} from "./tessel-painter";

export {TesselPainterFactory}

import * as loglevel from '../libraries/loglevel.js';
import {TesselPainterComposite} from "./tessel-painter-composite";
var log = loglevel.getLogger("painter");

class TesselPainterFactory {

    constructor() {
        if (!TesselPainterFactory._instance) {
            TesselPainterFactory._instance = this;

        this._painters = {};
        this._painters[ClearTesselPainter.name] = new ClearTesselPainter();
        this._painters[OutlineTesselPainter.name] = new OutlineTesselPainter();
        this._painters[SelectedTesselPainter.name] = new SelectedTesselPainter();
        this._painters[SolidTesselPainter.name] = new SolidTesselPainter();
        //this._painters[TesselPainterComposite.name] = new TesselPainterComposite();
        }
        return TesselPainterFactory._instance;
    }

    static getInstance() {
        return new TesselPainterFactory();
    }
    createTesselPainter(name) {
        if (this._painters[name]) {
            return this._painters[name];
        } else {
            throw "Unavailable Tessel Painter: " + name
        }

    }

}
/*
TesselPainterFactory.instanceCount = 0;
TesselPainterFactory.instance = new TesselPainterFactory();
*/