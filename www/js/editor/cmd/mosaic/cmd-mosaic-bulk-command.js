import {CmdList} from "../imports";
import * as loglevel from "../../libraries/loglevel";

export {CmdMosaicBulkCommand}


var log = loglevel.getLogger("cmd.mosaic");

class CmdMosaicBulkCommand extends CmdList {
    constructor() {
        super();
    }

    get name() {
        return CmdMosaicBulkCommand.name;
    }


    _predo() {
        super._predo();
        log.debug("Predo bulk of commands");
        log.debug("Starting bulk update on mosaic");
        this.mosaicView.mosaic.startBulkUpdate();
    }


    _postdo() {
        super._postdo();
        log.debug("Ending bulk update on mosaic");
        log.debug("Postdo bulk of commands");
        this.mosaicView.mosaic.endBulkUpdate();
    }

    getUndoCommand() {
        let undoList = new CmdMosaicBulkCommand();
        for (let i = this.commandList.length-1; i >= 0; i--) {
            //this.commandList[i].getUndoCommand();
            undoList.addCommand(this.commandList[i].getUndoCommand());
        }
        undoList.mosaicView = this._mosaicView;
        return undoList;
    }
    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
    }
}