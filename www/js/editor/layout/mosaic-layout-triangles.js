import {TesselShapeTriangle} from "../mosaic/tessel-shape-triangle";

export {MosaicLayoutTriangles}
import {MosaicLayoutGrid} from "./mosaic-layout-grid";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("layout");

/**
 *
 *
 */
class MosaicLayoutTriangles extends MosaicLayoutGrid {
    constructor() {
        super();
        this._params = null;
        this._cols = null;
        this._rows = null;
    }

    set params(params) {
        this._params = params;
        this._cols = params[0];
        this._rows = params[1];
    }

    get params() {
        return this._params;
    }
    get name() {
        return MosaicLayoutTriangles.name;
    }

    doLayout(mosaic) {
        mosaic.startBulkUpdate();
        //super.doLayout(mosaic);

        log.debug("Doing layout ", this._cols, " x ", this._rows);
        let tessels = mosaic.tessels;
        if (tessels.length !== (this._cols*this._rows)) {
            log.warn("Number of tessels does not match number of cols * rows!");
        }
        if (tessels.length === 0) {
            log.warn("Zero tessels to lay out.");
        } else {
            let colWidth = tessels[0].w;
            let rowHeight = tessels[0].h;
            //console.log(colWidth, rowHeight);
            let effectiveRows = Math.ceil(tessels.length / this._cols);

            let i = 0;

            for (let row = 0; row < effectiveRows && i < tessels.length; row++) {
                let inverted = row % 2 == 0;
                for (let col = 0; col < this._cols && i < tessels.length; col++) {
                    if (!(tessels[i].shape instanceof TesselShapeTriangle)) {
                        log.warn("Found a tile which is not a triangle", tessels[i]);
                    }

                    tessels[i].x = col*(colWidth/2.0);
                    tessels[i].y = row*(rowHeight);
                    tessels[i].shape.inverted = inverted;
                    //console.log(inverted, tessels[i].shape.inverted);
                    inverted = !inverted;
                    i++;
                }
            }
            //console.log(tessels);
        }


        mosaic.endBulkUpdate();
    }

    toJSON() {
        return {name: MosaicLayoutTriangles.name, cols: this._cols, rows: this._rows};
    }

    fromJSON(obj) {
        log.trace("Creating from JSON: ", MosaicLayoutTriangles.name);
        if (obj.name !== MosaicLayoutTriangles.name) {
            throw "Wrong layout name. Was expecting '" + MosaicLayout.name + "'. Got '" + obj.name + "' instead.";
        }
        this.params = [obj.cols, obj.rows];
    }
}