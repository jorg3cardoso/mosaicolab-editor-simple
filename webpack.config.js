const path = require('path');

module.exports = {
    mode: 'development',
    entry: './www/js/editor/sketch.js',
    output: {
        path: path.resolve(__dirname, 'www'),
        filename: 'bundle.js'
    }
};