import {TesselShapeOval} from "../../mosaic/tessel-shape-oval";
import * as loglevel from '../../libraries/loglevel.js';
import {CmdMosaicBulkCommand} from "../imports";
import {CmdTileChangeShape} from "../tile/cmd-tile-change-shape";
import {TesselShapeRoundRect} from "../../mosaic/tessel-shape-round-rect";
import {TesselShapeTriangle} from "../../mosaic/tessel-shape-triangle";
import {CmdMosaicViewSetLayout} from "../view/cmd-mosaic-view-set-layout";
import {MosaicLayoutTriangles} from "../../layout/mosaic-layout-triangles";
import {MosaicLayoutGrid} from "../../layout/mosaic-layout-grid";
import {CmdMosaicViewSetDefaultTile} from "../view/cmd-mosaic-view-set-default-tile";

export {CmdGUIChangeShape}


var log = loglevel.getLogger("cmd.gui");

class CmdGUIChangeShape extends CmdMosaicBulkCommand {

    constructor() {
        super();
        this._mosaicView = null;
        this._shapeName = null;

        this._previousShapeNames = null;
    }

    get name() {
        return CmdGUIChangeShape.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }
    get shapeName() {
        return this._shapeName;
    }

    set shapeName(value) {
        this._shapeName = value;
    }

    _predo() {
        this.commandList = [];
        let shapeName;
        switch (this._shapeName) {
            case "Circles":
                shapeName =  TesselShapeOval.name;
                break;
            case "Rounded Squares":
                shapeName = TesselShapeRoundRect.name;
                break;
            case "Triangles":
                shapeName = TesselShapeTriangle.name;
                break;
        }
        let tile = this._mosaicView.defaultTile;

        let cmdChangeDefaultTile = new CmdMosaicViewSetDefaultTile();
        cmdChangeDefaultTile.mosaicView = this._mosaicView;
        cmdChangeDefaultTile.w = tile.w;
        cmdChangeDefaultTile.h = tile.h;
        cmdChangeDefaultTile.colorIndex = tile.colorIndex;
        cmdChangeDefaultTile.shapeName = shapeName;
        this.addCommand(cmdChangeDefaultTile);

        this._mosaicView.mosaic.tessels.forEach(function(tessel, index) {
            let cmdChangeShape = new CmdTileChangeShape();
            cmdChangeShape.mosaicView = this._mosaicView;
            cmdChangeShape.tesselId = tessel.id;

            cmdChangeShape.shapeName =  shapeName;

            this.addCommand(cmdChangeShape);
        }.bind(this));

        let cmdChangeLayout = new CmdMosaicViewSetLayout();
        cmdChangeLayout.mosaicView = this._mosaicView;

        if (this._shapeName === "Triangles") {
            cmdChangeLayout.mosaicLayoutName = MosaicLayoutTriangles.name;
            cmdChangeLayout.params = this._mosaicView.mosaicLayout.params;
        } else {
            cmdChangeLayout.mosaicLayoutName = MosaicLayoutGrid.name;
            cmdChangeLayout.params = [this._mosaicView.mosaicLayout.params[0], this._mosaicView.mosaicLayout.params[1], 5];
        }

        this.addCommand(cmdChangeLayout);
        super._predo();
    }

    toJS() {
        let json = {cmdName: this.name, shapeName: this._shapeName};
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        if (obj.cmdName !== this.name) {
            throw "Wrong command name. Was expecting '" + this.name + "'. Got '" + obj.name + "' instead.";
        }


        this.shapeName = obj.shapeName;
        return this;
    }


    toString() {
        return "CmdGUIChangeShape [mosaicView:" + this._mosaicView + ", shapeName:" + this._shapeName +"]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (!this._shapeName) {
            log.error(this.toString());
            throw("No shape name provided, cannot execute command");
        }

    }
}