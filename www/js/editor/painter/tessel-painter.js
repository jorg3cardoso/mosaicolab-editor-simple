import {MosaicSolidColor, MosaicTextureColor} from "../color/mosaic-color";

export {TesselPainter, ClearTesselPainter, OutlineTesselPainter, SolidTesselPainter, SelectedTesselPainter}

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("painter");

class TesselPainter {
    constructor() {

    }

    add(tesselPainter) {}
    remove(tesselPainter) {}
    getChildren() {}

    paint(ctx, palette, tessel) {
        this.beginPaint(ctx, palette, tessel);
        this.doPaint(ctx, palette,  tessel);
        this.endPaint(ctx, palette, tessel);
    }

    beginPaint(ctx, palette, tessel) {
        ctx.save();
        ctx.translate(tessel.x, tessel.y);
        tessel.shape.doPath(ctx, tessel.w, tessel.h);
        ctx.clip();
    }

    doPaint(ctx, palette, tessel){   }

    endPaint(ctx, palette, tessel) {
        ctx.restore();
    }


}

class ClearTesselPainter extends TesselPainter {
    constructor() {
        super();
    }

    doPaint(ctx, palette, tessel) {
        let prevGlobalCompositeOperation = ctx.globalCompositeOperation;
        ctx.globalCompositeOperation = 'destination-out';
        ctx.fillStyle = 'rgba(0, 0, 0, 1)';
        ctx.strokeStyle = 'rgba(0, 0, 0, 1)';


        ctx.fill();
        ctx.stroke();
        ctx.globalCompositeOperation = prevGlobalCompositeOperation;
    }


}


class OutlineTesselPainter extends TesselPainter {
    constructor() {
        super();
    }

    doPaint(ctx, palette, tessel) {
        ctx.strokeStyle = "#000";

        let t = ctx.getTransform();

        // reverse the scaling, which is also applied to lines
        // outlines are always 2 units thick
        ctx.lineWidth = 2/Math.max(t.a, t.d); //t.a, t.d hold the horiz and vert scaling
        ctx.stroke();
        //ctx.restore();
    }
}


class SelectedTesselPainter extends TesselPainter {
    constructor() {
        super();
    }

    doPaint(ctx, palette, tessel) {
        ctx.strokeStyle = "#f00";
        ctx.lineWidth = 4;
        ctx.stroke();
    }


}



class SolidTesselPainter extends TesselPainter {
    constructor() {
        super();
        this._dict = {};
    }

    doPaint(ctx, palette, tessel) {
        // if the palette is not yet available we can't solid paint
        if (!palette) return;
        let color = palette.colorFromIndex(tessel.colorIndex);
        if (color instanceof MosaicSolidColor) {
            this._doSolidColorPaint(ctx, palette, tessel);
        } else if (color instanceof MosaicTextureColor) {
            this._doTextureColorPaint(ctx, palette, tessel);
        }


    }

    _doSolidColorPaint(ctx, palette, tessel) {
        let color = palette.colorFromIndex(tessel.colorIndex);
        let r = color.red;
        let g = color.green;
        let b = color.blue;
        ctx.fillStyle = `rgba(${r},${g},${b},1)`;
        ctx.fill();
    }

    _key(color, w, h) {
        return color.imgUrl + w +h;
    }

    _doTextureColorPaint(ctx, palette, tessel) {
        let color = palette.colorFromIndex(tessel.colorIndex);
        let w = tessel.w;
        let h = tessel.h;
        let offscreenCanvas = this._dict[this._key(color, w, h)];
        if (!offscreenCanvas){
            log.info("Creating offscreen canvas for ", color);
            offscreenCanvas = document.createElement('canvas');
            offscreenCanvas.width = w;
            offscreenCanvas.height = h;
            let ctx = offscreenCanvas.getContext('2d');
            ctx.drawImage(color.texture, 0, 0, w, h);
            //ctx.restore();

            this._dict[this._key(color, w, h)] = offscreenCanvas;
        }

        ctx.drawImage(offscreenCanvas, 0, 0, w, h);
    }

}