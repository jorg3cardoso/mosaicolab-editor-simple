export {CmdMosaicViewSetIsScaling}
import {CmdUndoable} from "../cmd-undoable";
import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.view");

class CmdMosaicViewSetIsScaling extends CmdUndoable {
    constructor() {
        super();
        this._mosaicView = null;
        this._isScaling = null;

        this._previousIsScaling = null;

    }
    get name() {
        return CmdMosaicViewSetIsScaling.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }


    get isScaling() {
        return this._isScaling;
    }

    set isScaling(value) {
        this._isScaling = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Changing isScaling: ", this._isScaling);
        this._previousIsScaling = this._mosaicView.isScaling;

        this._mosaicView.isScaling = this._isScaling;

        log.debug("Changed isScaling to: ", this._isScaling);
    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {isScaling: this._isScaling});

        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);
        this.isScaling = obj.isScaling;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdMosaicViewSetIsScaling();
        undo.mosaicView = this._mosaicView;
        undo.isScaling = this._previousIsScaling;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousIsScaling === this._isScaling) {
            return false;
        } else {
            return true;
        }
    }



    toString() {
        return "CmdMosaicViewSetIsScaling [mosaicView:" + this._mosaicView + ", isScaling: " + this._isScaling + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }

        if (this._isScaling === null || this._isScaling === undefined) {
            log.error(this.toString());
            throw("No isScaling has been set, cannot execute command.");
        }

    }
}