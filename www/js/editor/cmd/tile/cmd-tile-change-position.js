export {CmdTileChangePosition}

import {CmdUndoable} from "../cmd-undoable";

import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.tile");


class CmdTileChangePosition extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._tesselId = null;
        this._tessel = null;
        this._x = null;
        this._y = null;
        this._previousX = null;
        this._previousY = null;
    }

    get name() {
        return CmdTileChangePosition.name;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get tesselId() {
        return this._tesselId;
    }

    set tesselId(value) {
        this._tesselId = value;
    }

    get x() {
        return this._x;
    }

    set x(value) {
        this._x = value;
    }

    get y() {
        return this._y;
    }

    set y(value) {
        this._y = value;
    }
    _dodo() {
        super._dodo();

        this._tessel = this._mosaicView.getTesselById(this._tesselId);
        if (this._tessel === null) {
            log.error("No tile with id ", this._tesselId, " found in mosaic.");
            return;
        }
        this._previousX = this._tessel.x;
        this._previousY = this._tessel.y;
        this._tessel.x = this._x;
        this._tessel.y = this.y;
        log.debug("Changed tile position to: ", this._tessel.x, this._tessel.y );

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {tesselId: this._tessel.id, x: this._x, y: this._y});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.tesselId = obj.tesselId;
        this.x = obj.x;
        this.y = obj.y;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdTileChangePosition();
        undo.mosaicView = this._mosaicView;
        undo.tesselId = this._tesselId;
        undo.x = this._previousX;
        undo.y = this._previousY
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousX === this._x && this._previousY === this._y) {
            return false;
        } else {
            return true;
        }
    }



    toString() {
        return "CmdTileChangePosition [mosaicView:" + this._mosaicView + ", tesselId: " + this._tesselId + ", x: " + this._x + ", y: " + this._y + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (this._tesselId === null || this._tesselId === undefined) {
            log.error(this.toString());
            throw("No tile id provided, cannot execute command.");
        }

    }
}