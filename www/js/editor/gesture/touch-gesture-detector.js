import {GestureDetector} from "./gesture-detector";

export {TouchGestureDetector}

import * as loglevel from '../libraries/loglevel.js';
import {
    ControlMainDrag, ControlSecundaryDrag,
    MainClick,
    MainDrag, Pinch, SecundaryClick,
    SecundaryDrag,
    ShiftMainDrag,
    ShiftSecundaryDrag
} from "./events/pointer-event";
import {Vec2} from "../utility/vec2";

var log = loglevel.getLogger("gesture");

class TouchGestureDetector extends GestureDetector {

    constructor(htmlEl) {
        super();
        this._el = htmlEl;
        this._state = new TouchGestureStateIdle(this, null);
    }

    get state() {
        return this._state;
    }

    set state(value) {
        log.debug("Changing state to ", value);
        this._state = value;
    }

    handleTouchStart(evt){
        log.debug("handleTouchStart", this._state);
        this._state.handleTouchStart(evt);
    }
    handleTouchMove(evt){
        log.debug("handleTouchMove", this._state);
        this._state.handleTouchMove(evt);
    }
    handleTouchEnd(evt){
        log.debug("handleTouchEnd", this._state);
        this._state.handleTouchEnd(evt);
    }
    handleTouchCancel(evt){
        log.debug("handleTouchCancel", this._state);
        this._state.handleTouchCancel(evt);
    }
}

class TouchGestureState {
    constructor(tgd, evt) {
        this._tgd = tgd;
    }
    handleTouchStart(evt){   }
    handleTouchMove(evt){   }
    handleTouchEnd(evt){   }
    handleTouchCancel(evt){   }
}

class TouchGestureStateIdle extends TouchGestureState {
    constructor(tgd, evt) {
        super(tgd, evt);
    }

    handleTouchStart(evt){
        if (evt.touches.length > 0) {
            this._tgd.state = new TouchGestureStateTouching(this._tgd, evt);
        }
    }
    handleTouchMove(evt){
        if (evt.touches.length === 1) {
            this._tgd.state = new TouchGestureStateDragging(this._tgd, evt);
        } else if (evt.touches.length === 2) {
            this._tgd.state = new TouchGestureStatePinching(this._tgd, evt);
        }
    }
    handleTouchEnd(evt){   }
    handleTouchCancel(evt){   }
}
class TouchGestureStateTouching extends TouchGestureState {
    constructor(tgd, evt) {
        super(tgd, evt);

        this._lastEvt = null;

        this._timer = null;
        let _this = this;
        if (evt.touches.length === 1) {
            //console.log("setting timer")
            this._timer = setTimeout(function () {
                //console.log("timer expired")
                _this._timerExpired(evt);
            }, 1000);
        }
    }

    _timerExpired(evt) {
        this._tgd.sendNotification(new MainClick(evt));
    }

    handleTouchStart(evt){
        if (evt.touches.length === 2) {
            clearTimeout(this._timer);
            this._timer = null;
        } else if (evt.touches.length === 1) {
            this._lastEvt = evt;
        }
    }
    handleTouchMove(evt){
        if (evt.touches.length === 1) {
            clearTimeout(this._timer);
            this._timer = null;
            this._tgd.state = new TouchGestureStateDragging(this._tgd, evt);
        } else if (evt.touches.length === 2) {
            clearTimeout(this._timer);
            this._timer = null;
            this._tgd.state = new TouchGestureStatePinching(this._tgd, evt);
        }
    }
    handleTouchEnd(evt){
        clearTimeout(this._timer);
        this._timer = null;
        if (evt.touches.length === 0) {
            this._tgd.sendNotification(new MainClick(this._lastEvt));
        }

        this._tgd.state = new TouchGestureStateIdle(this._tgd, evt);
    }
    handleTouchCancel(evt){   }
}
class TouchGestureStateDragging extends TouchGestureState {
    constructor(tgd, evt) {
        super(tgd, evt);
        this._lastEvt = null;

        if (evt.touches.length === 1) {
            this._tgd.sendNotification(new MainDrag(evt, false));
            this._lastEvt = evt;
        }
    }

    handleTouchStart(evt){
        if (evt.touches.length === 2) {
            this._tgd.sendNotification(new MainDrag(this._lastEvt, true));
            this._tgd.state = new TouchGestureStatePinching(this._tgd, evt);
        }
    }
    handleTouchMove(evt){
        if (evt.touches.length === 1) {
            this._tgd.sendNotification(new MainDrag(evt, false));
            this._lastEvt = evt;
        } else if (evt.touches.length === 2) {
            this._tgd.sendNotification(new MainDrag(this._lastEvt, true));
            this._tgd.state = new TouchGestureStatePinching(this._tgd, evt);
        }
    }
    handleTouchEnd(evt){
        if (evt.touches.length === 0 || evt.touches.length > 2) {
            this._tgd.sendNotification(new MainDrag(this._lastEvt, true));
            this._tgd.state = new TouchGestureStateIdle(this._tgd, evt);
        } else if (evt.touches.length === 2) {
            this._tgd.sendNotification(new MainDrag(this._lastEvt, true));
            this._tgd.state = new TouchGestureStatePinching(this._tgd, evt);
        }
    }
    handleTouchCancel(evt){   }
}
class TouchGestureStatePinching extends TouchGestureState {
    constructor(tgd, evt) {
        super(tgd, evt);
        this._lastEvt = null;

        this._entry(evt);
    }
    _entry(evt) {
        log.debug("Entry");

        log.debug("Sending entry event ");
        this._lastEvt = evt;
        this._pinchStart = [new Vec2(evt.touches[0].clientX, evt.touches[0].clientY),
                new Vec2(evt.touches[1].clientX, evt.touches[1].clientY)];
        this._tgd.sendNotification(new Pinch(evt, false, this._pinchStart));
    }

    _exit(evt) {
        log.debug("Exit");
        this._tgd.sendNotification(new Pinch(evt, true, this._pinchStart));
    }

    handleTouchStart(evt){
        if (evt.touches.length === 1) {
            //this._exit(evt);
            log.debug("No transition because only 1 touch")
            //this._tgd.state = new TouchGestureStateDragging(this._tgd, evt);
        } else if (evt.touches.length === 0 || evt.touches.length > 2) {
            this._exit(evt);
            log.debug("Transition to Idle state because no touches or more than 2 touches")
            this._tgd.state = new TouchGestureStateIdle(this._tgd, evt);
        }
    }
    handleTouchMove(evt){
        if (evt.touches.length === 2) {
            this._lastEvt = evt;
            this._tgd.sendNotification(new Pinch(evt, false, this._pinchStart));
        } else if (evt.touches.length === 1) {
            //this._exit(evt);
            log.debug("No transition because only 1 touch")
            //this._tgd.state = new TouchGestureStateDragging(this._tgd, evt);
        } else if (evt.touches.length === 0 || evt.touches.length > 2) {
            this._exit(evt);
            log.debug("Transition to Idle state because no touches or more than 2 touches")
            this._tgd.state = new TouchGestureStateIdle(this._tgd, evt);
        }
    }
    handleTouchEnd(evt){
        if (evt.touches.length === 1) {
            //this._exit(this._lastEvt);
            log.debug("No transition because only 1 touch")
            //this._tgd.state = new TouchGestureStateDragging(this._tgd, evt);
        } else if (evt.touches.length === 0 || evt.touches.length > 2) {
            this._exit(this._lastEvt);
            log.debug("Transition to Idle state because no touches or more than 2 touches")
            this._tgd.state = new TouchGestureStateIdle(this._tgd, evt);
        }
    }
    handleTouchCancel(evt){   }
}

