export {CmdMosaicViewSetLayout}
import {CmdUndoable} from "../cmd-undoable";
import {MosaicLayoutFactory} from "../../layout/mosaic-layout-factory";
import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.view");

class CmdMosaicViewSetLayout extends CmdUndoable {
    constructor() {
        super();
        this._mosaicView = null;
        this._mosaicLayoutName = null;
        this._params = [];
        this._previousMosaicLayoutName = null;
        this._previousParams = null;
    }
    get name() {
        return CmdMosaicViewSetLayout.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get mosaicLayoutName() {
        return this._mosaicLayoutName;
    }

    set mosaicLayoutName(value) {
        this._mosaicLayoutName = value;
    }

    get params() {
        return this._params;
    }

    set params(value) {
        this._params = value;
    }

    _dodo() {
        super._dodo();

        let newLayout = MosaicLayoutFactory.create(this._mosaicLayoutName);
        if (newLayout === null) {
            log.warn("Unable to create layout: ", this._mosaicLayoutName);
        } else {
            newLayout.params = this._params;
        }
        this._previousMosaicLayoutName = this._mosaicView.mosaicLayout?this._mosaicView.mosaicLayout.name : null;
        this._previousParams = this._mosaicView.mosaicLayout? this._mosaicView.mosaicLayout.params : null;

        this._mosaicView.mosaicLayout = newLayout;

        log.debug("Changed mosaic layout to: ", this._mosaicLayoutName);

    }

    toJS() {
        let json = super.toJS();
        //Object.assign(json, {mosaicLayoutName: this._mosaicLayoutName, params: this._params});

        let newLayout = MosaicLayoutFactory.create(this._mosaicLayoutName);
        if (newLayout === null) {
            log.warn("Unable to create layout: ", this._mosaicLayoutName);
        } else {
            newLayout.params = this._params;
        }
        Object.assign(json, {layout: newLayout.toJSON()});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);
        let layout = MosaicLayoutFactory.createFromJSON(obj.layout);
        this.mosaicLayoutName = layout.name;
        this.params = layout.params;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdMosaicViewSetLayout();
        undo.mosaicView = this._mosaicView;
        undo.mosaicLayoutName = this._previousMosaicLayoutName;
        undo.params = this._previousParams;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousMosaicLayoutName === this._mosaicLayoutName) {
            return false;
        } else {
            return true;
        }
    }



    toString() {
        return "CmdMosaicViewSetLayout [mosaicView:" + this._mosaicView + ", mosaicLayoutName: " + this._mosaicLayoutName + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }


    }
}