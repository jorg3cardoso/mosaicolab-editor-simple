import {ControlWheeling, SecundaryWheeling, ShiftWheeling} from "./events/wheeling";

export {MouseGestureDetector}
import {GestureDetector} from "./gesture-detector";

import {
    ControlMainClick,
    ControlMainDrag, ControlSecundaryClick, ControlSecundaryDrag,
    MainClick,
    MainDrag, SecundaryClick,
    SecundaryDrag, ShiftMainClick,
    ShiftMainDrag, ShiftSecundaryClick,
    ShiftSecundaryDrag
} from "./events/pointer-event";
import {Wheeling} from "./events/wheeling";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("gesture");

class MouseGestureDetector extends GestureDetector {

    constructor(htmlEl) {
        super();
        this._el = htmlEl;
        this._state = new MouseGestureStateIdle(this, null);
    }

    get state() {
        return this._state;
    }

    set state(value) {
        this._state = value;
    }

    handleMousePressed(evt){
        this._state.handleMousePressed(evt);
    }
    handleMouseMoved(evt){
        this._state.handleMouseMoved(evt);
    }
    handleMouseReleased(evt){
        this._state.handleMouseReleased(evt);
    }
    handleMouseWheel(evt){
        this._state.handleMouseWheel(evt);
    }
}

class MouseGestureState {
    constructor(mgd, evt) {
        this._mgd = mgd;
    }
    handleMousePressed(evt){   }
    handleMouseMoved(evt){   }
    handleMouseReleased(evt){   }
    handleMouseWheel(evt){
        log.debug("mouse wheel");
    }
}

class MouseGestureStateIdle extends MouseGestureState {
    constructor(mgd, evt) {
        super(mgd, evt);
    }
    handleMousePressed(evt){
        this._mgd.state = new MouseGestureStatePressed(this._mgd, evt);
    }
    handleMouseMoved(evt){
    }
    handleMouseReleased(evt){   }
    handleMouseWheel(evt){
        this._mgd.state = new MouseGestureStateWheeled(this._mgd, evt);
    }
}

class MouseGestureStatePressed extends MouseGestureState {
    constructor(mgd, evt) {
        super(mgd, evt);
    }

    _sendClickEvent(shiftKey, ctrlKey, button, evt) {
        if (!shiftKey && !ctrlKey && button === 0) {
            this._mgd.sendNotification(new MainClick(evt));
        } else if (shiftKey && button === 0) {
            this._mgd.sendNotification(new ShiftMainClick(evt));
        } else if (ctrlKey && button === 0) {
            this._mgd.sendNotification(new ControlMainClick(evt));
        } else if (!shiftKey && !ctrlKey && button === 1) {
            this._mgd.sendNotification(new SecundaryClick(evt));
        } else if (shiftKey && button === 1) {
            this._mgd.sendNotification(new ShiftSecundaryClick(evt));
        } else if (ctrlKey && button === 1) {
            this._mgd.sendNotification(new ControlSecundaryClick(evt));
        }
    }

    handleMousePressed(evt){
        log.debug("Received mouse pressed event while in MouseGestureStatePressed state!");
    }
    handleMouseMoved(evt){
        if (evt.movementX === 0 && evt.movementY === 0) return; // bug that causes mousemove while button is pressed?
        this._mgd.state = new MouseGestureStateDragged(this._mgd, evt);
    }
    handleMouseReleased(evt){
        this._sendClickEvent(evt.shiftKey, evt.ctrlKey, evt.button, evt);
        this._mgd.state = new MouseGestureStateIdle(this._mgd, evt);
    }
}

class MouseGestureStateDragged extends MouseGestureState {
    constructor(mgd, evt) {
        super(mgd, evt);
        //console.log(evt);
        this._shiftKey = evt.shiftKey;
        this._ctrlKey = evt.ctrlKey;
        this._buttons = evt.buttons;

        this._x = evt.clientX;
        this._y = evt.clientY;

        this._sendDragEvent(evt.shiftKey, evt.ctrlKey, evt.buttons, false, evt);
    }
    _sendDragEvent(shiftKey, ctrlKey, buttons, finished, evt) {
        let movementX = evt.clientX - this._x;
        let movementY = evt.clientY - this._y;

        let newEvt;
        if (!shiftKey && !ctrlKey && buttons === 1) {
            newEvt = new MainDrag(evt, finished);

        } else if (shiftKey && buttons === 1) {
            newEvt = new ShiftMainDrag(evt, finished);
        } else if (ctrlKey && buttons === 1) {
            newEvt = new ControlMainDrag(evt, finished);
        } else if (!shiftKey && !ctrlKey && buttons === 2) {
            newEvt = new SecundaryDrag(evt, finished);
        } else if (shiftKey && buttons === 2) {
            newEvt = new ShiftSecundaryDrag(evt, finished);
        } else if (ctrlKey && buttons === 2) {
            newEvt = new ControlSecundaryDrag(evt, finished);
        }
        newEvt.movementX = movementX;
        newEvt.movementY = movementY
        this._mgd.sendNotification(newEvt);
    }

    _changed(evt) {
        if (this._shiftKey !== evt.shiftKey || this._ctrlKey !== evt.ctrlKey || this._buttons !== evt.buttons) {
            return true;
        } else {
            return false;
        }
    }
    handleMousePressed(evt){
    }

    handleMouseMoved(evt){
        if (evt.movementX === 0 && evt.movementY === 0) return; // bug that causes mousemove while button is pressed?

        if (this._changed(evt)) {
            this._sendDragEvent(this._shiftKey, this._ctrlKey, this._buttons, true, evt);

        }
        this._sendDragEvent(evt.shiftKey, evt.ctrlKey, evt.buttons, false, evt);
        this._shiftKey = evt.shiftKey;
        this._ctrlKey = evt.ctrlKey;
        this._buttons = evt.buttons;
    }
    handleMouseReleased(evt){
        this._sendDragEvent(this._shiftKey, this._ctrlKey, this._buttons, true, evt);
        this._mgd.state = new MouseGestureStateIdle(this._mgd, evt);
    }

}


class MouseGestureStateWheeled extends MouseGestureState {
    constructor(mgd, evt) {
        super(mgd, evt);
        //console.log(evt);
        this._shiftKey = evt.shiftKey;
        this._ctrlKey = evt.ctrlKey;
        this._buttons = evt.buttons;

        this._sendWheelEvent(evt.shiftKey, evt.ctrlKey, evt.buttons, false, evt);
    }

    _sendWheelEvent(shiftKey, ctrlKey, buttons, finished, evt) {
        clearTimeout(this._timer);
        if (!finished) {
            this._timer = setTimeout(this._timedOut.bind(this), 200);
        }

        if (!shiftKey && !ctrlKey && buttons === 0) {
            this._mgd.sendNotification(new Wheeling(evt, finished));
        } else if (shiftKey && buttons === 0) {
            this._mgd.sendNotification(new ShiftWheeling(evt, finished));
        } else if (ctrlKey && buttons === 0) {
            this._mgd.sendNotification(new ControlWheeling(evt, finished));
        } else if (!shiftKey && !ctrlKey && buttons === 2) {
            this._mgd.sendNotification(new SecundaryWheeling(evt, finished));
        }
    }

    _timedOut() {
        this._sendWheelEvent(this._shiftKey, this._ctrlKey, this._buttons, true, null);
        this._mgd.state = new MouseGestureStateIdle(this._mgd, null);
    }

    _changed(evt) {
        if (this._shiftKey !== evt.shiftKey || this._ctrlKey !== evt.ctrlKey || this._buttons !== evt.buttons) {
            return true;
        } else {
            return false;
        }
    }
    handleMousePressed(evt){
    }

    handleMouseMoved(evt){

    }
    handleMouseReleased(evt){
    }

    handleMouseWheel(evt){
        if (this._changed(evt)) {
            this._sendWheelEvent(this._shiftKey, this._ctrlKey, this._buttons, true, evt);

        }
        this._sendWheelEvent(evt.shiftKey, evt.ctrlKey, evt.buttons, false, evt);
        this._shiftKey = evt.shiftKey;
        this._ctrlKey = evt.ctrlKey;
        this._buttons = evt.buttons;
    }
}