import {ObservableObserver} from "../observer/observer-observable.js";
import {MosaicColorPalette} from "../color/mosaic-color-palette";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("color");

export {MosaicColorPaletteSelector}


class MosaicColorPaletteSelector extends ObservableObserver {
    constructor(htmlElementId) {
        super();
        this._htmlElementId = htmlElementId;
        this._htmlElement = document.querySelector("#" + this._htmlElementId);
        let _this = this;

        this._htmlElement.addEventListener("change", function valueChanged(evt) {
            _this.notifyObservers(this.value);
        });
    }

    update(component) {

        if (component instanceof MosaicColorPalette) {
            log.debug("Setting value of color palette selector from", this._htmlElement.value, "to ", component.paletteName);

            if (this._htmlElement.value !== component.paletteName) {

                this._htmlElement.value = component.paletteName;
            }
        }
    }

    get value() {
        return this._htmlElement.value;
    }

    toString() {
        return "MosaicColorPaletteSelector [value: "+ this._htmlElement.value + "]";
    }
}