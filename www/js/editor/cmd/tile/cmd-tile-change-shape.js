import {TesselShapeFactory} from "../../mosaic/tessel-shape-factory";
import {CmdUndoable} from "../cmd-undoable";

import * as loglevel from '../../libraries/loglevel.js';

export {CmdTileChangeShape}

var log = loglevel.getLogger("cmd.tile");


class CmdTileChangeShape extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._tesselId = null;
        this._tessel = null;
        this._shapeName = null;
        this._previousShape = null;
    }

    get name() {
        return CmdTileChangeShape.name;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get tesselId() {
        return this._tesselId;
    }

    set tesselId(value) {
        this._tesselId = value;
    }

get shape() {
        return this._shape;
}
set shape(value) {
        this._shape = value;
}
    get shapeName() {
        return this._shapeName;
    }

    set shapeName(value) {
        this._shapeName = value;
    }

    _dodo() {
        super._dodo();
        this._tessel = this._mosaicView.getTesselById(this._tesselId);
        if (this._tessel === null) {
            log.error("No tile with id ", this._tesselId, " found in mosaic.");
            return;
        }
        this._previousShape = this._tessel.shape;
        if (this._shape) {
            this._tessel.shape = this._shape;
        } else {
            this._tessel.shape = TesselShapeFactory.create(this._shapeName);
        }

        log.debug("Changed tile shape to: ", this._tessel.shape );

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {tesselId: this._tessel.id, shapeName: this._shapeName, shape: this._shape});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.tesselId = obj.tesselId;
        this.shapeName = obj.shapeName;
        this.shape = obj.shape;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdTileChangeShape();
        undo.mosaicView = this._mosaicView;
        undo.tesselId = this._tesselId;
        undo.shapeName = this._previousShape.name;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        return true;
    }



    toString() {
        return "CmdTileChangeShape [mosaicView:" + this._mosaicView + ", tesselId: " + this._tesselId + ", colorId: " + this._colorId + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (this._tesselId === null || this._tesselId === undefined) {
            log.error(this.toString());
            throw("No tile id provided, cannot execute command.");
        }
        if ((this._shape === null || this._shape === undefined) && (this._shapeName === null || this._shapeName === undefined)) {
            log.error(this.toString());
            throw("No shape name provided, cannot execute command.");
        }

    }
}