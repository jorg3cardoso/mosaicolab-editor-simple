import {Persistence} from "./persistence.js";

export {PersistanceDeepStream}



import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("persistence");

class PersistanceDeepStream extends Persistence {
    constructor(deepStreamServer) {
        super();
        //this._persistantState = persistantState;
        this._deepStreamServer = deepStreamServer;
        log.trace(deepStreamServer);

        this._deepClient = deepstream(this._deepStreamServer);
        this._deepClient.on('error', function (err) {
            log.error(err);
        });
    }

    getJS() {
        return null;
    }

    loadFromJS(obj) {

    }

    load() {
        let _this = this;
        return new Promise(function(resolve, reject){
            _this._deepClient.login({}, function (success, data) {
                    if (success) {
                        log.info("Connected to DeepStream", _this._deepClient._url);
                        _this._getEditorRecord(resolve, reject);

                        // subscribe to commands
                        _this._deepClient.event.subscribe('cmd', function(cmd){
                            log.debug("Got command ", cmd);
                            // let cmdBuilder = CommandBuilder.getCommandBuilder(cmd.name);
                            // let cmdFromNet = cmdBuilder.build();
                            // cmdFromNet.fromJS(cmd);
                            // _this.notifyObservers(cmdFromNet);
                        });
                        log.debug("Persistant state loading from DeepStream");
                        //resolve();
                    } else {
                        log.warn("Failed login on ", _this._deepClient._url);
                        reject();
                    }
                }
            );
        });
    }

    _getEditorRecord(resolve, reject) {
        log.info("Loading Editor from DeepStream... ");
        let _this = this;
        let editorRecord = _this._deepClient.record.getRecord("editor");
        editorRecord.whenReady(function (editor) {
            log.debug("Received: ", editor);

            // let cols = parseInt(editor.get("gridSize").cols);
            // let rows = parseInt(editor.get("gridSize").rows);
            // let grid = editor.get("grid");
            // let cmdBuilder = CommandBuilder.getCommandBuilder(SetGridCommand.CMD_NAME);
            // let newGrid = new Array(cols * rows);
            // cmdBuilder.setCols(cols);
            // cmdBuilder.setRows(rows);
            // for (var key in grid) {
            //     let indices = key.split('x');
            //     newGrid[parseInt(indices[0]) + parseInt(indices[1]) * cols] = grid[key];
            // }
            // cmdBuilder.setNewGrid(newGrid);
            //
            // _this.notifyObservers(cmdBuilder.build());
            log.info("Loaded Editor from DeepStream.");
            resolve();
        });
    }

    /**
     * Sends deepstream event but also updates record
     * @param cmd
     * @param emmitedEvent
     * @private
     */
    _sendToDeepStream(cmd, emmitedEvent) {
        let _this = this;

        // Sends event
        // if (!emmitedEvent) this._deepClient.event.emit('cmd', cmd.toJS());
        //
        // if (cmd instanceof CmdList) {
        //     cmd.commandList.forEach(function(cmd){
        //         _this._sendToDeepStream(cmd, true);
        //     })
        // }else {
        //     let editor = this._deepClient.record.getRecord('editor');
        //
        //     if (cmd instanceof PaintCellCommand) {
        //         log.debug("Updating Editor Record from paint cell command");
        //         editor.set("grid."+cmd.col+"x"+cmd.row, cmd.colorIndex)
        //     } else {
        //         log.debug("Updating Editor Record");
        //         editor.set("gridSize", {cols: cmd.mosaicGrid.cols, rows: cmd.mosaicGrid.rows});
        //         let grid = {};
        //         for (let c = 0; c < cmd.mosaicGrid.cols; c++) {
        //             for (let r = 0; r < cmd.mosaicGrid.rows; r++) {
        //                 grid[c+"x"+r] = cmd.mosaicGrid.getColorIndex(c, r);
        //             }
        //         }
        //         editor.set("grid", grid);
        //     }
        // }

    }

    update(observable, cmds) {
        super.update(observable, cmds);

        this._sendToDeepStream(cmds);
        log.trace("Update ", cmds);
    }
}
