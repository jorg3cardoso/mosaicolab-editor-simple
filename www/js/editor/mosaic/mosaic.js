import {Observable, ObservableObserver} from "../observer/observer-observable";

export {Mosaic}


import {Dimension} from "../utility/dimension";
import Quadtree from "../libraries/quadtree-lib/quadtree";
import {Tessel} from "./tessel";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("mosaic");

class Mosaic extends ObservableObserver {

    constructor() {
        super();
        this._tessels = [];
        this._painter = null;
        this._bulkUpdateCount = 0;
        this._changedDuringBulkUpdate = false;

        this._mosaicDimension = null;
        // Dirty flag, used by get mosaicDimension to determine if the dimension needs to be recalculated
        this._mosaicDimensionChanged = true;

        this._quadtree = null;
        // Dirty flag, used by getTesselAt to determine if the quadtree needs reconstruction
        this._quadtreeNeedsRebuild = true;

        this._notifyParams = {};
    }

    notifyObservers(params) {
        Object.assign(this._notifyParams, params);
        if (this._bulkUpdateCount === 0) {
            super.notifyObservers(this._notifyParams);
            this._notifyParams = {};
        }
    }



    /* Getters and setters */

    addTessel(tessel) {
        this._tessels.push(tessel);
        if (this._bulkUpdateCount > 0) this._changedDuringBulkUpdate = true;
        this._mosaicDimensionChanged = true;
        this._quadtreeNeedsRebuild = true;
        this.notifyObservers({addTessel:true});
        tessel.addObserver(this);
    }

    addTessels(tessels) {
        Array.prototype.push.apply(this._tessels, tessels);
        if (this._bulkUpdateCount > 0) this._changedDuringBulkUpdate = true;
        this._mosaicDimensionChanged = true;
        this._quadtreeNeedsRebuild = true;
        this.notifyObservers({addTessel:true});
        tessels.forEach(function(tessel) {
            tessel.addObserver(this)
        }.bind(this));
    }

    replaceTessels(tessels) {
        this._tessels.forEach(function(tessel){
           tessel.removeObserver(this);
        }.bind(this));
        this._tessels = [];
        Array.prototype.push.apply(this._tessels, tessels);
        if (this._bulkUpdateCount > 0) this._changedDuringBulkUpdate = true;
        this._mosaicDimensionChanged = true;
        this._quadtreeNeedsRebuild = true;
        this.notifyObservers({addTessel:true});
        tessels.forEach(function(tessel) {
            tessel.addObserver(this)
        }.bind(this));
    }

    insertTesselAt(tessel, position) {
        this._tessels.splice(position,0, tessel);
        if (this._bulkUpdateCount > 0) this._changedDuringBulkUpdate = true;
        this._mosaicDimensionChanged = true;
        this._quadtreeNeedsRebuild = true;
        this.notifyObservers({addTessel:true});
        tessel.addObserver(this);
    }

    getTesselIndex(tessel) {
        for( var i = 0; i< this._tessels.length; i++){
            if ( this._tessels[i] === tessel) {
                return i;
            }
        }
        return null;
    }

    removeTessel(tessel) {
        tessel.removeObserver(this);
        for( var i = this._tessels.length-1; i >= 0; i--){
            if ( this._tessels[i] === tessel) {
                this._tessels.splice(i, 1);
            }
        }
        if (this._bulkUpdateCount > 0) this._changedDuringBulkUpdate = true;
        this._mosaicDimensionChanged = true;
        this._quadtreeNeedsRebuild = true;

        this.notifyObservers({removeTessel:true});
    }

    get tessels() {
        return [...this._tessels];
    }

    get painter() {
        return this._painter;
    }

    set painter(value) {
        this._painter = value;
    }

    get mosaicDimension() {
        if (this._mosaicDimensionChanged) {
            this._calculateMosaicDimension();
        }
        log.debug("Mosaic dimension: ", this._mosaicDimension);
        return this._mosaicDimension;
    }


    /* Overrides update in ObserverObservable. Invoked when Observable notifies of change. */
    /**
     * Mosaic observes each tile added to it
     *
     * @param observable
     * @param params
     */
    update(observable, params) {
        if (observable instanceof Tessel) {
            if (params) {
                if( params.x || params.y || params.w || params.h) {
                    this._quadtreeNeedsRebuild = true;
                    this._mosaicDimensionChanged = true;
                }
            } else {
                this._quadtreeNeedsRebuild = true;
                this._mosaicDimensionChanged = true;
            }

            if (this._bulkUpdateCount > 0) {
                this._changedDuringBulkUpdate = true;
                this.notifyObservers( params );
            } else {
                this.notifyObservers(Object.assign({tesselId: observable.id}, params));
            }



        }
    }

    /* Public methods */


    /**
     * Returns the list of tessels at the given coordinates (in mosaic coordinate space)
     * @param x
     * @param y
     * @returns {Array<T> | * | Array}
     */
    getTesselsAt(ctx, x, y) {
        if (this._quadtreeNeedsRebuild) {
            this._rebuildQuadtree();
        }
        let colliding = this._quadtree.colliding({x: x, y: y}).map(function(el){return el.tessel});
        let tessels = [];
        colliding.forEach(function(tessel){
           if (tessel.isPointInPath(ctx, x, y)) tessels.push(tessel);
        });
        return tessels;

    }

    /*
    resize(percent) {
        log.debug("Resizing mosaic to ", percent);
        this.startBulkUpdate();
        for (let i = 0; i < this._tessels.length; i++) {
            this._tessels[i].x = this._tessels[i].x*percent;
            this._tessels[i].y = this._tessels[i].y*percent;
            this._tessels[i].w = this._tessels[i].w*percent;
            this._tessels[i].h = this._tessels[i].h*percent;
            this._tessels[i].shape.scale(percent);
        }
        this.endBulkUpdate();
    }*/

    /**
     * Returns a tile by id, or null if there is no tile with the provided id.
     * @param id
     * @returns {null|Tessel}
     */
    getTesselById(id) {
        for (let i = 0; i < this._tessels.length; i++) {
            //console.log(this._tessels[i].id);
            if (this._tessels[i].id === id) return this._tessels[i];
        }
        return null;
    }



    paint(ctx, palette, params) {
        if (this._painter === null) {
            log.error("Mosaic has no painter assigned");
        } else {
            this._painter.paint(ctx, palette, this, params);
        }
    }


    startBulkUpdate() {
        //log.trace("startBulkUpdate");
        this._bulkUpdateCount++;
        this._changedDuringBulkUpdate = false;
        this._notifyParams = {};
    }

    endBulkUpdate() {
        //log.trace("endBulkUpdate");
        this._bulkUpdateCount--;
        if (this._bulkUpdateCount < 0) {
            log.trace(this);
            throw "Negative Bulk update count!";

        }
        if (this._changedDuringBulkUpdate)
            this.notifyObservers();

    }


    /* private methods */
    _calculateMosaicDimension() {

        let minX = Number.POSITIVE_INFINITY;
        let minY = Number.POSITIVE_INFINITY;
        let maxX = Number.NEGATIVE_INFINITY;
        let maxY = Number.NEGATIVE_INFINITY;

        this._tessels.forEach(function(tessel){
            if (tessel.x < minX) {
                minX = tessel.x;
            }
            if (tessel.x+tessel.w > maxX) {
                maxX = tessel.x+tessel.w;
            }
            if (tessel.y < minY) {
                minY = tessel.y;
            }
            if (tessel.y+tessel.h > maxY) {
                maxY = tessel.y+tessel.h;
            }
            log.debug(tessel);
            log.debug(minX, " " , minY, " ", maxX, " ", maxY)
        });
        this._mosaicDimension = new Dimension(minX, minY, maxX-minX, maxY-minY);
        this._mosaicDimensionChanged = false;
        log.debug("Calculated mosaic dimension: ", this._mosaicDimension);
    }

    _rebuildQuadtree() {
        let t0 = performance.now();
        let dim = this.mosaicDimension;

        this._quadtree = new Quadtree({
            width: dim.w,
            height:dim.h
        })

        this._tessels.forEach(function (tessel) {
            this._quadtree.push(
                {
                    x: tessel.x,
                    y: tessel.y,
                    width: tessel.w,
                    height: tessel.h,
                    tessel: tessel
                });
        }.bind(this));
        this._quadtreeNeedsRebuild = false;
        let t1 = performance.now();
        log.trace("Quadtree reconstruction took " + (t1 - t0) + " milliseconds.");
    }

    toString() {
        return "Mosaic [bulkUpdateCount: " +this._bulkUpdateCount + ", changedDuringBulkUpdate: " + this._changedDuringBulkUpdate + ", mosaicDimensionChanged: " + this._mosaicDimensionChanged + ", quadtreeNeedsRebuild: " + this._quadtreeNeedsRebuild+"]";
    }

}