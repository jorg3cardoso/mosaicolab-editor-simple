import {Observer} from "./observer/observer-observable.js";
import {CmdHistory} from "./cmd/cmd-history.js";


import * as loglevel from './libraries/loglevel.js';
import {CmdTileChangeColor} from "./cmd/tile/cmd-tile-change-color";
import {Vec2} from "./utility/vec2";
import {CmdMosaicClear} from "./cmd/mosaic/cmd-mosaic-clear";
import {CmdFactory} from "./cmd/cmd-factory";
import {CmdMosaicViewSetCanvasViewPort} from "./cmd/view/cmd-mosaic-view-set-canvas-view-port";
import {MouseGestureDetector} from "./gesture/mouse-gesture-detector";
import {ControlMainDrag, MainClick, MainDrag, Pinch} from "./gesture/events/pointer-event";
import {TouchGestureDetector} from "./gesture/touch-gesture-detector";
import {CmdMosaicViewSetCanvasScale} from "./cmd/view/cmd-mosaic-view-set-canvas-scale";
import {CmdList} from "./cmd/cmd-list";
import {CmdMosaicViewSetIsScaling} from "./cmd/view/cmd-mosaic-view-set-is-scaling";
import {Analytics} from "./analytics/analytics";
import {Wheeling} from "./gesture/events/wheeling";

export {MosaicEditor}

var log = loglevel.getLogger("editor");

class MosaicEditor extends Observer {
    constructor(canvasId, persistantState) {
        super();
        this._setupDone = false;
        this._canvasId = canvasId;

        this._persistantState = persistantState;
        this._persistantState.addObserver(this);
        this._commandHistory = new CmdHistory();
        this._commandHistory.addObserver(this);
        // persistence is an async observer
        this._commandHistory.addObserver(persistantState, true);
        this._commandHistory.addObserver(new Analytics(), true);

        this._networkCommandHistory = new CmdHistory();

        this._colorPalette = null;


        this._mouseDetector = new MouseGestureDetector();
        this._touchDetector = new TouchGestureDetector();

        // for pinching
        this._startCanvasViewPort = null;
        this._startCanvasScale = null;

        let _this = this;
        new p5(function (s) {
            _this.sketch = s;

            s.setup = function () {
                _this.setup(s);
            }

            s.draw = function () {
                _this.draw(s);
            }

            s.windowResized = function (evt) {
                _this.windowResized(s, evt);
            }

        }, canvasId);
    }

    set mosaicView(mosaicView) {
        this._mosaicView = mosaicView;
        this._mosaicView.addObserver(this);
    }

    /*
    set shapesListSelector(shapesListSelector) {
        this._shapesListSelector = shapesListSelector;
        shapesListSelector.addObserver(this, true);
        this._commandHistory.addObserver(shapesListSelector, true);
        this._networkCommandHistory.addObserver(shapesListSelector, true);
    }
*/

    set grid(grid) {
        this._mosaicGrid = grid;
        this._mosaicGrid.addObserver(this);
        this._mosaicGridView = new MosaicGridSimpleView(this._mosaicGrid);
    }

    set colorPalette(colorSelector) {
        this._colorPalette = colorSelector;
        this._colorPalette.addObserver(this);
    }

    get colorPalette() {
        return this._colorPalette;
    }


    /*set gridSizeSelector(gridSizeSelector) {
        this._gridSizeSelector = gridSizeSelector;
        gridSizeSelector.addObserver(this, true);
        this._commandHistory.addObserver(gridSizeSelector, true);
        this._networkCommandHistory.addObserver(gridSizeSelector, true);
    }*/

    addGUIElement(guiElement) {
        log.debug(guiElement)
        guiElement.addObserver(this, true)
        this._commandHistory.addObserver(guiElement, true);
        this._networkCommandHistory.addObserver(guiElement, true);
    }

    update(component, params) {
        //log.debug(component, params);

        if (params && params.cmdName) { // it's a command
            let cmd  = CmdFactory.createFromJS(params);
            // not all commands need mosaicView or persistence, but we set this on all...
            cmd.editor = this;
            cmd.mosaicView = this._mosaicView;
            cmd.persistence = this._persistantState;
            this._commandHistory.do(cmd);
        }
        switch (component) {
            case this._mosaicView:
                if (this._setupDone === true) {
                    this._mosaicView.paint(this.sketch.drawingContext, params);
                }
                break;
            case this._mouseDetector:
            case this._touchDetector:
                this._handleInput(params);
                break;
        }

        //console.log("Update:", _mosaicGrid);
        if (component === this._persistantState) {
            //console.log("Received command from persistantstate")
            let cmd = params;
            cmd.mosaicView = this._mosaicView;
            //params.mosaicGrid = this._mosaicGrid;
            //this._commandHistory._dodo(cmd);
            this._networkCommandHistory.do(cmd);
        } else if (component === this._commandHistory) {
            //this.saveState(false);
            //this.sendToDeepStream(params);
        } else if (component === this._colorPalette && this._setupDone) {
            this._mosaicView.paint(this.sketch.drawingContext);
        }
        // else if (component instanceof MosaicGridSizeSelector) {
        //     log.info("Changing grid size to ",params.cols, "x", params.rows);
        //     this.setGridSize(params.cols, params.rows);
        // }
    }


    setup(sketch) {
        let editorWidth = Math.min(document.querySelector("#" + this._canvasId).clientWidth, sketch.windowWidth);
        let editorHeight = Math.min(document.querySelector("#" + this._canvasId).clientHeight, sketch.windowHeight);

        sketch.createCanvas(editorWidth, editorHeight);


        let _this = this;

        if (this._mosaicView) this._mosaicView.paint(sketch.drawingContext);

        if (document.querySelector("#clearGridButton")) {
            document.querySelector("#clearGridButton").addEventListener("click", function () {

                let cmd = new CmdMosaicClear();
                cmd.mosaicView = _this._mosaicView;

                _this._commandHistory.do(cmd);

            });
        }
        /*
                document.querySelector("#fullscreenButton").addEventListener("click", function () {

                    let fs = sketch.fullscreen();
                    if (fs) {
                        log.info("Leaving fullscreen");
                    } else {
                        log.info("Entering fullscreen");
                    }
                    sketch.fullscreen(!fs);
                });


                document.querySelector("#undoButton").addEventListener("click", function () {
                    _this.undo();
                });

                document.querySelector("#redoButton").addEventListener("click", function () {
                    _this.redo();
                });
        */

        document.onkeydown = function (evt) {
            var evtobj = window.event ? event : evt;

            if (evtobj.keyCode === 90 && evtobj.ctrlKey) { // ctrl-z
                log.debug("Ctrl-Z pressed");
                _this.undo();
            } else if (evtobj.keyCode === 89 && evtobj.ctrlKey) { // ctrl-y
                log.debug("Ctrl-Y pressed");
                _this.redo();
            } else if (evtobj.keyCode === 83 && evtobj.ctrlKey) { // ctrl-s
                //_this.saveState(true);
            } else if (evtobj.keyCode === 79 && evtobj.ctrlKey) { // ctrl-o
                //_this.loadState(true);
            }
        };
        let el = document.querySelector("canvas");
        el.addEventListener("touchstart", function (evt) {
            if (evt.target.parentElement.id === _this._canvasId)
                _this._touchDetector.handleTouchStart(evt);
                //_this.touchStarted(_this.sketch, evt);
        }, false);
        el.addEventListener("touchend", function (evt) {
            if (evt.target.parentElement.id === _this._canvasId)
                _this._touchDetector.handleTouchEnd(evt);
                //_this.touchEnded(_this.sketch, evt);
        }, false);
        el.addEventListener("touchcancel", function (evt) {

        }, false);
        el.addEventListener("touchmove", function (evt) {
            if (evt.target.parentElement.id === _this._canvasId)
                _this._touchDetector.handleTouchMove(evt);
                //_this.touchMoved(_this.sketch, evt);
        }, false);

        el = document.querySelector("canvas");
        el.addEventListener("mousedown", function (evt) {
            if (evt.target.parentElement.id === _this._canvasId)
                _this._mouseDetector.handleMousePressed(evt);
        }, false);
        el.addEventListener("mouseup", function (evt) {
            if (evt.target.parentElement.id === _this._canvasId)
                _this._mouseDetector.handleMouseReleased(evt);
        }, false);
        el.addEventListener("mousemove", function (evt) {
            //console.log(evt);
            if (evt.target.parentElement.id === _this._canvasId)
                _this._mouseDetector.handleMouseMoved(evt);
        }, false);
        el.addEventListener("wheel", function (evt) {
            //console.log(evt);
            if (evt.target.parentElement.id === _this._canvasId)
                _this._mouseDetector.handleMouseWheel(evt);
        }, false);
        this._mouseDetector.addObserver(this);
        this._touchDetector.addObserver(this);
        //this.loadState();
        this._setupDone = true;
        log.info("Setup done");

    }

    notify(msg, type) {
        $.notify(msg, {
            type: type, delay: 1500, newest_on_top: true, placement: {
                from: "top",
                align: "center"
            }
        });
    }

    //
    // fromJS(jsonState) {
    //     log.trace("Loading mosaic editor project from JSON.");
    //     log.trace("JSON: ", jsonState);
    //
    //     if (!jsonState) {
    //         log.error("Invalid JSON data.");
    //         return;
    //     }
    //     let project = JSON.parse(jsonState);
    //
    //     // TODO: We should have a ColorPalette.fromObject() function...
    //     this._colorPalette.fromJS(JSON.stringify(project.colorPalette));
    //
    //     // TODO: We should have a MosaicGrid.fromObject() function
    //     this._mosaicGrid.fromJS(JSON.stringify(project.grid));
    //
    // }
/*
    undo() {
        let result = this._commandHistory.undo();
        if (!result) {
            log.info("Nothing to undo");
        }
    }

    redo() {
        let result = this._commandHistory.redo();
        if (!result) {
            log.info("Nothing to redo");
        }
    }*/


    draw(sketch) {

    }

    _handleInput(evt) {
        let cmdList;
        let cmdScale;
        let cmd;
        let movement;

        switch (evt.name) {

            case MainDrag.name:
                if (evt.finished) {
                    if (this._commandHistory.isGrouping()) {
                        this._commandHistory.endGroup();
                    }
                } else {
                    if (!this._commandHistory.isGrouping()) {
                        this._commandHistory.startGroup();
                    }
                }
                // flows through to MainClick
            case MainClick.name:
                let tile = this._mosaicView.getTesselAt(this.sketch.drawingContext, evt.clientX, evt.clientY);
                if (tile !== null) {
                    cmd = new CmdTileChangeColor();
                    cmd.mosaicView = this._mosaicView;
                    cmd.tesselId = tile.id;
                    cmd.colorIndex = this._colorPalette.selectedColorIndex;
                    this._commandHistory.do(cmd);
                }
                break;
            case ControlMainDrag.name:
                cmdList = new CmdList();
                if (!evt.finished) {
                    if (!this._networkCommandHistory.isGrouping()) {
                        this._networkCommandHistory.startGroup();
                    }
                    if (this._startCanvasViewPort === null) {
                        this._startCanvasViewPort = this._mosaicView.canvasViewPort;
                        let cmdIsScaling = new CmdMosaicViewSetIsScaling();
                        cmdIsScaling.mosaicView = this._mosaicView;
                        cmdIsScaling.isScaling = true;
                        cmdList.addCommand(cmdIsScaling);
                    }
                }

                cmd = new CmdMosaicViewSetCanvasViewPort();
                cmd.mosaicView = this._mosaicView;
                movement = new Vec2(evt.movementX/this.sketch.width, evt.movementY/this.sketch.height);
                console.log(movement)
                cmd.canvasViewPort = this._startCanvasViewPort.add(movement);
                console.log(cmd.canvasViewPort)
                cmdList.addCommand(cmd);

                if (evt.finished) {
                    if (this._networkCommandHistory.isGrouping()) {
                        this._networkCommandHistory.endGroup();
                    }
                    this._startCanvasViewPort = null;

                    let cmdIsScaling = new CmdMosaicViewSetIsScaling();
                    cmdIsScaling.mosaicView = this._mosaicView;
                    cmdIsScaling.isScaling = false;
                    cmdList.addCommand(cmdIsScaling);
                }
                this._networkCommandHistory.do(cmdList);
                break;
            case Pinch.name:
                cmdList = new CmdList();
                if (!evt.finished) {
                    if (!this._networkCommandHistory.isGrouping()) {
                        this._networkCommandHistory.startGroup();
                    }
                    if (this._startCanvasViewPort === null) {
                        this._startCanvasViewPort = this._mosaicView.canvasViewPort;
                        this._startCanvasScale = this._mosaicView.canvasScale;
                        let cmdIsScaling = new CmdMosaicViewSetIsScaling();
                        cmdIsScaling.mosaicView = this._mosaicView;
                        cmdIsScaling.isScaling = true;
                        cmdList.addCommand(cmdIsScaling);
                    }
                }

                cmd = new CmdMosaicViewSetCanvasViewPort();
                cmd.mosaicView = this._mosaicView;
                movement = new Vec2(evt.movementX/this.sketch.width, evt.movementY/this.sketch.height);

                cmd.canvasViewPort = this._startCanvasViewPort.add(movement);
                cmdList.addCommand(cmd);

                cmdScale = new CmdMosaicViewSetCanvasScale();
                cmdScale.mosaicView = this._mosaicView;
                cmdScale.canvasScale = this._startCanvasScale*evt.pointerDistPercent;
                cmdList.addCommand(cmdScale);


                if (evt.finished) {
                    if (this._networkCommandHistory.isGrouping()) {
                        this._networkCommandHistory.endGroup();
                    }
                    this._startCanvasViewPort = null;
                    this._startCanvasScale = null;
                    let cmdIsScaling = new CmdMosaicViewSetIsScaling();
                    cmdIsScaling.mosaicView = this._mosaicView;
                    cmdIsScaling.isScaling = false;
                    cmdList.addCommand(cmdIsScaling);
                }
                this._networkCommandHistory.do(cmdList);
                break;
            case Wheeling.name:
                cmdList = new CmdList();
                if (!evt.finished) {
                    if (!this._networkCommandHistory.isGrouping()) {
                        this._networkCommandHistory.startGroup();
                    }
                    if (this._startCanvasScale === null) {
                        this._startCanvasScale = this._mosaicView.canvasScale;
                        let cmdIsScaling = new CmdMosaicViewSetIsScaling();
                        cmdIsScaling.mosaicView = this._mosaicView;
                        cmdIsScaling.isScaling = true;
                        cmdList.addCommand(cmdIsScaling);
                    }
                }

                cmdScale = new CmdMosaicViewSetCanvasScale();
                cmdScale.mosaicView = this._mosaicView;
                let scaleFactor = 1;
                if (evt.originalEvt) {
                    scaleFactor = evt.originalEvt.deltaY > 0 ? 0.9 : 1.1;
                }

                cmdScale.canvasScale = this._mosaicView.canvasScale*scaleFactor;
                cmdList.addCommand(cmdScale);


                if (evt.finished) {
                    if (this._networkCommandHistory.isGrouping()) {
                        this._networkCommandHistory.endGroup();
                    }

                    this._startCanvasScale = null;
                    let cmdIsScaling = new CmdMosaicViewSetIsScaling();
                    cmdIsScaling.mosaicView = this._mosaicView;
                    cmdIsScaling.isScaling = false;
                    cmdList.addCommand(cmdIsScaling);
                }
                this._networkCommandHistory.do(cmdList);
                break;
            default:
                log.debug("Received input event ", evt.name)
        }

    }


    windowResized(sketch) {
        let editorWidth = Math.min(document.querySelector("#" + this._canvasId).clientWidth, sketch.windowWidth);
        let editorHeight = Math.min(document.querySelector("#" + this._canvasId).clientHeight, sketch.windowHeight);
        log.debug("New canvas size: ", editorWidth + " x " + editorHeight);

        sketch.resizeCanvas(editorWidth, editorHeight);
        //sketch.background(200);
        this.update(this._mosaicView);
        //this.setup(sketch);
    }



    toString() {
        return "MosaicEditor []";
    }
}