export {TesselShapeFactory}
import {TesselShapeOval} from "./tessel-shape-oval";
import {TesselShapeRoundRect} from "./tessel-shape-round-rect";
import {TesselShapeTriangle} from "./tessel-shape-triangle";
import {TesselShapePath} from "./tessel-shape-path";

import * as loglevel from '../libraries/loglevel.js';

var log = loglevel.getLogger("mosaic");
class TesselShapeFactory {

    static create(shapeName) {
        switch (shapeName) {
            case TesselShapeRoundRect.name:
            case "TileShapeRoundRect": // old version used "tile..." names so we add here for backward compatibility
                return new TesselShapeRoundRect();
            case TesselShapeOval.name:
            case "TileShapeOval":
                return new TesselShapeOval();
            case TesselShapeTriangle.name:
            case "TileShapeTriangle":
                return new TesselShapeTriangle();
            case TesselShapePath.name:
            case "TileShapePath":
                return new TesselShapePath();
            default:
                log.warn("Tried creating a tile shape not available in TesselShapeFactory: ", shapeName);
        }

        return null;
    }

    static createFromJSON(obj){
        let shapeName = obj.tesselShapeName;
        let shape = TesselShapeFactory.create(shapeName);
        shape.fromJSON(obj);
        return shape;
    }

}