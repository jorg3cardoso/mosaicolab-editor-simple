export {Cmd}

import * as loglevel from '../libraries/loglevel.js';

var log = loglevel.getLogger("cmd");

class Cmd {
    constructor() {
        this._executed = false;
    }

    get name() {
        return Cmd.name;
    }

    do () {
        this._predo();
        this._dodo();
        this._postdo();
    }

    _predo() {
        this._check();
    }

    _dodo() {

    }

    _postdo(){
        this._executed = true;
    }

    toJS() {
        return {cmdName: this.name};
    }

    fromJS(obj) {
        if (obj.cmdName !== this.name) {
            throw "Wrong command name. Was expecting '" + this.name + "'. Got '" + obj.cmdName + "' instead.";
        }
        return this;
    }

    _check() {

    }
}
