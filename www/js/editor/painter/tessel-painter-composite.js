export {TesselPainterComposite}
import {TesselPainter} from "./tessel-painter";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("painter");

class TesselPainterComposite extends TesselPainter {
    constructor() {
        super();
        this._children = [];
    }


    add(tesselPainter) {
        this._children.push(tesselPainter);
    }

    remove(tesselPainter) {
        for( var i = this._children.length-1; i >= 0; i--){
            if ( this._children[i] === tesselPainter) {
                this._children.splice(i, 1);
            }
        }
    }

    getChildren() {
        return [...this._children];
    }

    doPaint(ctx, palette, tessel) {
        this._children.forEach(function(tesselPainter){

            tesselPainter.doPaint(ctx, palette, tessel);

        })
    }
}