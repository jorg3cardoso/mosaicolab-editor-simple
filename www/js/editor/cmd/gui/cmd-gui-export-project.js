

export {CmdGUIExportProject}

import * as loglevel from '../../libraries/loglevel.js';

import {Cmd} from "../imports";

var log = loglevel.getLogger("cmd.gui");

class CmdGUIExportProject extends Cmd {

    constructor() {
        super();
        this._editor = null;
        this._persistence = null;
    }

    get name() {
        return CmdGUIExportProject.name;
    }

    get editor() {
        return this._editor;
    }

    set editor(value) {
        this._editor = value;
    }

    get persistence() {
        return this._persistence;
    }

    set persistence(value) {
        this._persistence = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Exporting project");
        if (this._editor) {
            this._editor.notify("Exporting project...", "info");
        }

        let js = this._persistence.getJS();

        let date = new Date();
        let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(js));

        let a = document.createElement("a");
        a.href = dataStr;
        a.download = "mosaic-"+date.getFullYear()+ "-" + date.getMonth() + "-" + date.getDate() + "-" + date.getHours()+ "-" + date.getMinutes() + "-"+ date.getSeconds()+".json";
a.click();

        if (this._editor) {
            this._editor.notify("Exported project!", "success");
        }
        log.debug("Exported project: ", a.download);
    }

    toJS() {
        let json = {cmdName: this.name, linkSelector: this._linkSelector};
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        super.fromJS(obj)

        this.linkSelector = obj.linkSelector;
        return this;
    }


    toString() {
        return "CmdGUIExportProject [persistence:" + this._persistence + ", linkSelector:" + this._linkSelector +"]";
    }


    /* Private methods */
    _check() {
        if (!this._persistence) {
            log.error(this.toString());
            throw("No PersistentState has been set, cannot execute command.");
        }
    }
}