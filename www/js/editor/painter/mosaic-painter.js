import {ClearTesselPainter} from "./tessel-painter";

export {MosaicPainter}

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("painter");

class MosaicPainter {
    constructor() {
        this._clearTesselPainter = new ClearTesselPainter();
    }

    paint(ctx, palette, mosaic, params) {
        this.beginPaint(ctx, palette, mosaic, params);
        this.doPaint(ctx, palette, mosaic, params);
        this.endPaint(ctx, palette, mosaic, params);
    }

    beginPaint(ctx, palette, mosaic, params) {
        ctx.save();
    }

    doPaint(ctx, palette, mosaic, params){
        let tessel = null;
        if (params && params.tesselId && !params.shape) {
            tessel = mosaic.getTesselById(params.tesselId);
            log.debug("Painting tessel ", tessel);
            if (tessel.color === null) {
                this._clearTesselPainter.paint(ctx, palette, tessel);
            }else {
                tessel.paint(ctx, palette);
            }
        } else {
            log.debug("Painting complete mosaic");
            mosaic.tessels.forEach(function(tessel){
                if (tessel.color === null) {
                    this._clearTesselPainter.paint(ctx, palette, tessel);
                }
                tessel.paint(ctx, palette);


            }.bind(this));
        }

    }

    endPaint(ctx, palette, mosaic, params) {
        ctx.restore();
    }

    getTesselAt(x, y) {

    }


}