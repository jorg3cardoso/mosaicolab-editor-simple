import {CmdTileChangeColor} from "../cmd/tile/cmd-tile-change-color";

export {PersistenceLocalStorage}

import {Persistence} from "./persistence";

import {CmdMosaicCreate} from "../cmd/mosaic/cmd-mosaic-create";
import {CmdFactory, CmdList} from "../cmd/imports";
import {CmdMosaicViewSetLayout} from "../cmd/view/cmd-mosaic-view-set-layout";
import {MosaicLayoutFactory} from "../layout/mosaic-layout-factory";
import {CmdTileChangeShape} from "../cmd/tile/cmd-tile-change-shape";
import {CmdMosaicInsertDefaultTile} from "../cmd/mosaic/cmd-mosaic-insert-default-tile";
import {CmdMosaicRemoveTile} from "../cmd/mosaic/cmd-mosaic-remove-tile";

import * as loglevel from '../libraries/loglevel.js';
import {CmdMosaicAddDefaultTile} from "../cmd/mosaic/cmd-mosaic-add-default-tile";
import {CmdMosaicClear} from "../cmd/mosaic/cmd-mosaic-clear";
import {CmdTileChangeId} from "../cmd/tile/cmd-tile-change-id";
import {CmdMosaicViewGridResize} from "../cmd/view/cmd-mosaic-view-grid-resize";
import {TesselShapeFactory} from "../mosaic/tessel-shape-factory";
import {CmdMosaicViewSetDefaultTile} from "../cmd/view/cmd-mosaic-view-set-default-tile";
import {CmdTileChangePosition} from "../cmd/tile/cmd-tile-change-position";
import {CmdMosaicBulkCommand} from "../cmd/mosaic/cmd-mosaic-bulk-command";
import {CmdGUIImportSVG} from "../cmd/gui/cmd-gui-import-svg";
import {CmdTileChangeSize} from "../cmd/tile/cmd-tile-change-size";

var log = loglevel.getLogger("persistence");

class PersistenceLocalStorage extends Persistence{
    constructor() {
        super();

        this._timeoutId = null;

        this._editorRecord = {
            version: "0.2.1",
            defaultTile: {
                w: 100,
                h: 100,
                colorIndex: null,
                shapeName: "TesselShapeTriangle"
            },
            mosaic: {
                numberOfTessels: 256,
                ids:[],
                tessels: {
                },
                layout: {
                }
            }
        };
        let editorRecord = localStorage.getItem("editor");
        if (!editorRecord) {
            log.warn("No Editor Record in LocalStorage");
        } else {
            Object.assign(this._editorRecord, JSON.parse(editorRecord));
            log.debug("Loaded editor record from localstorage: ", editorRecord)
        }
    }

    getJS() {
        return this._editorRecord;
    }

    loadFromJS(obj){
        this._editorRecord = obj;
        this.load();
    }

    load() {
        let _this = this;

        return new Promise(function(resolve, reject){
            log.debug("Loading from LocalStorage");

            log.debug("Parsing editor record", _this._editorRecord);
            if (!_this._editorRecord.mosaic) {
                throw "No mosaic in editor record";
                reject();
                return;
            }

            let cmdSetDefaultTile = new CmdMosaicViewSetDefaultTile();
            cmdSetDefaultTile.w = _this._editorRecord.defaultTile.w;
            cmdSetDefaultTile.h = _this._editorRecord.defaultTile.h;
            cmdSetDefaultTile.colorIndex = _this._editorRecord.defaultTile.colorIndex;
            cmdSetDefaultTile.shapeName = _this._editorRecord.defaultTile.shapeName;
            _this.notifyObservers(cmdSetDefaultTile);

            let mosaic = _this._editorRecord.mosaic;

            // Should set layout first so that previoius layout does not mess with the mosaic being loaded
            if (!mosaic.layout) {
                throw "No layout command in mosaic";
            }
            let cmdSetMosaicLayout = new CmdMosaicViewSetLayout();
            let layout = MosaicLayoutFactory.createFromJSON(mosaic.layout);
            cmdSetMosaicLayout.mosaicLayoutName = layout.name;
            cmdSetMosaicLayout.params = layout.params;


            _this.notifyObservers(cmdSetMosaicLayout);


            let numberOfTessels = parseInt(mosaic.numberOfTessels);
            let cmdCreateMosaic = CmdFactory.create(CmdMosaicCreate.name);
            cmdCreateMosaic.numberOfTessels = numberOfTessels;
            _this.notifyObservers(cmdCreateMosaic);

            if (mosaic.ids) {
                let cmdList = new CmdMosaicBulkCommand();
                mosaic.ids.forEach(function(key, index){
                    let tessel = mosaic.tessels[key];
                    let tesselId = parseInt(key);
                    let cmdChangeId = new CmdTileChangeId();
                    cmdChangeId.tesselIndex = index;
                    cmdChangeId.newTesselId = key;
                   // _this.notifyObservers(cmdChangeId);
                    cmdList.addCommand(cmdChangeId);

                    if (tessel.x && tessel.y) {
                        let cmdChangePosition = new CmdTileChangePosition();
                        cmdChangePosition.tesselId = tesselId;
                        cmdChangePosition.x = tessel.x;
                        cmdChangePosition.y = tessel.y;

                        //_this.notifyObservers(cmdChangeColor);
                        cmdList.addCommand(cmdChangePosition);
                    }
                    if (tessel.w && tessel.h) {
                        let cmdChangeSize = new CmdTileChangeSize();
                        cmdChangeSize.tesselId = tesselId;
                        cmdChangeSize.w = tessel.w;
                        cmdChangeSize.h = tessel.h;

                        //_this.notifyObservers(cmdChangeColor);
                        cmdList.addCommand(cmdChangeSize);
                    }
                    if (tessel.colorIndex !== undefined) {
                        let cmdChangeColor = new CmdTileChangeColor();
                        cmdChangeColor.tesselId = tesselId;
                        cmdChangeColor.colorIndex = tessel.colorIndex;

                        //_this.notifyObservers(cmdChangeColor);
                        cmdList.addCommand(cmdChangeColor);
                    }
                    if (tessel.shape !== undefined) {
                        let cmdChangeShape = new CmdTileChangeShape();
                        cmdChangeShape.tesselId = tesselId;
                        let shape = TesselShapeFactory.createFromJSON(tessel.shape);
                        cmdChangeShape.shape = shape;
                        //_this.notifyObservers(cmdChangeShape);
                        cmdList.addCommand(cmdChangeShape);
                    }

                });
                _this.notifyObservers(cmdList);
            }

            log.debug("Persistant state loaded from LocalStorage");
            resolve();
        });

    }

    _checkTesselId(tesselId) {
        if (!this._editorRecord.mosaic.tessels[tesselId]) {
            this._editorRecord.mosaic.tessels[tesselId] = {};
        }
    }
    _addToIndex(mosaic, tesselId) {
        this._editorRecord.mosaic.ids.splice(mosaic.getTesselIndex(mosaic.getTesselById(tesselId)), 0, tesselId );
    }

    _removeFromIndex(tesselId) {
        let index = this._editorRecord.mosaic.ids.indexOf(tesselId);
        if (index >= 0) {
            this._editorRecord.mosaic.ids.splice(index, 1);
        }
    }

    _changeIndex(oldTesselId, newTesselId) {
        let index = this._editorRecord.mosaic.ids.indexOf(oldTesselId);
        if (index >= 0) {
            this._editorRecord.mosaic.ids[index] = newTesselId;
        }
    }

    _saveToLocalStorage() {

        let t0 = performance.now();
        log.debug("Record changed, saving to localstorage.");
        localStorage.setItem("editor", JSON.stringify(this._editorRecord));

        var t1 = performance.now();
        log.debug("Saving to localstorage took: " + (t1 - t0) + " milliseconds.");
    }

    _updateEditorRecord(cmd) {
        let t0 = performance.now();
        let _this = this;
        let changed = false;

        // if a previous scheduled saving to localstorage was set, clear it
        clearTimeout(this._timeoutId);

        if (cmd instanceof CmdList) {
            cmd.commandList.forEach(function(cmd){
                _this._updateEditorRecord(cmd);
            })
        } else {
            log.debug("Persisting effect of Command: ", cmd.name);
            let editor = this._editorRecord;
            let tessel = null;
            switch(cmd.name) {
                case CmdMosaicViewSetDefaultTile.name:
                    editor.defaultTile.w = cmd.w;
                    editor.defaultTile.h = cmd.h;
                    editor.defaultTile.shapeName = cmd.shapeName;
                    editor.defaultTile.colorIndex = cmd.colorIndex;
                    changed = true;
                    break;
                case CmdMosaicAddDefaultTile.name:
                    this._checkTesselId(cmd._createdTesselId);
                    tessel = editor.mosaic.tessels[cmd._createdTesselId];
                    this._addToIndex(cmd.mosaicView.mosaic, cmd._createdTesselId);
                    editor.mosaic.numberOfTessels = cmd.mosaicView.mosaic.tessels.length;
                    changed = true;
                    break;
                case CmdMosaicClear.name:
                    cmd.mosaicView.mosaic.tessels.forEach(function(tessel){
                        this._checkTesselId(tessel.id);
                        editor.mosaic.tessels[tessel.id].colorIndex = null;
                    }.bind(this));
                    changed = true;
                    break;
                case CmdMosaicCreate.name:
                case CmdGUIImportSVG.name:
                case CmdMosaicViewGridResize.name:
                    editor.mosaic.numberOfTessels = cmd.mosaicView.mosaic.tessels.length;
                    editor.mosaic.tessels = {};
                    editor.mosaic.ids = [];
                    cmd.mosaicView.mosaic.tessels.forEach(function(tessel){
                        editor.mosaic.tessels[tessel.id] = {};
                        editor.mosaic.tessels[tessel.id].index = cmd.mosaicView.mosaic.getTesselIndex(cmd.mosaicView.mosaic.getTesselById(cmd._createdTesselId));
                        editor.mosaic.tessels[tessel.id].colorIndex = tessel.colorIndex;
                        editor.mosaic.tessels[tessel.id].shape = tessel.shape.toJSON();
                        editor.mosaic.tessels[tessel.id].x = tessel.x;
                        editor.mosaic.tessels[tessel.id].y = tessel.y;
                        editor.mosaic.tessels[tessel.id].w = tessel.w;
                        editor.mosaic.tessels[tessel.id].h = tessel.h;
                        editor.mosaic.ids.push(tessel.id);
                    });
                    editor.mosaic.layout = cmd.mosaicView.mosaicLayout.toJSON();
                    changed = true;
                    break;
                case CmdMosaicInsertDefaultTile.name:
                    this._checkTesselId(cmd._createdTesselId);
                    tessel = editor.mosaic.tessels[cmd._createdTesselId];
                    this._addToIndex(cmd.mosaicView.mosaic, cmd._createdTesselId);
                    editor.mosaic.numberOfTessels = cmd.mosaicView.mosaic.tessels.length;
                    changed = true;
                    break;
                case CmdMosaicRemoveTile.name:
                    delete editor.mosaic.tessels[cmd._tesselId];
                    this._removeFromIndex(cmd._tesselId);
                    editor.mosaic.numberOfTessels = cmd.mosaicView.mosaic.tessels.length;
                    changed = true;
                    break;
                case CmdTileChangeColor.name:
                    this._checkTesselId(cmd.tesselId);
                    /*if (!editor.mosaic.tessels[cmd.tesselId]) {
                        editor.mosaic.tessels[cmd.tesselId] = {};
                    }*/
                    tessel = editor.mosaic.tessels[cmd.tesselId];
                    tessel.colorIndex = cmd.colorIndex;
                    changed = true;
                    break;
                case CmdTileChangeId.name:
                    let previous = null;
                    if (editor.mosaic.tessels[cmd._previousTesselId]) {
                        previous = editor.mosaic.tessels[cmd._previousTesselId];
                    }
                    delete editor.mosaic.tessels[cmd._previousTesselId];
                    this._changeIndex(cmd._previousTesselId, cmd._newTesselId);

                    editor.mosaic.tessels[cmd._newTesselId] = previous === null? {} : previous;
                    changed = true;
                    break;
                case CmdTileChangeShape.name:
                    this._checkTesselId(cmd.tesselId);
                    /*if (!editor.mosaic.tessels[cmd.tesselId]) {
                        editor.mosaic.tessels[cmd.tesselId] = {};
                    }*/
                    tessel = editor.mosaic.tessels[cmd.tesselId];

                    tessel.shape = cmd.mosaicView.mosaic.getTesselById(cmd.tesselId).shape.toJSON();
                    changed = true;
                    break;
                case CmdMosaicViewSetLayout.name:
                    editor.mosaic.layout = cmd.mosaicView.mosaicLayout.toJSON();
                    //editor.mosaic.layout.params = cmd.params;
                    changed = true;
                    break;
                default:
                    log.info("Don't know how to persist ", cmd.name);
            }
             /*if (cmd instanceof CmdMosaicViewGridResize) {
                editor.mosaic.numberOfTessels = cmd.mosaicView.mosaic.tessels.length;
                editor.mosaic.tessels = {};
                editor.mosaic.ids = [];
                cmd.mosaicView.mosaic.tessels.forEach(function(tessel){
                    editor.mosaic.tessels[tessel.id] = {};
                    editor.mosaic.tessels[tessel.id].colorIndex = tessel.colorIndex;
                    editor.mosaic.tessels[tessel.id].shape = tessel.shape.toJSON();
                    editor.mosaic.tessels[tessel.id].x = tessel.x;
                    editor.mosaic.tessels[tessel.id].y = tessel.y;
                    editor.mosaic.tessels[tessel.id].w = tessel.w;
                    editor.mosaic.tessels[tessel.id].h = tessel.h;
                    editor.mosaic.ids.push(tessel.id);
                });
                editor.mosaic.layout = cmd.mosaicView.mosaicLayout.toJSON();
                changed = true;
            }*/

        }

        if (changed) {
            this._timeoutId = setTimeout(this._saveToLocalStorage.bind(this), 1000);
        }

        var t1 = performance.now();

        log.debug("Updating editor record took: " + (t1 - t0) + " milliseconds.");
    }

    update(observable, cmds)  {
        super.update(observable, cmds);
        this._updateEditorRecord(cmds);
    }
}
