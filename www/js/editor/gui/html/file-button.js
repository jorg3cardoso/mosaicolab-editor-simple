import {HTMLObserverObservable} from "./html-observer-observable";

export {FileButton}

import * as loglevel from '../../libraries/loglevel.js';
var log = loglevel.getLogger("gui.html");

class FileButton extends HTMLObserverObservable {
    constructor() {
        super();
        //this._buttonEl = null;

        this._command = null;
        log.debug("FileButton initialized", this);
    }

    set command(command) {
        this._command = command;
        this._buildHTML();
    }


    _buildHTML() {
        let a = document.createElement("a");
        a.classList.add("mt-2", "btn", "btn-block", "btn-outline-primary", "btn-lg");
        a.setAttribute("role", "button");
        a.setAttribute("type", "button");

        let file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "file");
        file.setAttribute("style", "display:none;");

        this.innerHTML = '';
        this.appendChild(a);

        // Create and append icon
        if (this.dataset.src) {
            let icon = document.createElement("img");
            icon.setAttribute("src", this.dataset.src);
            icon.setAttribute("width", this.dataset.width);
            icon.setAttribute("height", this.dataset.height);
            a.appendChild(icon);
        }


        // Create and append label
        let label = document.createTextNode(this.dataset.label || this._command.text)

        a.appendChild(label);

        this.appendChild(file);

        let _this = this;
        a.addEventListener("click", function(evt) {
            log.trace("Button clicked", evt);
            file.click();
        });



        file.addEventListener('change', function (evt) {

            var files = evt.target.files; // FileList object

            // files is a FileList of File objects. List some properties.
            var output = [];
            for (var i = 0, f; f = files[i]; i++) {
                var reader = new FileReader();

                // Closure to capture the file information.
                reader.onload = (function (theFile) {
                    return function (e) {
                        // Render thumbnail.
                        _this.notifyObservers(Object.assign(_this._command.value, {data: e.target.result}));
                        // reset file input so that the same file can be opened again
                        file.value = '';
                    };
                })(f);

                // Read in the image file as a data URL.
                reader.readAsText(f);
            }

        }, false);
    }

    attributeChangedCallback(name, oldValue, newValue) {

    }
}

customElements.define('mosaic-file-button', FileButton);

