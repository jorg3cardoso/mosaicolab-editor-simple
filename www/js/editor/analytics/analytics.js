export {Analytics}
import {Observer} from "../observer/observer-observable";
import {CmdList} from "../cmd/cmd-list";
import {CmdTileChangeColor} from "../cmd/tile/cmd-tile-change-color";
import * as loglevel from "../libraries/loglevel";


var log = loglevel.getLogger("analytics");

class Analytics extends Observer {
    constructor() {
        super();
    }

    update(observable, cmds)  {
        super.update(observable, cmds);
        let _this = this;
        if (cmds instanceof CmdList) {
            cmds.commandList.forEach(function(cmd){
                _this.update(observable, cmd);
            })
        } else {
            switch (cmds.name) {
                case CmdTileChangeColor.name:
                    gtag('event', 'tile', {
                        'event_category' : 'paint',
                        'event_label' : cmds.colorIndex,
                        'event_callback': function() {
                            log.info("Sent analytics event");
                        }
                    });
                    break;
                default:
                    gtag('event', cmds.name, {
                        'event_category' : 'command',
                        'event_callback': function() {
                            log.info("Sent analytics event");
                        }
                    });
                    break;
            }
        }
    }
}