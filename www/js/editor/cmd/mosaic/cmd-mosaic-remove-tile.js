import {CmdList} from "../imports";
import {CmdUndoable} from "../imports";
import * as loglevel from '../../libraries/loglevel.js';
import {CmdMosaicInsertDefaultTile} from "./cmd-mosaic-insert-default-tile";
import {CmdTileChangeColor} from "../tile/cmd-tile-change-color";

export {CmdMosaicRemoveTile}

var log = loglevel.getLogger("cmd.mosaic");

class CmdMosaicRemoveTile extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._tesselId = null;
        this._removedTessel = null;
        this._removedTesselPosition = null;
    }

    get name() {
        return CmdMosaicRemoveTile.name;
    }
    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get tesselId() {
        return this._tesselId;
    }

    set tesselId(value) {
        this._tesselId = value;
    }

    _dodo() {
        super._dodo();
        log.debug("Removing tile with id: ", this._tesselId );

        let tesselToRemove = this._mosaicView.mosaic.getTesselById(this._tesselId);
        if (tesselToRemove === null) {
            log.warn("No tile with id: ", this._tesselId, " found in mosaic!");
            return;
        }

        this._removedTessel = tesselToRemove;
        this._removedTesselPosition = this._mosaicView.mosaic.getTesselIndex(this._removedTessel);
        this._mosaicView.mosaic.removeTessel(tesselToRemove);
        log.debug("Removed tile at position: ", this._removedTesselPosition, this._removedTessel.toString() );

    }

    getUndoCommand() {
        let undo = new CmdList();

        let cmdInsertTessel = new CmdMosaicInsertDefaultTile();
        cmdInsertTessel.mosaicView = this._mosaicView;
        cmdInsertTessel.positionToInsert = this._removedTesselPosition;
        cmdInsertTessel.idToInsert = this._removedTessel.id;
        undo.addCommand(cmdInsertTessel);

        let cmdChangeColor = new CmdTileChangeColor();
        cmdChangeColor.mosaicView = this._mosaicView;
        cmdChangeColor.tesselId = this._removedTessel.id;
        cmdChangeColor.colorIndexd = this._removedTessel.colorIndex;
        undo.addCommand(cmdChangeColor);

        return undo;

    }

    hadEffect() {
        super.hadEffect();

        return true;
    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {tesselId: this._tesselId});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.tesselId = obj.tesselId;
        return this;
    }


    toString() {
        return "CmdMosaicRemoveTile [mosaicView:" + this._mosaicView + ", tesselId: " + this._tesselId + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (this._tesselId === null || this._tesselId === undefined) {
            log.error(this.toString());
            throw("No tile id provided, cannot execute command.");
        }
    }
}