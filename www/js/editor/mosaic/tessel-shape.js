
export {TesselShape}

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("mosaic");

class TesselShape {
    constructor() { }

    /**
     * Performs the path on the drawingContext.
     * The shape will adapt to the necessary width and height.
     * @param ctx the drawing context
     * @param w the intended width of the tile
     * @param h the intended height of the tile
     */
    doPath(ctx, w, h) {  }


    get name() {
        return TesselShape.name;
    }

    scale(percent) {

    }

    toJSON() {
        return {tesselShapeName: this.name};
    }

    fromJSON(obj) {
        if (obj.tesselShapeName !== this.name) {
            log.warn("Wrong tile shape name. Was expecting '" + this.name + "'. Got '" + obj.tesselShapeName + "' instead.");
        }
    }
}

Path2D.prototype.beginPath = function() {

}



