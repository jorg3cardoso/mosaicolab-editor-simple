export {CmdHistory}
import {Observable} from "../observer/observer-observable.js";
import {Cmd} from "./imports";
import {CmdList, CmdUndoable} from "./imports";
import * as loglevel from '../libraries/loglevel.js';

var log = loglevel.getLogger("cmd");

class CmdHistory extends Observable {
    constructor() {
        super();
        this.undoHistory = new Array();
        this.redoHistory = new Array();
        this.groupStartIndex = -1;
    }

    isGrouping() {
        return this.groupStartIndex >= 0;
    }

    startGroup() {
        this.groupStartIndex = this.undoHistory.length;
    }

    endGroup() {
        if (this.groupStartIndex === this.undoHistory.length) {
            log.trace("Ending empty group");
            return;
        }
        let cmdList = new CmdList();

        let list = this.undoHistory.splice(this.groupStartIndex, this.undoHistory.length-this.groupStartIndex);
        list.forEach(function(item){
           cmdList.addCommand(item);
        });

        this.undoHistory.push(cmdList);

        this.groupStartIndex = -1;
        log.trace("Undo history: ", this.undoHistory);
    }

    do( command ) {
        if (!command instanceof  Cmd)  this.redoHistory = new Array();

        log.debug("Executing command: ", command);
        command.do();
        if (command instanceof CmdUndoable) {
            if ( command.hadEffect() ) {
                this.undoHistory.push(command);
                log.trace("Adding ", command.toString(), " to undo history");

            } else {
                log.trace(command.toString(), " had no effect, not adding to undo history");
            }
        }
        this.notifyObservers(command);
        log.trace("Undo history: ", this.undoHistory);
        //console.log(this.undoHistory);
    }

    undo() {
        let cmd = this.undoHistory.pop();

        if (cmd !== undefined) {
            let undoCmd = cmd.getUndoCommand();
            undoCmd.do();
            this.redoHistory.push(cmd);
            this.notifyObservers(undoCmd);
            return true;
        } else {
            return false;
        }
    }

    redo() {
        let cmd = this.redoHistory.pop();
        if (cmd !== undefined) {
            cmd.do();
            this.undoHistory.push(cmd);
            this.notifyObservers(cmd);
            return true;
        } else {
            return false;
        }
    }
}