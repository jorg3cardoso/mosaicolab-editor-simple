
import {MosaicEditor} from "./mosaic-editor.js";
import {MosaicColorPaletteSelector} from "./color/mosaic-color-palette-selector.js";
import {MosaicColorPalette} from "./color/mosaic-color-palette.js";
import {FetchLocal} from "./utility/FetchLocal.js";
import {PersistanceDeepStream} from "./persistence/persistance-deepstream";
import {PersistenceLocalStorage} from "./persistence/persistence-local-storage";

import * as log from './libraries/loglevel.js';
import {Mosaic} from "./mosaic/mosaic";
import {MosaicPainter} from "./painter/mosaic-painter";
import {Tessel} from "./mosaic/tessel";
import {MosaicSolidColor} from "./color/mosaic-color";

import {TesselPainterComposite} from "./painter/tessel-painter-composite";
import {TesselPainterFactory} from "./painter/tessel-painter-factory";
import {MosaicView} from "./gui/mosaic-view";

import {CmdTileChangeColor} from "./cmd/tile/cmd-tile-change-color";
import {PersistenceComposite} from "./persistence/persistence-composite";
import {CmdMosaicCreate} from "./cmd/mosaic/cmd-mosaic-create";
import {CmdMosaicViewSetLayout} from "./cmd/view/cmd-mosaic-view-set-layout";
import {CmdMosaicAddDefaultTile} from "./cmd/mosaic/cmd-mosaic-add-default-tile";
import {CmdMosaicInsertDefaultTile} from "./cmd/mosaic/cmd-mosaic-insert-default-tile";
import {CmdMosaicRemoveTile} from "./cmd/mosaic/cmd-mosaic-remove-tile";
import {CmdTileChangeShape} from "./cmd/tile/cmd-tile-change-shape";
import {CmdMosaicViewGridResize} from "./cmd/view/cmd-mosaic-view-grid-resize";
import {CmdMosaicViewSetViewport} from "./cmd/view/cmd-mosaic-view-set-viewport";
import {Dimension} from "./utility/dimension";
import {ListSelector} from "./gui/html/list-selector";
import {ShapeListSelector} from "./gui/html/shape-list-selector";
import {GridSizeListSelector} from "./gui/html/grid-size-list-selector";
import {Button} from "./gui/html/button";
import {FileButton} from "./gui/html/file-button";
import {ViewPortSelector} from "./gui/html/viewport-selector";
import {MosaicLayoutGrid} from "./layout/mosaic-layout-grid";
import {MosaicLayoutTriangles} from "./layout/mosaic-layout-triangles";
import {CmdGUIImportProject} from "./cmd/gui/cmd-gui-import-project";
import {TesselShapePath} from "./mosaic/tessel-shape-path";
import {CmdMosaicCreateFromSVG} from "./cmd/mosaic/cmd-mosaic-create-from-svg";

let url = new URL(window.location.href);

log.setLevel('debug');

log.getLogger("cmd").setLevel("info");
log.getLogger("cmd.mosaic").setLevel("info");
log.getLogger("cmd.tile").setLevel("info");
log.getLogger("cmd.view").setLevel("info");
log.getLogger("cmd.gui").setLevel("info");
log.getLogger("editor").setLevel("info");
log.getLogger("gesture").setLevel("info");
log.getLogger("gui").setLevel("debug");
log.getLogger("gui.html").setLevel("info");
log.getLogger("layout").setLevel("info");
log.getLogger("color").setLevel("info");
log.getLogger("mosaic").setLevel("info");
log.getLogger("observer").setLevel("info");
log.getLogger("painter").setLevel("info");
log.getLogger("persistence").setLevel("info");
log.getLogger("utility").setLevel("info");

/*Set version string*/
document.getElementById("versionString").innerHTML = "%%VERSION%%";

gtag('set', {
    'version': '%%VERSION%%'
});

/* Custom elements */
let promises = [];
promises.push(customElements.whenDefined('shape-list-selector'));
promises.push(customElements.whenDefined('grid-size-list-selector'));
promises.push(customElements.whenDefined('mosaic-button'));
promises.push(customElements.whenDefined('mosaic-file-button'));
promises.push(customElements.whenDefined('mosaic-viewport-selector'));

FetchLocal.getInstance().baseUrl = document.getElementById ('baseUrl').href;


let config = null;
let configUrl = url.searchParams.get("config") || document.getElementById('config')?.getAttribute("href") ||  "resources/config.json";
log.info("Loading Config from " + configUrl);

gtag('event', 'config', {
    'event_category' : 'load',
    'event_label' : configUrl
});

Promise.all(promises).then(() => {
    return FetchLocal.getInstance().fetchJSON(configUrl);
}).then(function (json) {
    log.debug("Config JSON:", json);
    config = json;
}).finally(function () {
    init(config);
});


function init(config) {
    let persistantState = new PersistenceComposite();
    persistantState.add(new PersistenceLocalStorage());
    if (config.multiUserServer) {
        persistantState.add(new PersistanceDeepStream(config.multiUserServer));
    }
    let editor = new MosaicEditor('editor', persistantState);

    let shapesListSelector = document.querySelector("#shapes-list-selector");
    shapesListSelector.items = config.tesselShapes;
    editor.addGUIElement(shapesListSelector);
    //editor.shapesListSelector = shapesListSelector;

    let gridSizeSelector = document.querySelector("#grid-size-selector");
    gridSizeSelector.items = config.gridSizes;
    //editor.gridSizeSelector = gridSizeSelector;
    editor.addGUIElement(gridSizeSelector);

    let exportProjectButton = document.querySelector("#export-project-button");
    exportProjectButton.command = config.exportProjectButton;
    editor.addGUIElement(exportProjectButton);
    //exportProjectButton.addObserver(editor);

    let importProjectButton = document.querySelector("#import-project-button");
    importProjectButton.command = config.importProjectButton;
    editor.addGUIElement(importProjectButton);
    //importProjectButton.addObserver(editor);

    let importSVGButton = document.querySelector("#import-svg-button");
    importSVGButton.command = config.importSVGButton;
    //importSVGButton.addObserver(editor);
    editor.addGUIElement(importSVGButton);


    let exportImageButton = document.querySelector("#export-image-button");
    exportImageButton.command = config.exportImageButton;
    //exportImageButton.addObserver(editor);
    editor.addGUIElement(exportImageButton);


    let homeButton = document.querySelectorAll("#homeButton");
    homeButton.forEach(function(button) {
        button.command = config.homeButton;
        editor.addGUIElement(button);
        //button.addObserver(editor);
    })



    let undoButton = document.querySelectorAll("#undoButton");
    undoButton.forEach(function (button) {
        button.command = config.undoButton;
        //button.addObserver(editor);
        editor.addGUIElement(button);
    });


    let redoButton = document.querySelectorAll("#redoButton");
    redoButton.forEach(function(button){
        button.command = config.redoButton;
        //button.addObserver(editor);
        editor.addGUIElement(button);
    })


    let saveButton = document.querySelectorAll("#saveButton");
    saveButton.forEach(function (button) {
        button.command = config.saveButton;
        //button.addObserver(editor);
        editor.addGUIElement(button);
    })


    let fullscreenButton = document.querySelectorAll("#fullscreenButton");
    fullscreenButton.forEach(function (button) {
        button.command = config.fullscreenButton;
        //button.addObserver(editor);
        editor.addGUIElement(button);
    })


    let viewPortSelector = document.querySelector("#view-port-selector");
    viewPortSelector.viewPort = config.viewPorts;
    //viewPortSelector.addObserver(editor);
    editor.addGUIElement(viewPortSelector);



    /* Palettes*/
    let sel = document.querySelector("#colorPaletteSelect");
    sel.innerHTML = "";
    config.palettes.forEach(function (option) {
        let op = document.createElement("option");
        op.value = option.url;
        op.innerHTML = option.name;
        sel.appendChild(op);
    });

    /**
     * Touches on the canvas hide the side drawer menu automatically
     */
    document.body.addEventListener("touchstart", function (evt) {
        //log.debug(evt);
        if (evt.target.parentElement.id === 'editor') {
            if (document.querySelector('#app__drawer').classList.contains('open')) {
                //log.debug("Hiding drawer");
                //$('#menuDrawer').drawer('hide');
                evt.preventDefault();
                evt.stopImmediatePropagation();
                evt.stopPropagation();
            }
        }
    }, false);

    document.body.addEventListener("touchended", function (evt) {
        //log.debug(evt);
        if (evt.target.parentElement.id === 'editor') {
            if (document.querySelector('#app__drawer').classList.contains('open')) {
                log.debug("Hiding drawer");
                $('#app__drawer').drawer('hide');
                evt.preventDefault();
                evt.stopImmediatePropagation();
                evt.stopPropagation();
            }
        }
    }, false);

    document.body.addEventListener("mousedown", function (evt) {
        //log.debug(evt);
        if (evt.target.parentElement.id === 'editor') {
            if (document.querySelector('#app__drawer').classList.contains('open')) {
                log.debug("Ignoring event");
                //log.debug("Hiding drawer");
                //$('#menuDrawer').drawer('hide');
                evt.stopImmediatePropagation();
            }
        }
    }, false);

    document.body.addEventListener("mouseup", function (evt) {
        //log.debug(evt);
        if (evt.target.parentElement.id === 'editor') {
            if (document.querySelector('#app__drawer').classList.contains('open')) {
                log.debug("Hiding drawer");
                $('#app__drawer').drawer('hide');
                evt.stopImmediatePropagation();
            }
        }
    }, false);



    let mosaicView = new MosaicView();
    editor.mosaicView = mosaicView;

    let colorPalette = new MosaicColorPalette('#colorPaletteInTab,#colorPaletteTop');
    editor.colorPalette = colorPalette;
    mosaicView.colorPalette = colorPalette;




    let colorPaletteSelector = new MosaicColorPaletteSelector('colorPaletteSelect');
    colorPalette.addObserver(colorPaletteSelector);
    editor.colorPaletteSelector = colorPaletteSelector;
    colorPalette.colorPaletteSelector = colorPaletteSelector;



    let c = url.searchParams.get("project");
    if (c !== null) {
        let cmd = new CmdGUIImportProject();
        cmd.persistence = persistantState;
        cmd.fileURI = c;
        cmd.do();
        setTimeout(function(){test()}, 5000);


    } else {
        persistantState.load()
            .then(function(){
                log.debug("Persistence loaded.............")
                setTimeout(function(){test()}, 5000);

            }).catch(function(error){
                log.warn("Persistence failed to load", error);
                createEmptyMosaic();
        });
    }

    c = url.searchParams.get("palette");

    if (c !== null) {
        log.debug("Loading pallette from: ", c);
        let option = document.createElement("option");
        option.value = c;
        option.innerHTML = c;
        document.querySelector("#colorPaletteSelect").add(option);
    }

    // Hide
    if (config.hide) {
        config.hide.forEach(function (cssSelector) {
            log.info("Hiding '", cssSelector, "'");
            document.querySelector(cssSelector).style.display = "none";
        });
    }
    //test();



function createEmptyMosaic() {

    let cmdCreateMosaic = new CmdMosaicCreate();
    cmdCreateMosaic.numberOfTessels = 4*4;
    cmdCreateMosaic.mosaicView = mosaicView;
    editor._commandHistory.do(cmdCreateMosaic);


    let cmd = new CmdMosaicViewSetLayout();
    cmd.mosaicView = mosaicView;
    cmd.mosaicLayoutName = MosaicLayoutTriangles.name;
    cmd.params = [4, 4];
    editor._commandHistory.do(cmd);
    //test();
}


function test() {
    //    let cmd = new CmdMosaicCreateFromSVG();
    //    cmd.mosaicView = mosaicView;
    //    cmd.numberOfTessels = 1;
    //editor._commandHistory.do(cmd);



    // console.log("Running test");
    // let cmd = new CmdMosaicViewSetViewport();
    // cmd.mosaicView = mosaicView;
    // cmd.mosaicViewPort = new Dimension(0, 0, 0.5, 0.5);
    // editor._commandHistory._dodo(cmd);
    // let cmd = new CmdMosaicViewGridResize();
    // cmd.mosaicView = mosaicView;
    // cmd.cols = 8;
    // cmd.rows = 8;
    // editor._commandHistory._dodo(cmd);

    // let cmd = new CmdMosaicInsertDefaultTile();
    // cmd.mosaicView = mosaicView;
    // cmd.positionToInsert = 10;
    // editor._commandHistory._dodo(cmd);
    //
    // let cmd1 = new CmdTileChangeColor();
    // cmd1.mosaicView = mosaicView;
    // cmd1.tesselId = mosaicView.mosaic.tessels[10].id;
    // cmd1.colorId = 1;
    // editor._commandHistory._dodo(cmd1);
    // //mosaicView.mosaicLayout.doLayout(mosaicView.mosaic);

    // let cmd2 = new CmdTileChangeShape();
    // cmd2.mosaicView = mosaicView;
    // cmd2.tesselId = 5;
    // cmd2.shapeName = TesselShapeRoundRect.name;
    // editor._commandHistory._dodo(cmd2);
    // let cmd2 = new CmdMosaicRemoveTile();
    // cmd2.mosaicView = mosaicView;
    // cmd2.tesselId = cmd._createdTesselId;
    // editor._commandHistory._dodo(cmd2);
    //console.log(mosaicView);

    //
    // let tile = new Tessel();
    // tile.w = 100;
    // tile.h = 100;
    // tile.color = null;
    // tile.shape = new TesselShapeOval();
    // let painter = new TesselPainterComposite();
    // painter.add(TesselPainterFactory.instance.createTesselPainter("SolidTesselPainter"));
    // painter.add(TesselPainterFactory.instance.createTesselPainter("OutlineTesselPainter"));
    // tile.painter = painter;//new SolidTesselPainter(); //new ClearTesselPainter();
    // mosaicView.mosaic.addTessel(tile);
}



    //let t = mosaicView.getTesselAt(ctx, ctx.canvas.width/2-50, ctx.canvas.height/2-50);
    //let cmd = new CmdTileChangeColor(t, new MosaicSolidColor(0,0, 200));
    //cmd._dodo();
}
