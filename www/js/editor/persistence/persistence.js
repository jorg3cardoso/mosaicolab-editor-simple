export {Persistence}

import {ObservableObserver} from "../observer/observer-observable.js";
import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("persistence");

class Persistence extends ObservableObserver{
    constructor() {
        super();
        log.trace(this._persistantState);
    }
    add(persistence) {}
    remove(persistence) {}
    getChildren() {}
    getJS() {
    }
    loadFromJS(obj){}
    load() {
    }

    update(observable, cmds)  {
        super.update(observable, cmds);
    }
}
