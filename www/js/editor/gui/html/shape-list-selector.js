import {HTMLObserverObservable} from "./html-observer-observable";

export {ShapeListSelector}

import * as loglevel from '../../libraries/loglevel.js';
import {ListSelector} from "./list-selector";
import {CmdMosaicViewSetDefaultTile} from "../../cmd/view/cmd-mosaic-view-set-default-tile";
import {TesselShapeOval} from "../../mosaic/tessel-shape-oval";
import {TesselShapeRoundRect} from "../../mosaic/tessel-shape-round-rect";
import {TesselShapeTriangle} from "../../mosaic/tessel-shape-triangle";
var log = loglevel.getLogger("gui.html");

class ShapeListSelector extends ListSelector {
    constructor() {
        super();
    }

    update(observable, params) {
        super.update(observable, params);
        if (params instanceof CmdMosaicViewSetDefaultTile) {
            let shapeName = params.shapeName;
            switch (shapeName) {
                case TesselShapeOval.name:
                    this.select("Circles");
                    break;
                case TesselShapeRoundRect.name:
                    this.select("Rounded Squares");
                    break;
                case TesselShapeTriangle.name:
                    this.select("Triangles");
                    break;
            }
        }
    }


    attributeChangedCallback(name, oldValue, newValue) {
        // When the drawer is disabled, update keyboard/screen reader behavior.
        // if (this.disabled) {
        //     this.setAttribute('tabindex', '-1');
        //     this.setAttribute('aria-disabled', 'true');
        // } else {
        //     this.setAttribute('tabindex', '0');
        //     this.setAttribute('aria-disabled', 'false');
        // }
        // TODO: also react to the open attribute changing.
    }
}

customElements.define('shape-list-selector', ShapeListSelector);

