

export {CmdGUIExportImage}

import * as loglevel from '../../libraries/loglevel.js';

import {Cmd} from "../imports";

var log = loglevel.getLogger("cmd.gui");

class CmdGUIExportImage extends Cmd {

    constructor() {
        super();
        this._editor = null;
        this._mosaicView = null;
    }

    get name() {
        return CmdGUIExportImage.name;
    }


    get editor() {
        return this._editor;
    }

    set editor(value) {
        this._editor = value;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }


    _dodo() {
        super._dodo();
        log.debug("Exporting image");
        if (this._editor) {
            this._editor.notify("Exporting image...", "info");
        }
        let _this = this;

        this._mosaicView.fullCanvas().toBlob(function(blob) {
            let a = document.createElement("a");
            let date = new Date();
            a.download = "mosaic-"+date.getFullYear()+ "-" + date.getMonth() + "-" + date.getDate() + "-" + date.getHours()+ "-" + date.getMinutes() + "-"+ date.getSeconds()+".png";
            a.href = URL.createObjectURL(blob);
            a.click();

            if (_this._editor) {
                _this._editor.notify("Exported image!", "success");
            }
            log.debug("Exported image. ");
        });

    }

    toJS() {
        let json = super.toJS();
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        super.fromJS(obj)
        return this;
    }


    toString() {
        return "CmdGUIExportImage [editor: " + this._editor + ", mosaicView:" + this._mosaicView  +"]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
    }
}