export {GestureDetector}
import {Observable} from "../observer/observer-observable";

import * as loglevel from '../libraries/loglevel.js';

var log = loglevel.getLogger("gesture");
class GestureDetector extends Observable{
    constructor() {
        super();
    }


    sendNotification(evt) {
        log.debug("Sending event notification: ", evt);
        this.notifyObservers(evt);
    }
}
