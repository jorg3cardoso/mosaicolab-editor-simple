export {CmdTileChangeSize}

import {CmdUndoable} from "../cmd-undoable";

import * as loglevel from '../../libraries/loglevel.js';

var log = loglevel.getLogger("cmd.tile");


class CmdTileChangeSize extends CmdUndoable {

    constructor() {
        super();
        this._mosaicView = null;
        this._tesselId = null;
        this._tessel = null;
        this._w = null;
        this._h = null;
        this._previousW = null;
        this._previousH = null;
    }

    get name() {
        return CmdTileChangeSize.name;
    }

    get mosaicView() {
        return this._mosaicView;
    }

    set mosaicView(value) {
        this._mosaicView = value;
    }

    get tesselId() {
        return this._tesselId;
    }

    set tesselId(value) {
        this._tesselId = value;
    }

    get w() {
        return this._w;
    }

    set w(value) {
        this._w = value;
    }

    get h() {
        return this._h;
    }

    set h(value) {
        this._h = value;
    }
    _dodo() {
        super._dodo();

        this._tessel = this._mosaicView.getTesselById(this._tesselId);
        if (this._tessel === null) {
            log.error("No tile with id ", this._tesselId, " found in mosaic.");
            return;
        }
        this._previousW = this._tessel.w;
        this._previousH = this._tessel.h;
        this._tessel.w = this._w;
        this._tessel.h = this.h;
        log.debug("Changed tile size to: ", this._tessel.w, this._tessel.h );

    }

    toJS() {
        let json = super.toJS();
        Object.assign(json, {tesselId: this._tessel.id, w: this._w, h: this._h});
        log.trace("Converted ", this.toString(), " to JSON: ", json);
        return json;
    }

    fromJS(obj) {
        log.trace("Converting from JSON: ", obj);
        super.fromJS(obj);

        this.tesselId = obj.tesselId;
        this.w = obj.w;
        this.h = obj.h;
        return this;
    }

    getUndoCommand() {
        let undo = new CmdTileChangeSize();
        undo.mosaicView = this._mosaicView;
        undo.tesselId = this._tesselId;
        undo.w = this._previousW;
        undo.h = this._previousH;
        return undo;

    }

    hadEffect() {
        super.hadEffect();

        if (this._previousW === this._w && this._previousH === this._h) {
            return false;
        } else {
            return true;
        }
    }



    toString() {
        return "CmdTileChangeSize [mosaicView:" + this._mosaicView + ", tesselId: " + this._tesselId + ", w: " + this._w + ", h: " + this._h + "]";
    }


    /* Private methods */
    _check() {
        if (!this._mosaicView) {
            log.error(this.toString());
            throw("No MosaicView has been set, cannot execute command.");
        }
        if (this._tesselId === null || this._tesselId === undefined) {
            log.error(this.toString());
            throw("No tile id provided, cannot execute command.");
        }

    }
}