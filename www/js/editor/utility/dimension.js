export {Dimension}

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("utility");

class Dimension {

    constructor(v1, v2, v3, v4){
        if (arguments.length === 0) {

        } else if (arguments.length === 2) {
            this._w = v1;
            this._h = v2;
        } else if (arguments.length === 4) {
            this._x = v1;
            this._y = v2;
            this._w = v3;
            this._h = v4;
        } else {
            log.error("Wrong number of parameter of Dimension", arguments);
            throw "Wrong number of parameter of Dimension. ";
        }
    }

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }

    get w() {
        return this._w;
    }

    get h() {
        return this._h;
    }


    set x(value) {
        this._x = value;
    }

    set y(value) {
        this._y = value;
    }

    set w(value) {
        this._w = value;
    }

    set h(value) {
        this._h = value;
    }

    toJSON() {
        return {x: this._x, y: this._y, w: this._w, h: this._h};
    }

    fromJSON(obj) {
        this._x = obj.x;
        this._y = obj.y;
        this._w = obj.w;
        this._h = obj.h;
        return this;
    }

    equals(dim) {
        return dim.x === this._x && dim.y === this._y && dim.w === this._w && dim.h === this._h;
    }
    clone() {
        return new Dimension(this._x, this._y, this._w, this._h);
    }

    toString() {
        return "Dimension [x: "+this.x + ", y: " + this._y + ", w: " + this._w + ", h: " + this._h + "]";
    }
}