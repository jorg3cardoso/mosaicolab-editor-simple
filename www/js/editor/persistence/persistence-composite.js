export {PersistenceComposite}

import {Persistence} from "./persistence";

import * as loglevel from '../libraries/loglevel.js';
var log = loglevel.getLogger("persistence");
class PersistenceComposite extends Persistence {
    constructor() {
        super();
        this._children = [];
    }

    add(persistence) {
        persistence.addObserver(this);
        this._children.push(persistence);
    }
    remove(persistence) {
        for( var i = this._children.length-1; i >= 0; i--){
            if ( this._children[i] === persistence) {
                persistence.removeObserver(this);
                this._children.splice(i, 1);
            }
        }
    }

    getChildren() {
        return [...this._children];
    }

    update(observable, cmds)  {
        super.update(observable, cmds);

        // notification from children, pass to our observers
        if (this._children.includes(observable)) {
            this.notifyObservers(cmds);
        } else { // notification from observed, passed to children
            this._children.forEach(function(persistance){
               persistance.update(observable, cmds);
            });

        }

    }

    getJS() {
        let json = null;
        for (let i = 0; i < this._children.length; i++) {
            json = this._children[i].getJS();
            if (json !== null) {
                break;
            }
        }
        return json;
    }

    loadFromJS(obj) {
        for (let i = 0; i < this._children.length; i++) {
            this._children[i].loadFromJS(obj);
        }
    }

    /**
     * A PersistenceComposite will try loading its children one at a time
     */
    load() {

        let promise = this._children[0].load();
        for (let i = 1; i < this._children.length; i++) {
            let curPersistance = this._children[i];
            let p = new Promise(function(resolve, reject){
               promise.then(curPersistance.load()).catch(curPersistance.load());
            });
            promise = p;
        }
        return promise;
    }


}