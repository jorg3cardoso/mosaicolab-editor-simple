import {HTMLObserverObservable} from "./html-observer-observable";

export {GridSizeListSelector}

import * as loglevel from '../../libraries/loglevel.js';
import {ListSelector} from "./list-selector";
import {CmdMosaicViewSetDefaultTile} from "../../cmd/view/cmd-mosaic-view-set-default-tile";
import {TesselShapeOval} from "../../mosaic/tessel-shape-oval";
import {TesselShapeRoundRect} from "../../mosaic/tessel-shape-round-rect";
import {TesselShapeTriangle} from "../../mosaic/tessel-shape-triangle";
import {CmdMosaicViewSetLayout} from "../../cmd/view/cmd-mosaic-view-set-layout";
var log = loglevel.getLogger("gui.html");

class GridSizeListSelector extends ListSelector {
    constructor() {
        super();
    }

    update(observable, params) {
        super.update(observable, params);
        if (params instanceof CmdMosaicViewSetLayout) {
            let size = params.params[0]+"x"+params.params[1];
            this.select(size);
        }
    }


    attributeChangedCallback(name, oldValue, newValue) {
        // When the drawer is disabled, update keyboard/screen reader behavior.
        // if (this.disabled) {
        //     this.setAttribute('tabindex', '-1');
        //     this.setAttribute('aria-disabled', 'true');
        // } else {
        //     this.setAttribute('tabindex', '0');
        //     this.setAttribute('aria-disabled', 'false');
        // }
        // TODO: also react to the open attribute changing.
    }
}

customElements.define('grid-size-list-selector', GridSizeListSelector);

